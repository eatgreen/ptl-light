#include "App.hpp"

namespace PTL{

class initTrack_App: public App
{
    private:        
        Dataset::Init_Track_Type        init_track_type_;
        OptMethod::Method_Type          opt_method_type_;

        std::unique_ptr<Transport>      transport_;

        cv::Point3i                     nXc_init_;
        std::string                     velo_file_name_;
        cv::Point3d                     Xc_top_init_;
        cv::Point3d                     Xc_bottom_init_;

        std::string                     predictor_file_;

    public:
        initTrack_App():App(){
            method_str_="initTrack";

            this->loadTransportParams();
            this->loadPTVParams();
            this->loadIPRParams();
            this->loadSTBParams();
            this->loadENSParams();

            init_track_type_=static_cast<Dataset::Init_Track_Type>(std::stoi(para_map_["init_track_type"]));
            opt_method_type_=static_cast<OptMethod::Method_Type>(std::stoi(para_map_["opt_method_type"]));

            velo_file_name_=para_map_["velo_file_name"];
            Xc_top_init_=cv::Point3d(std::stod(para_map_["Xc_topX_init"]),std::stod(para_map_["Xc_topY_init"]),std::stod(para_map_["Xc_topZ_init"]));
            Xc_bottom_init_=cv::Point3d(std::stod(para_map_["Xc_bottomX_init"]),std::stod(para_map_["Xc_bottomY_init"]),std::stod(para_map_["Xc_bottomZ_init"]));
            nXc_init_=cv::Point3i(std::stoi(para_map_["nXc_x_init"]),std::stoi(para_map_["nXc_y_init"]),std::stoi(para_map_["nXc_z_init"]));
        }

        void initialize(void) final{
            App::initialize();      

            transport_ = std::make_unique<Transport>(velocity_to_scatter_interp_type_, Xc_top_init_, Xc_bottom_init_, nXc_init_,
                                                     time_scheme_, dt_, dtObs_);
        
            cout<<"####### READ VELOCITY PREDICTION AND MAKE FINE GRID ON REFERENCE###"<<endl;
            if(case_name_.compare("lptchal4500")==0)     predictor_file_ = str(boost::format("%1%/result/%5%/part%2$06d_maxt%3$04d_fct0%4$04d/LAPIV%6%/%7%") 
                                                                        % rootDir_ % n_part_per_snapshot_ % it_tot_ % it_fct_.at(0) % case_name_  % run_index_ % velo_file_name_);
            cout<<predictor_file_<<endl;
            transport_->readVelocityFieldsFromTXTFile(predictor_file_);
            transport_->makeGrids();
            transport_->setVelocityOnGrids(transport_->velocity_fields());
        }

        void run(void) final{
            if(init_track_type_==Dataset::Init_Track_Type::Hacker){
                cout<<"Hacker version: idea background tracks, DO NOTHING!!!"<<endl
                    <<"Are you sure?"<<endl;
                std::abort();
            }
            else{
                ASSERT(init_track_type_==Dataset::Init_Track_Type::Triangulation);

                cout<<"##############################Read IPR Particle Field"<<endl;
                std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_ipr_in_frames;
                track::readIPRDataIntoFrames(it_bkg_, imgDir_+"/bkg", false, particles_ipr_in_frames);

                cout<<endl<<"##############################Make link"<<endl;
                auto particles_recon = track::makeLinkUsingPredictionField(it_bkg_, *transport_, dXc_px_, particles_ipr_in_frames);

                cout<<"##############################Save tracked particles"<<endl;
                track::saveInitTrackedParticles(it_bkg_, imgDir_, particles_recon);

                track::printTrackInformation(it_bkg_.back(), particles_recon);
            }
        }
};

}//namespace PTL

int main(){
    auto app=std::make_unique<PTL::initTrack_App>();

    app->printPara();
    app->initialize();

    auto t_deb=Clock::now();
    app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of reconstruction with initialization track method: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}