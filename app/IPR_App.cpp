#include "App.hpp"

namespace PTL{

class IPR_App : public App
{
    private:
        std::string                                 opt_method_str_;
        std::unique_ptr<OptMethod>                  opt_singlestep_;
        std::unique_ptr<PTV>                        ptv_;

        std::string                                 bkgdir_;

    public:
        IPR_App():App(){
            method_str_="IPR";
            this->loadPTVParams();
            this->loadIPRParams();
            this->loadSTBParams();
            this->loadENSParams();

            opt_method_str_=(IPR_opt_method_type_==OptMethod::Method_Type::STB? "STB" : "ENS");
            intensity_control_flag_ = true;

            bkgdir_ = imgDir_ + "/bkg";
            if(!boost::filesystem::exists(bkgdir_))
                boost::filesystem::create_directories(bkgdir_);

            IPR_retriangulation_iter_ = (IPR_flag_ ? IPR_retriangulation_iter_ : 1);
        }

        void printPara(void) final{
            App::printPara();
            cout<<std::boolalpha;
                cout<<str(boost::format("Optimization method             is %1%") % opt_method_str_)<<endl;
            if(IPR_opt_method_type_==OptMethod::Method_Type::STB){
                cout<<str(boost::format("Fine    Shake Maximum iteration is %1$02d.") % fine_shake_max_iter_)<<endl;
                cout<<str(boost::format("Fine    Shake Radius  pixel     is %1$f")    % fine_shake_pixel_)<<endl;
                cout<<str(boost::format("Fine    Shake Relax   factor    is %1$f")    % fine_shake_relaxfactor_)<<endl;
            }
            else if(IPR_opt_method_type_==OptMethod::Method_Type::ENS){
                cout<<str(boost::format("Ensemble number                 is %1$3d.")  % n_ens_)<<endl;
                cout<<str(boost::format("Regularization constant         is %1$3d.")  % regul_const_)<<endl;
                cout<<str(boost::format("Outer Loop Maximum iteration    is %1$02d.") % outer_max_iter_)<<endl;
                cout<<str(boost::format("Ensemble position initial error is %1% px.") % sigmaXcRatio_)<<endl;
            }       
        }

        void initialize(void) final{
            App::initialize();     

            auto min_intensity_ratio = (noise_ratio_<=0.01 ? 0.05 : (noise_ratio_<=0.03 ? 0.1 : 0.3) );
            
            if(case_name_.compare("lptchal4500")==0){  
                min_intensity_ratio=0.01;
                if(n_part_per_snapshot_==320000) min_intensity_ratio=0.03;
                cam_seq_ = std::vector<int>{0,3,1,2};
            }

            auto intensity_deletion_threshold_ratio = 0.1;
            auto intensity_min_threshold_ratio = 0;
            auto eval_window = (IPR_opt_method_type_==OptMethod::Method_Type::STB ? 3 : 2);
            auto iterative_triangulation_flag = false;
            auto lsq_triangulation_flag = true;

            ptv_=std::make_unique<PTV>( subPixel_type_, initial_min_intensity_threshold_, filter_threshold_,
                                        dilate_threshold_, dilate_surrounding_checker_threshold_, dilate_surrounding_difference_threshold_,
                                        search_threshold_, Wieneke_threshold_, stereo_verbal_flag_, min_intensity_ratio,
                                        Xc_top_, Xc_bottom_, dXc_px_, cam_seq_,
                                        iterative_triangulation_flag, !lsq_triangulation_flag);

            if(IPR_opt_method_type_==OptMethod::Method_Type::STB)
                opt_singlestep_ = std::make_unique<STB>(OptMethod::Method_Type::STB, control_vector_type_, intensity_control_flag_,
                                                        n_part_per_snapshot_, n_part_per_snapshot_, Xc_top_, Xc_bottom_,
                                                        dXc_px_, resDir_, eval_window,
                                                        IPR_max_iter_, fine_shake_pixel_, fine_shake_relaxfactor_);
            else if(IPR_opt_method_type_==OptMethod::Method_Type::ENS)
                opt_singlestep_ = std::make_unique<ENS>(OptMethod::Method_Type::ENS, control_vector_type_, intensity_control_flag_, 
                                                        n_part_per_snapshot_, n_part_per_snapshot_, Xc_top_, Xc_bottom_,
                                                        dXc_px_, resDir_, eval_window,
                                                        n_ens_, regul_const_, IPR_max_iter_, local_analysis_flag_, 
                                                        true, sigmaXcRatio_, sigmaE_, joint_intensity_flag_);

            opt_singlestep_->setintensity_deletion_threshold_ratio(intensity_deletion_threshold_ratio);
            opt_singlestep_->setintensity_min_threshold_ratio(intensity_min_threshold_ratio);
        }

        void run(void) final{
            for(auto pt_seq=it_bkg_.begin(); pt_seq<it_bkg_.end(); ++pt_seq){
                Particle::num_deleted_tot_=0;

                cout<<"#################### Reconstruct snapshots "<<*pt_seq<<"#################"<<endl;
                for(auto& ic : cam_)  ic->readRecord(*pt_seq, imgDir_);
                ptv_->readRecordsFromCamera(cam_);

                ptv_->setsearch_threshold(search_threshold_);
                ptv_->setWieneke_threshold(Wieneke_threshold_);

                for(int i=1; i<IPR_retriangulation_iter_+1; i++){
                    auto dup_flag = i>1; 
                    auto adjust_threshold_flag = false;//(i>=4);//set as false unless particle density is high and you have a lot of ghost

                    if(KLPT::IPR_PTV_pipeline(ptv_.get(), *pt_seq, cam_, false, i, dup_flag, save_result_, bkgdir_)){
                        KLPT::IPR_OPT_pipeline(ptv_.get(), opt_singlestep_.get(), *pt_seq, cam_, i);
                    }

                    // if(adjust_threshold_flag){
                    //     ptv_->setsearch_threshold(2.*search_threshold_);
                    //     ptv_->setWieneke_threshold(2.*Wieneke_threshold_);
                    // }
                }

                // ptv_->setsearch_threshold(0.5*search_threshold_);
                // ptv_->setWieneke_threshold(0.5*Wieneke_threshold_);

                ptv_->resetmin_intensity_threshold();
                if(IPR_flag_ && IPR_reduced_retriangulation_iter_!=0){
                     for(int i=1; i<IPR_reduced_retriangulation_iter_+1; i++){
                        auto dup_flag = true; 
                        auto adjust_threshold_flag = false;//(i>=1);//set as false unless particle density is high and you have a lot of ghost

                        if(KLPT::IPR_PTV_pipeline(ptv_.get(), *pt_seq, cam_, true, i, dup_flag)){
                            KLPT::IPR_OPT_pipeline(ptv_.get(), opt_singlestep_.get(), *pt_seq, cam_, i);
                        }

                        if(adjust_threshold_flag){
                            ptv_->setsearch_threshold(0.333*search_threshold_);
                            ptv_->setWieneke_threshold(0.333*Wieneke_threshold_);
                        }
                    }
                }

                cout<<"################SAVE RESULTS###################"<<endl;
                for(auto& ic: cam_) ic->saveImageResidual(*pt_seq, "Iresf", imgDir_);
                this->saveResults(*pt_seq);
                this->clearForNextCycle();
            }
        }

        void saveResults(int it_seq) final {
            if(IPR_flag_){
                cout<<"save IPR file"<<endl;
                ptv_->writeIPRParticlesToFile(it_seq, bkgdir_, opt_method_str_);
            }
        }

        void clearForNextCycle(void) final {
            opt_singlestep_->clear();
            ptv_->clearDataForNextTimeLevel();
        }
};

}//namespace PTL

int main(){
    auto app=std::make_unique<PTL::IPR_App>();

    app->printPara();
    app->initialize();

    auto t_deb=Clock::now();
    app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of reconstruction with IPR method: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}