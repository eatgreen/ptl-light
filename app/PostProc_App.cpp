#include "App.hpp"

namespace PTL{

class PostProc_App : public App
{
    private:
        bool                            check_ipr_flag_=false; 
        bool                            check_opt_flag_=false;
        bool                            rm_untracked_ghost_flag_=false;
        bool                            reorganize_particle_data_flag_=false;
        bool                            use_thin_data_flag_=false;
        bool                            use_reorder_data_flag_=false;
        bool                            check_track_stat_flag_=false;
        bool                            check_track_err_flag_=false;

        OptMethod::Method_Type          method_type_;
        Dataset::Init_Track_Type        init_track_type_;

    public:
        PostProc_App():App(){
            method_type_=static_cast<OptMethod::Method_Type>(std::stoi(para_map_["opt_method_type"]));
            if(method_type_==OptMethod::Method_Type::STB)        method_str_="STB";
            else if(method_type_==OptMethod::Method_Type::ENS)   method_str_="ENS";

            init_track_type_=static_cast<Dataset::Init_Track_Type>(std::stoi(para_map_["init_track_type"]));
            if(init_track_type_==Dataset::Init_Track_Type::Hacker)               init_track_str_="hck";
            else if(init_track_type_==Dataset::Init_Track_Type::Triangulation)   init_track_str_="trg";
        
            check_ipr_flag_=stob(para_map_["check_ipr_flag"]);
            check_opt_flag_=stob(para_map_["check_opt_flag"]);
            rm_untracked_ghost_flag_=stob(para_map_["rm_untracked_ghost_flag"]);
            reorganize_particle_data_flag_=stob(para_map_["reorganize_particle_data_flag"]);
            use_thin_data_flag_=stob(para_map_["use_thin_data_flag"]);
            use_reorder_data_flag_=stob(para_map_["use_reorder_data_flag"]);
            check_track_stat_flag_=stob(para_map_["check_track_stat_flag"]);
            check_track_err_flag_=stob(para_map_["check_track_err_flag"]);
        }

        void initialize(void) final {
            App::initialize();

            if(data_source_==Dataset::Source_Type::FromLPTChallenge){
                dataset_ = std::make_unique<LPTCHAL>(n_part_per_snapshot_, LPTCHAL::Chal_case_Type::TR);
            }

            dynmodel_ = std::make_unique<DynModel>(dynmodel_type_);
        }

        void run(void) final {
            auto opt_method_flag = ( method_type_==OptMethod::Method_Type::STB ? "STB" : "ENS");

            if(check_ipr_flag_){    
                cout<<"##################################################"<<endl;
                cout<<"Check IPR, read reference background data"<<endl;
                dataset_->readBackgroundDataTXT(imgDir_, it_bkg_, false, std::optional<cv::Point3d>{std::in_place, Xc_top_}, std::optional<cv::Point3d>{std::in_place, Xc_bottom_});

                cout<<"##################################################"<<endl;
                cout<<"For method "<<opt_method_flag<<endl;
                std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_ipr_in_frames;
                track::readIPRDataIntoFrames(std::vector<int>{it_deb_}, imgDir_+"/bkg", method_type_==OptMethod::Method_Type::STB, particles_ipr_in_frames);
                
                eval::evaluateIPRParticles(std::vector<int>{it_deb_}, dXc_px_, cam_, Xc_top_, Xc_bottom_, particles_ipr_in_frames, dataset_->particles_bkg());
            }

            if(rm_untracked_ghost_flag_){
               cout<<"##################################################"<<endl;
                cout<<"Read OPT Particle Data"<<endl;
                cout<<resDir_<<endl;
                dataset_->readParticleData(resDir_+"/result.dat", it_deb_, it_tot_, Dataset::Data_Format_Type::TXT_ONEFILE, Xc_top_, Xc_bottom_);

                int track_min_length=4;
                for(int it_seq=it_deb_; it_seq<=it_tot_; it_seq++){
                    track::saveFrameDataTXT(it_seq, dataset_->particles_ref(), resDir_, dtObs_, it_seq!=it_deb_, track_min_length, "_thin");
                    track::saveFrameDataVTK(it_seq, dataset_->particles_ref(), resDir_, track_min_length, "_thin");
                }
            }

            if(reorganize_particle_data_flag_){
               cout<<"##################################################"<<endl;
                cout<<"Read OPT Particle Data"<<endl;
                cout<<resDir_<<endl;
                dataset_->readParticleData(resDir_+"/result.dat", it_deb_, it_tot_, Dataset::Data_Format_Type::TXT_ONEFILE, Xc_top_, Xc_bottom_);

                // auto particles_rm      = std::move(dataset_->particles_ref());
                // auto particles_rm      = track::rmVeryShortTracksFromParticleField(dataset_->particles_ref(), Xc_top_, Xc_bottom_);

                auto particles_reorder = track::reorderParticleField(dataset_->particles_ref());
    
                for(int it_seq=it_deb_; it_seq<=it_tot_; it_seq++){
                    track::saveFrameDataTXT(it_seq, particles_reorder, resDir_, dtObs_, it_seq!=it_deb_, 1, "_reorder");
                    track::saveFrameDataVTK(it_seq, particles_reorder, resDir_, 1, "_reorder");
                }    
            }

            if(check_opt_flag_){
                cout<<"##################################################"<<endl;
                cout<<"Read OPT Particle Data"<<endl;

                auto res_name = resDir_+"/result"+(use_thin_data_flag_ ? "_thin" : (use_reorder_data_flag_ ? "_reorder" : "") )+".dat";
                cout<<res_name<<endl;

                dataset_->readParticleData(res_name, it_deb_, it_tot_, Dataset::Data_Format_Type::TXT_ONEFILE, Xc_top_, Xc_bottom_);
                
                if(check_track_stat_flag_){
                    track::printTrackInformation(it_tot_, dataset_->particles_ref(), dtObs_, dXc_px_);
                }

                if(check_track_err_flag_){
                    auto particles_opt = std::move( dataset_->get_particles_ref() );                    

                    cout<<"##################################################"<<endl;
                    cout<<"Read Reference Particle Data"<<endl;
                    if(case_name_=="lptchal4500"){
                        auto original_str="_original";
                        auto davisFile=str(boost::format("%1%/result/%5%/part%2$06d_maxt%3$04d_fct0%4$04d/STBdebug_davis%6%/ShakeTheBox-tr.dat") 
                                            % rootDir_ % n_part_per_snapshot_ % it_tot_ % it_fct_.at(0) % case_name_ % original_str);

                        dataset_->readParticleData(davisFile, it_deb_, it_tot_, Dataset::Data_Format_Type::TXT_ONEFILE, Xc_top_, Xc_bottom_);
                    }

                    cout<<"##################################################"<<endl;
                    cout<<"Compute track information"<<endl;
                    eval::evaluateOPTTracks(it_fct_, dXc_px_, particles_opt, dataset_->particles_ref() , cam_);

                    cout<<"##################################################"<<endl;
                    cout<<"Compute particle information"<<endl;
                    eval::evaluateOPTParticles(it_fct_, dXc_px_, Xc_top_, Xc_bottom_, particles_opt, dataset_->particles_ref() , cam_);
                }
            }
        }
};

}//namespace PTL

int main(){
    auto app=std::make_unique<PTL::PostProc_App>();

    app->initialize();

    auto t_deb=Clock::now();
    app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of postprocessing: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}