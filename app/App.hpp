#ifndef APP
#define APP

#include "../src/KLPT.hpp"
#include "../src/LPTCHAL.hpp"
#include "../src/Evaluation.hpp"
#include "../src/Track.hpp"

namespace PTL{

class App
{
	protected:
        std::unordered_map<std::string, std::string> para_map_;
		std::string 								 rootDir_;

		int 							n_cam_;
		int 							opencv_mat_type_;
		int 							n_part_per_snapshot_;
		std::string 					case_name_;
		int 							it_tot_case_;
		int 							it_deb_;
		int 							it_tot_;
		int 							it_bkg_fin_;
		bool 							save_result_;
		OptMethod::ControlVector_Type 	control_vector_type_;
		DynModel::DynModel_Type 	  	dynmodel_type_;
		double 						  	pixel_lost_;
		std::string 				  	run_index_;
		Dataset::Source_Type 	    	data_source_;
		bool 						  	intensity_control_flag_;
		double      					noise_ratio_;

		std::string 		dataDir_;
		std::string 		data_path_;
		std::string			property_path_;

		cv::Point3d 		Xc_top_;
		cv::Point3d 		Xc_bottom_;

		double 				theta_=6.;

		Transport::TimeScheme_Type	     		 time_scheme_;
        double 									 dt_;
        double 									 dtObs_=0.0;
		Transport::VelocityInterpolation_Type	 velocity_to_scatter_interp_type_;

		Calibration::Source_Type  calibration_source_; 
		Camera::Model_Type 		  model_type_;
        cv::Point2i 			  nXc_rig_;
        int 					  nViews_;
		int 					  z_order_;

		int 					  n_width_;
		int 					  n_height_;
		int 					  n_width_mask_;
		int 					  n_height_mask_;

        double 					  f_;
        double 					  k_;
        double 					  Z0_;
        cv::Mat 				  distCoeffs_;
		
        PTV::SubPixelMethod_Type        subPixel_type_;
        double                          initial_min_intensity_threshold_;
        double                          filter_threshold_;
        double                          dilate_threshold_;
        double                          dilate_surrounding_checker_threshold_;
        double                          dilate_surrounding_difference_threshold_;

        double                          search_threshold_;
        double                          Wieneke_threshold_;        
        bool                            stereo_verbal_flag_;

        int                             IPR_retriangulation_iter_;
        int                             IPR_reduced_retriangulation_iter_;
        bool                            IPR_flag_;
        int                             IPR_max_iter_;
        OptMethod::Method_Type          IPR_opt_method_type_;

		bool 				            initial_shake_flag_;
        int  				            initial_shake_max_iter_;
        double                          initial_shake_pixel_;
        double                          initial_shake_relaxfactor_;
        int  				            fine_shake_max_iter_;
        double                          fine_shake_pixel_;
        double                          fine_shake_relaxfactor_;

		int         		            n_ens_;
        double      		            regul_const_;
        int         		            outer_max_iter_;
        bool        		            local_analysis_flag_;
        bool        		            resample_flag_;
        cv::Point3d 		            sigmaXcRatio_;
        double      		            sigmaE_;
        bool        		            joint_intensity_flag_;
        bool        		            warm_start_flag_;
        int         		            warm_start_max_iter_;
        double      		            warm_start_pixel_;
        double      		            warm_start_relaxfactor_;

		std::string 		method_str_;
		std::string 		init_track_str_;
		std::string 		imgDir_;
        std::string 		resDir_;

        cv::Point3d			dXc_px_;

		std::vector<int> 	it_bkg_;
		std::vector<int> 	it_fct_;

		std::unique_ptr<Dataset>     dataset_;

		std::vector<Camera_ptr> 	 cam_;

        std::unique_ptr<DynModel>    dynmodel_;

        LPTCHAL::Chal_case_Type      case_type_=LPTCHAL::Chal_case_Type::TR;

        std::vector<int>             cam_seq_{0,1,2,3};

	public:
		App(const std::string& para_name="para_config");

		virtual ~App() = 0;

		virtual void printPara(void);

		virtual void initialize(void);

		virtual void run(void){}

		virtual void saveResults(int){}

		virtual void clearForNextCycle(void){}

	protected:
		void loadENSParams(void);
		void loadSTBParams(void);
		void loadIPRParams(void);
		void loadPTVParams(void);
		void loadCalibrationParams(void);
		void loadTransportParams(void);

        bool stob(std::string str){
            std::istringstream is(str);
            bool b;
            is>>std::boolalpha>>b;
            return b;
        }
};

App::~App(){}

App::App(const std::string& para_name){
    rootDir_=std::getenv("PTL_PATH");

    std::string paraPath=boost::filesystem::current_path().string();
    paraPath=paraPath+"/test/"+para_name;
    cout<<"paraPath "<<paraPath<<endl;

	std::ifstream paraFile(paraPath);
    std::string   line,name,value;

	if(paraFile.is_open())
	{
		while(std::getline(paraFile,line)){
            std::string::size_type nc = line.find("//");
            if(nc != std::string::npos)
                line.erase(nc);

            std::istringstream is(line);
            if(is >> name >> value){
            	para_map_.insert({name, value});
            }
		}
	}	
	else{
        std::cerr<<"Error opening para_config file"<<endl;
        std::abort();
    }

    paraFile.close();

    n_cam_=std::stoi(para_map_["n_cam"]);
    opencv_mat_type_=std::stoi(para_map_["opencv_mat_type"]);
    n_part_per_snapshot_=std::stoi(para_map_["n_part"]);
    case_name_=para_map_["case_name"];
    it_tot_case_=std::stoi(para_map_["it_tot_case"]);
    it_deb_=std::stoi(para_map_["it_deb"]);
    it_tot_=std::stoi(para_map_["it_tot"]);
    it_bkg_fin_=std::stoi(para_map_["it_bkg_fin"]);
    save_result_=stob(para_map_["save_result"]);
    control_vector_type_=static_cast<OptMethod::ControlVector_Type>(std::stoi(para_map_["control_vector_type"]));
    dynmodel_type_=static_cast<DynModel::DynModel_Type>(std::stoi(para_map_["dynmodel_type"]));
    pixel_lost_=std::stod(para_map_["pixel_lost"]);
    run_index_=para_map_["run_index"];
    data_source_=static_cast<Dataset::Source_Type>(std::stoi(para_map_["data_source"]));
    noise_ratio_=std::stod(para_map_["noise_ratio"]);

    if(data_source_==Dataset::Source_Type::FromLPTChallenge){
        dataDir_=std::getenv("PTVDATA_PATH");
        data_path_=para_map_["data_path"];
        property_path_=para_map_["property_path"];
    }

    Xc_top_=cv::Point3d(std::stod(para_map_["Xc_topX"]),std::stod(para_map_["Xc_topY"]),std::stod(para_map_["Xc_topZ"]));
    Xc_bottom_=cv::Point3d(std::stod(para_map_["Xc_bottomX"]),std::stod(para_map_["Xc_bottomY"]),std::stod(para_map_["Xc_bottomZ"]));

    dtObs_=std::stod(para_map_["dtObs"]);

    calibration_source_=static_cast<Calibration::Source_Type>(std::stoi(para_map_["calibration_source"]));
    model_type_=static_cast<Camera::Model_Type>(std::stoi(para_map_["model_type"]));

    if(calibration_source_==Calibration::Source_Type::FromLPTChallenge){
        theta_=std::stod(para_map_["theta"]);
        Camera::opencv_mat_type_=opencv_mat_type_;
        if(opencv_mat_type_==CV_8UC1)       Camera::max_intensity_value_=255;
        else if(opencv_mat_type_==CV_16UC1) Camera::max_intensity_value_=65535;
    }

    Camera::psf_type_=static_cast<Camera::psf_Type>(std::stoi(para_map_["psf_type"]));

    it_bkg_.resize(it_bkg_fin_-it_deb_+1);
    std::iota(it_bkg_.begin(), it_bkg_.end(), it_deb_);
    it_fct_.resize(it_tot_-it_bkg_fin_);
    std::iota(it_fct_.begin(), it_fct_.end(), it_bkg_fin_+1);

    if(case_name_.substr(0, 7).compare("lptchal")==0){
        case_type_=static_cast<LPTCHAL::Chal_case_Type>(std::stoi(para_map_["case_type"]));
        if(case_type_==LPTCHAL::Chal_case_Type::TP){
            it_deb_=1;
            it_tot_case_=2;
            it_tot_=2;
            it_bkg_fin_=1;

            it_bkg_.assign(1, 1);
            it_fct_.assign(1, 2);
        }

        imgDir_=str(boost::format("%1%/data/%5%/part%2$06d_maxt%3$04d_fct0%4$04d_dtobs%6$.4f") 
                            % rootDir_ % n_part_per_snapshot_ % it_tot_case_ % it_fct_.at(0) % case_name_ % dtObs_);
    }
}

void App::loadENSParams(void){
    n_ens_=std::stoi(para_map_["n_ens"]);
    regul_const_=std::stod(para_map_["regul_const"]);
    outer_max_iter_=std::stoi(para_map_["outer_max_iter"]);
    local_analysis_flag_=stob(para_map_["local_analysis_flag"]);
    resample_flag_=stob(para_map_["resample_flag"]);
    sigmaXcRatio_=cv::Point3d(std::stod(para_map_["sigmaXcRatioX"]),std::stod(para_map_["sigmaXcRatioY"]),std::stod(para_map_["sigmaXcRatioZ"]));
    sigmaE_=std::stod(para_map_["sigmaE"]);
    joint_intensity_flag_=stob(para_map_["joint_intensity_flag"]);
    warm_start_flag_=stob(para_map_["warm_start_flag"]);
    warm_start_max_iter_=std::stoi(para_map_["warm_start_max_iter"]);
    warm_start_pixel_=std::stod(para_map_["warm_start_pixel"]);
    warm_start_relaxfactor_=std::stod(para_map_["warm_start_relaxfactor"]);
}

void App::loadSTBParams(void){
    initial_shake_flag_=stob(para_map_["initial_shake_flag"]);
    initial_shake_max_iter_=std::stoi(para_map_["initial_shake_max_iter"]);
    fine_shake_max_iter_=std::stoi(para_map_["fine_shake_max_iter"]);
    initial_shake_pixel_=std::stod(para_map_["initial_shake_pixel"]);
    initial_shake_relaxfactor_=std::stod(para_map_["initial_shake_relaxfactor"]);
    fine_shake_pixel_=std::stod(para_map_["fine_shake_pixel"]);
    fine_shake_relaxfactor_=std::stod(para_map_["fine_shake_relaxfactor"]);
}

void App::loadIPRParams(void){
    IPR_retriangulation_iter_=std::stoi(para_map_["IPR_retriangulation_iter"]);
    IPR_reduced_retriangulation_iter_=std::stoi(para_map_["IPR_reduced_retriangulation_iter"]);
    IPR_flag_=stob(para_map_["IPR_flag"]);
    IPR_max_iter_=std::stoi(para_map_["IPR_max_iter"]);
    IPR_opt_method_type_=static_cast<OptMethod::Method_Type>(std::stoi(para_map_["IPR_opt_method"]));
}

void App::loadPTVParams(void){
    subPixel_type_=static_cast<PTV::SubPixelMethod_Type>(std::stoi(para_map_["subPixel_type"]));
    initial_min_intensity_threshold_=std::stod(para_map_["initial_min_intensity_threshold"]);
    filter_threshold_=std::stod(para_map_["filter_threshold"]);
    dilate_threshold_=std::stod(para_map_["dilate_threshold"]);
    dilate_surrounding_checker_threshold_=std::stod(para_map_["dilate_surrounding_checker_threshold"]);
    dilate_surrounding_difference_threshold_=std::stod(para_map_["dilate_surrounding_difference_threshold"]);
    
    search_threshold_=std::stod(para_map_["search_threshold"]);
    Wieneke_threshold_=std::stod(para_map_["Wieneke_threshold"]);
    stereo_verbal_flag_=stob(para_map_["stereo_verbal_flag"]);
}

void App::loadCalibrationParams(void){
    z_order_=std::stoi(para_map_["z_order"]);
    n_width_=std::stoi(para_map_["n_width"]);
    n_height_=std::stoi(para_map_["n_height"]);

    f_=std::stod(para_map_["f"]);
    k_=std::stod(para_map_["k"]);
}

void App::loadTransportParams(void){
    time_scheme_=static_cast<Transport::TimeScheme_Type>(std::stoi(para_map_["time_scheme"]));
    dt_=std::stod(para_map_["dt"]);
    velocity_to_scatter_interp_type_=static_cast<Transport::VelocityInterpolation_Type>(std::stoi(para_map_["velocity_to_scatter_interp_type"]));
}

void App::printPara(void){
    cout<<"This is STB-SPH-ENS run with "<<method_str_<<" method."<<endl;
    std::string source_type = (data_source_==Dataset::Source_Type::FromLPTChallenge ? 
                                "From LPTChallenge" : "User defined");
    cout<<str(boost::format("Test case with data %1%") % source_type )<<endl;
    cout<<str(boost::format("Test case with %1$5d particles, %2$1d cameras.") % n_part_per_snapshot_ % n_cam_)<<endl;
    std::string camera_model_type = (model_type_==Camera::Model_Type::Pinhole ? "Pinhole" : 
                                                                                (model_type_==Camera::Model_Type::Polynomial ? 
                                                                                    "Polynomial" : "PinholeAndPolynomial"));
    cout<<str(boost::format("Test case with %1% as camera model.") % camera_model_type)<<endl;
    cout<<str(boost::format("Test case starts at %1$03d time level, ends at %2$03d time level.") % it_deb_ % it_tot_)<<endl;
    cout<<str(boost::format("Background ends  at %1$03d time level.") % it_bkg_fin_)<<endl;
    cout<<str(boost::format("Analysis starts  at %1$03d time level, ends at %2$03d time level.") % it_fct_.at(0) % it_tot_)<<endl;
    cout<<str(boost::format("Image Background error  is %1$03d pixels.") % noise_ratio_)<<endl;
    cout<<std::boolalpha;
    cout<<"Save result flag        is "<<save_result_<<endl;
}

void App::initialize(void){

    if( method_str_=="ENS" || method_str_=="STB" ){
        if(case_name_.substr(0, 7).compare("lptchal")==0){
            resDir_=str(boost::format("%1%/result/%5%/part%2$06d_maxt%3$04d_fct0%4$04d/%6%%7%") 
                                % rootDir_ % n_part_per_snapshot_ % it_tot_ % it_fct_.at(0) % case_name_ % method_str_ % run_index_);
        }

        if(!boost::filesystem::exists(resDir_))
            boost::filesystem::create_directories(resDir_);

        for(int i_cam=0; i_cam<n_cam_; i_cam++){
            auto resDir_icam=resDir_+"/cam"+std::to_string(i_cam);
            if(!boost::filesystem::exists(resDir_icam))
                boost::filesystem::create_directories(resDir_icam);
        }
    }

    for(int i_cam=0; i_cam<n_cam_; i_cam++)
        cam_.push_back( std::make_unique<Camera>( model_type_, n_cam_, i_cam ) );
    
    bool pert_otf_flag =  case_name_.compare("lptchal4500")==0;

    for(auto& ic : cam_)
        ic->readParamsYaml(imgDir_, true, pert_otf_flag);

    cam_[0]->setPSFGrids(Xc_top_, Xc_bottom_);

    dXc_px_=cv::Point3d(0,0,0);
    for(auto& ic : cam_) dXc_px_+=ic->dXc_px()/static_cast<double>(n_cam_);
}

}//namespace PTL
#endif