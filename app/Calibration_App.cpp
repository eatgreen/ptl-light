#include "App.hpp"

namespace PTL{

class Calibration_App : public App
{
    private:
        std::unique_ptr<Calibration>    calib_;

        bool                            process_img_flag_=false;

        std::string                     img_src_path_;

        std::string                     calib_path_;

        std::string                     MarkTable_path_;

    public:
        Calibration_App():App(){
            method_str_="Calibration";
            this->loadCalibrationParams();

            process_img_flag_=stob(para_map_["process_img_flag"]);
        }

        void initialize(void) final {
            for(int i_cam=0; i_cam<n_cam_; i_cam++)
                cam_.push_back( std::make_unique<Camera>( model_type_, n_cam_, i_cam, n_width_, n_height_, z_order_, f_, k_) );

            calib_ = std::make_unique<Calibration>( calibration_source_, n_cam_, Xc_top_, Xc_bottom_ );

            switch(data_source_){
                case Dataset::Source_Type::FromLPTChallenge:
                    img_src_path_ = dataDir_+data_path_;
                    calib_path_ = dataDir_+property_path_;
                    MarkTable_path_ = calib_path_+"calibpoints.txt";
                    dataset_ = std::make_unique<LPTCHAL>(n_part_per_snapshot_, case_type_);
                    break;
            }

            cout<<"img_src_path_ "<<img_src_path_<<endl;
            cout<<"calib_path_   "<<calib_path_<<endl;
        }

        void run(void) final {
            std::string camera_model_type;
            switch (model_type_){
                case Camera::Model_Type::Pinhole:               camera_model_type="Pinhole"; break;
                case Camera::Model_Type::Polynomial:            camera_model_type="Polynomial"; break;
                case Camera::Model_Type::PinholeAndPolynomial:  camera_model_type="PinholeAndPolynomial"; break;
            }

            cout<<"###########CALIBRATION USING "<<camera_model_type<<" MODEL##################"<<endl;

            std::vector<double> ZViews;
            switch(data_source_){
                case Dataset::Source_Type::FromLPTChallenge:{
                    ZViews = std::vector<double>{0, 5, 10, 15, 20, 25, 30};
                    nXc_rig_ = cv::Point2i(24, 16);
                    break;
                }
                default:
                    break;
            }

            cout<<"########### READ CALIBRATION CORRESPANDENCE FILE "<<endl;
            calib_->clearXcoord_obj_cams();
            calib_->clearxcoord_img();
            calib_->setZViews(ZViews);
            dataset_->readMarkPositionTable(MarkTable_path_,
                                            calib_->nViews(), 
                                            calib_->get_Xcoord_obj_cams(),
                                            calib_->get_xcoord_img(), nXc_rig_.x, nXc_rig_.y);
            
            for(auto& ic : cam_){
                cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
                cout<<"###########GET PINHOLE MODEL PARAMETERS#####################"<<endl;
                int     view_zero = 0;

                int opencv_calib_flag = 
                     cv::CALIB_FIX_ASPECT_RATIO
                    |cv::CALIB_FIX_PRINCIPAL_POINT
                    // |cv::CALIB_RATIONAL_MODEL
                    // |cv::CALIB_TILTED_MODEL
                    // |cv::CALIB_FIX_TAUX_TAUY
                    // |cv::CALIB_THIN_PRISM_MODEL
                    |cv::CALIB_FIX_K1|cv::CALIB_FIX_K2
                    |cv::CALIB_FIX_K3
                    |cv::CALIB_FIX_K4|cv::CALIB_FIX_K5|cv::CALIB_FIX_K6
                    |cv::CALIB_ZERO_TANGENT_DIST
                    ;

                calib_->getPinholeModelParameters(ic.get(), opencv_calib_flag, view_zero);

                cout<<"###########COMPUTE PINHOLE MAPPING FUNCTION ERROR###########"<<endl;
                calib_->computeMappingFunctionError(*ic, true);
            }

            cout<<"###########DO STEREO CALIBRATION###############################"<<endl;
            for(int ic1=0; ic1<n_cam_; ic1++){
                for(int ic2=0; ic2<n_cam_; ic2++){
                    cout<<"Between "<<ic1<<"th camera and "<<ic2<<"th camera"<<endl;
                    calib_->getStereoCalibrationParameters(cam_[ic1].get(), cam_[ic2].get());
                }
            }

            if(model_type_==Camera::Model_Type::Polynomial || model_type_==Camera::Model_Type::PinholeAndPolynomial){
                for(auto& ic : cam_){
                    cout<<"###########GET POLYNOMIAL COEFFICIENT#######################"<<endl;
                    double  PixelPerMmFactor = std::abs(k_*f_/Z0_);
                    auto    normalizationFactor = cv::Point3d( double(n_width_), double(n_height_), double(n_width_)*((Xc_top_.z-Xc_bottom_.z)/(Xc_top_.x-Xc_bottom_.x)) )*1.1;
                    calib_->getPolynomialModelParameters(ic.get(), PixelPerMmFactor, normalizationFactor);

                    cout<<"###########COMPUTE MAPPING FUNCTION ERROR###################"<<endl;
                    calib_->computeMappingFunctionError(*ic);
                }
            }

            for(auto& ic : cam_){
                cout<<"########### CHECK DOMAIN BOUNDARY ON IMAGE SPACE############"<<endl;
                calib_->checkDomainBoundaryOnImageSpace(*ic, Xc_top_, Xc_bottom_);

                ic->checkSensitivityOfMappingFunction((Xc_top_-Xc_bottom_)/2., Xc_top_.x-Xc_bottom_.x);
            
                cout<<"########### READ OPTICAL TRANSFER FUNCTION##################"<<endl;
                calib_->setPSFParams(ic.get(), theta_);
            }

            cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
            if(!boost::filesystem::exists(imgDir_)){
                boost::filesystem::create_directories(imgDir_);
            }

            for(auto& ic : cam_)
                ic->saveParamsToYamlFile(imgDir_);

            if(process_img_flag_){      
                cout<<"########### PROCESSING PARTICLE IMAGE DATA##################"<<endl;
                for(int i_cam=0; i_cam<n_cam_; i_cam++){
                    auto imgDir_icam=imgDir_+"/cam"+std::to_string(i_cam);
                    if(!boost::filesystem::exists(imgDir_icam))
                        boost::filesystem::create_directories(imgDir_icam);
                }

                dataset_->setIMGData(img_src_path_, it_deb_, it_tot_, cam_, imgDir_);
            }
        }
};

}

int main(){
    auto app=std::make_unique<PTL::Calibration_App>();

    app->initialize();

    auto t_deb=Clock::now();
    app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of calibrate cameras: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}