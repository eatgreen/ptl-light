#include "App.hpp"

namespace PTL{

class PreProc_App : public App
{
    public:
        enum class Run_Type          {VSC_with_raw_image_raw_cam, VSC_with_processed_image_raw_cam, VSC_with_processed_image_processed_cam, Calib_Pinhole, Test_triangulation, Test_fundamental_matrix, Test_3D2D, Test_2D3D, Calib_OTF, Export_to_Davis};

    private:
        std::unique_ptr<Calibration> calib_;
        bool                         high_pass_flag_;
        bool                         undistort_original_image_flag_;
        bool                         mask_flag_;

        int                          subtract_sliding_minimum_size_;
        int                          subtract_constant_pixel_value_;
        int                          multiply_pixel_factor_;

        double                       brightest_threshold_ratio_;

        bool                         vsc_flag_=false;
        int                          num_brightest_=0;

        Calibration::OTFCalib_Type   OTF_calib_type_;

        std::unique_ptr<PTV>         ptv_;

        bool                         image_preprocess_flag_=false;
        bool                         read_raw_image_flag_=false;
        bool                         read_raw_cam_flag_=false;
        bool                         triangulation_test_flag_=false;
        bool                         fundamental_matrix_test_flag_=false;
        bool                         stereo_test_3Dto2D_flag_=false;
        bool                         stereo_test_2Dto3D_flag_=false;
        bool                         otf_calib_flag_=false;

        cv::Point3d                  Xc_doi_top_;
        cv::Point3d                  Xc_doi_bottom_;

        Run_Type                     run_type_;

        pcl::PointCloud<pcl::PointXYZI>::Ptr spawned_particles_in_one_frame_cloud_=pcl::PointCloud<pcl::PointXYZI>::Ptr(new pcl::PointCloud<pcl::PointXYZI>);

    public:
        PreProc_App():App(){
            method_str_="PreProcessing";
            run_type_=static_cast<PreProc_App::Run_Type>(std::stoi(para_map_["preprocess_run_type"]));

            high_pass_flag_=stob(para_map_["high_pass_flag"]);
            undistort_original_image_flag_=stob(para_map_["undistort_origin_flag"]);
            mask_flag_=stob(para_map_["mask_flag"]);

            subtract_sliding_minimum_size_=std::stoi(para_map_["subtract_sliding_minimum_size"]);
            subtract_constant_pixel_value_=std::stoi(para_map_["subtract_constant_pixel_value"]);
            multiply_pixel_factor_=std::stoi(para_map_["multiply_pixel_factor"]);
            brightest_threshold_ratio_=std::stod(para_map_["brightest_threshold_ratio"]);

            num_brightest_=std::stoi(para_map_["num_brightest"]);

            OTF_calib_type_=static_cast<Calibration::OTFCalib_Type>(std::stoi(para_map_["OTF_calib_type"]));

            this->loadCalibrationParams();
            this->loadPTVParams();

            switch(run_type_){
                case Run_Type::VSC_with_raw_image_raw_cam:
                    cout<<"0 vsc run with raw image and raw camera"<<endl;
                    vsc_flag_=true; 
                    read_raw_image_flag_=true; 
                    read_raw_cam_flag_=true; break;
                case Run_Type::VSC_with_processed_image_raw_cam:
                    cout<<"1 vsc run with preprocessed image and raw camera"<<endl;
                    vsc_flag_=true;
                    read_raw_image_flag_=false; 
                    read_raw_cam_flag_=true; break;
                case Run_Type::VSC_with_processed_image_processed_cam:
                    cout<<"2 vsc run with preprocessed image and processed camera"<<endl;
                    vsc_flag_=true; 
                    read_raw_image_flag_=false; 
                    read_raw_cam_flag_=false; break;
                case Run_Type::Test_triangulation:
                    cout<<"3 run triangulation to test R and T"<<endl;
                    triangulation_test_flag_=true; break;
                case Run_Type::Test_fundamental_matrix:
                    cout<<"4 run test fundamental matrix"<<endl;
                    fundamental_matrix_test_flag_=true; break;
                case Run_Type::Test_3D2D:
                    cout<<"5 run test 3D-2D correspondence, Optional"<<endl;
                    stereo_test_3Dto2D_flag_=true; break;
                case Run_Type::Test_2D3D:
                    cout<<"6 run test 2D-3D correspondence, Optional"<<endl;
                    stereo_test_2Dto3D_flag_=true; break;
                case Run_Type::Calib_OTF:
                    cout<<"7 run calibrate OTF"<<endl;
                    otf_calib_flag_=true; break;
                default: 
                    image_preprocess_flag_=true;
                    read_raw_cam_flag_=true; break;
            }
        }

        void initialize(void) final {
            for(int i_cam=0; i_cam<n_cam_; i_cam++)
                cam_.push_back( std::make_unique<Camera>(model_type_, n_cam_, i_cam, n_width_, n_height_, z_order_, f_, k_) );

            Xc_doi_top_ = Xc_top_;
            Xc_doi_bottom_ = Xc_bottom_;

            calib_ = std::make_unique<Calibration>( calibration_source_, n_cam_,
                                                    Xc_doi_top_, Xc_doi_bottom_, nXc_rig_, nViews_ );

            bool init_cam_read_flag = data_source_ == Dataset::Source_Type::FromLPTChallenge ? false : !read_raw_cam_flag_;
            
            for(auto& ic : cam_)
                ic->readParamsYaml(imgDir_, init_cam_read_flag);

            cam_[0]->setPSFGrids(Xc_doi_top_, Xc_doi_bottom_);

            dXc_px_=cv::Point3d(0,0,0);
            for(auto& ic : cam_) dXc_px_+=ic->dXc_px()/static_cast<double>(n_cam_);

            if(data_source_==Dataset::Source_Type::FromLPTChallenge)
                dataset_ = std::make_unique<LPTCHAL>();

            ptv_=std::make_unique<PTV>( subPixel_type_, initial_min_intensity_threshold_*brightest_threshold_ratio_, filter_threshold_,
                                        dilate_threshold_, dilate_surrounding_checker_threshold_, dilate_surrounding_difference_threshold_,
                                        search_threshold_, Wieneke_threshold_, stereo_verbal_flag_, 0.1,
                                        Xc_doi_top_, Xc_doi_bottom_, dXc_px_, cam_seq_ );
        }

        void run(void) final {
            //######################################################################################################
            if(image_preprocess_flag_){
                for(auto& ic : cam_){
                    cout<<"###########GENERATE UNDISDORT IMAGE RECORD "<<ic->i_cam()<<"#####"<<endl;
                    ic->preprocessRecords(it_bkg_, it_tot_case_, imgDir_, high_pass_flag_, undistort_original_image_flag_,
                                            subtract_sliding_minimum_size_, subtract_constant_pixel_value_, multiply_pixel_factor_,
                                            false, false,
                                            mask_flag_, Xc_doi_top_, Xc_doi_bottom_);
                }
            }

            //######################################################################################################
            if(vsc_flag_){
                cout<<"########### PROCESSING DAVIS PARTICLE FIELD#################"<<endl;
                dataset_->readSyntheticData(imgDir_, it_deb_, it_tot_);

                int vsc_iter_max=1;
                for(int it=0; it<vsc_iter_max; it++){

                    double max_threshold=0.5;

                    std::vector<std::vector<cv::Point3d>> obj_cams(cam_.size(), std::vector<cv::Point3d>());
                    std::vector<std::vector<cv::Point2d>> img_cams(cam_.size(), std::vector<cv::Point2d>());

                    for(int it_seq=it_bkg_.front(); it_seq<=it_tot_; ++it_seq){
                        cout<<"#################################################################"<<endl;
                        cout<<"For "<<it_seq<<"th snapshot at "<<it<<"th VSC iteration"<<endl;
                        ptv_->clearDataForNextTimeLevel();

                        cout<<"Find the brightest particles in 3D spaces"<<endl;
                        auto particles_vsc=track::findTheBrightestParticles(num_brightest_, it_seq, dataset_->particles_ref(), 
                                                                            Xc_doi_top_, Xc_doi_bottom_);

                        cout<<"Find the brightest points on images"<<endl;
                        for(auto& ic : cam_)  ic->readRecord(it_seq, imgDir_, !read_raw_image_flag_);
                        ptv_->readRecordsFromCamera(cam_);
                        ptv_->findParticleImagePeak();

                        for(auto& ic : cam_)
                            calib_->collectSamplePointsForVSC(it_seq, ic.get(), ptv_->Points_xy()[ic->i_cam()], particles_vsc, max_threshold, obj_cams[ic->i_cam()], img_cams[ic->i_cam()]);
                    }

                    double      PixelPerMmFactor = cam_[0]->PixelPerMmFactor();
                    cv::Point3d normalizationFactor = cv::Point3d( double(n_width_), double(n_height_), 
                                            double(n_width_)*((Xc_top_.z-Xc_bottom_.z)/(Xc_top_.x-Xc_bottom_.x)) )*1.1;
            
                    for(auto& ic : cam_)
                        // calib_->volumeSelfCalibration(ic.get(), obj_cams[ic->i_cam()], img_cams[ic->i_cam()]);
                        calib_->volumeSelfCalibration(ic.get(), obj_cams[ic->i_cam()], img_cams[ic->i_cam()], PixelPerMmFactor, normalizationFactor);
                }

                for(auto& ic : cam_){
                    cout<<"########### CHECK DOMAIN BOUNDARY ON IMAGE SPACE############"<<endl;
                    calib_->checkDomainBoundaryOnImageSpace(*ic, Xc_doi_top_, Xc_doi_bottom_);

                    ic->checkSensitivityOfMappingFunction((Xc_doi_top_-Xc_doi_bottom_)/2., Xc_doi_top_.x-Xc_doi_bottom_.x);
                }

                cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
                for(auto& ic : cam_) ic->saveParamsToYamlFile(imgDir_, true);
            }

            //######################################################################################################
            if(triangulation_test_flag_){
                std::vector<cv::Point3d> Xc_vec;
                int num = 100; 

                for(int i=0; i<num; i++){
                    auto Xcpx=Random::UniformDistributionNumber(Xc_doi_bottom_.x, Xc_doi_top_.x);
                    auto Ycpx=Random::UniformDistributionNumber(Xc_doi_bottom_.y, Xc_doi_top_.y);
                    auto Zcpx=Random::UniformDistributionNumber(Xc_doi_bottom_.z, Xc_doi_top_.z);
                    Xc_vec.push_back(cv::Point3d(Xcpx, Ycpx, Zcpx));
                }

                double old_err_sum=0, new_err_sum=0, nol_err_sum=0, nol2_err_sum=0;
                for(auto& Xc: Xc_vec){
                    cout<<"######################################################"<<endl;
                    cout<<"Xc true "<<Xc<<endl;

                    int ic1 = 1;
                    int ic2 = 3;

                    auto xd1=cam_[ic1]->mappingFunction(Xc);
                    auto xd2=cam_[ic2]->mappingFunction(Xc);

                    std::vector<cv::Point2f>   xc1_u,xc2_u;
                    cv::undistortPoints(std::vector<cv::Point2f>{cv::Point2f(float(xd1.x),float(xd1.y))}, xc1_u, cam_[ic1]->cameraMatrix(), cam_[ic1]->distCoeffs(), cv::noArray(), cam_[ic1]->cameraMatrix());
                    cv::undistortPoints(std::vector<cv::Point2f>{cv::Point2f(float(xd2.x),float(xd2.y))}, xc2_u, cam_[ic2]->cameraMatrix(), cam_[ic2]->distCoeffs(), cv::noArray(), cam_[ic2]->cameraMatrix()); 

                    auto Xc_old = triang::triangulateTwoPoints(cam_, ic1, ic2, xc1_u[0], xc2_u[0]); 
                    cout<<"Xc_old  "<<Xc_old<<endl;
                    cout<<"old err "<<cv::norm(Xc_old-Xc)<<endl;
                    old_err_sum += cv::norm(Xc_old-Xc)/cv::norm(dXc_px_);

                    auto Xc_new = triang::triangulateTwoPoints(cam_, ic1, ic2, xc1_u[0], xc2_u[0], true);
                    cout<<"Xc_new  "<<Xc_new<<endl;
                    cout<<"new err "<<cv::norm(Xc_new-Xc)<<endl;
                    new_err_sum += cv::norm(Xc_new-Xc)/cv::norm(dXc_px_);

                    auto Xc_nol = triang::triangulateTwoPointsNonlinear(cam_, ic1, ic2, Xc_top_, Xc_bottom_, xd1, xd2, Xc_new);
                    cout<<"Xc_nol  "<<Xc_nol<<endl;
                    cout<<"nol err "<<cv::norm(Xc_nol-Xc)<<endl;
                    nol_err_sum += cv::norm(Xc_nol-Xc)/cv::norm(dXc_px_);

                    ic1 = 1;
                    ic2 = 2;
                    int ic3 = 0;
                    int ic4 = 3;

                    xd1=cam_[ic1]->mappingFunction(Xc);
                    xd2=cam_[ic2]->mappingFunction(Xc);
                    auto xd3=cam_[ic3]->mappingFunction(Xc);
                    auto xd4=cam_[ic4]->mappingFunction(Xc);

                    auto Xc_nol2 = triang::triangulateThreeOrFourPointsNonlinear(cam_, ic1, ic2, ic3, ic4, Xc_top_, Xc_bottom_, xd1, xd2, xd3, Xc_new, xd4);
                    cout<<"Xc_nol2  "<<Xc_nol2<<endl;
                    cout<<"nol2 err "<<cv::norm(Xc_nol2-Xc)<<endl;
                    nol2_err_sum += cv::norm(Xc_nol2-Xc)/cv::norm(dXc_px_);
                }

                cout<<"mean old 3D error(px) "<<old_err_sum/static_cast<double>(num)<<endl;
                cout<<"mean new 3D error(px) "<<new_err_sum/static_cast<double>(num)<<endl;
                cout<<"mean nol 3D error(px) "<<nol_err_sum/static_cast<double>(num)<<endl;
                cout<<"mean nol2 3D error(px) "<<nol2_err_sum/static_cast<double>(num)<<endl;
            }

            //######################################################################################################
            if(fundamental_matrix_test_flag_){
                cout<<"########### PROCESSING DAVIS PARTICLE FIELD#################"<<endl;
                dataset_->readSyntheticData(imgDir_, it_deb_, it_tot_);

                std::vector<std::vector<double>> err_betw_cams(n_cam_, std::vector<double>(n_cam_, 0));

                for(int it_seq=it_bkg_.front(); it_seq<=it_tot_; ++it_seq){
                    cout<<"#################################################################"<<endl;
                    cout<<"For "<<it_seq<<"th snapshot"<<endl;
                    cout<<"Find the brightest particles in 3D spaces"<<endl;
                    auto particles_vsc=track::findTheBrightestParticles(num_brightest_, it_seq, dataset_->particles_ref(),
                                                                    Xc_doi_top_, Xc_doi_bottom_);

                    //recompute fundamental matrix
                    if(it_seq==it_bkg_.front()){
                        cout<<"########### GET FUNDAMENTAL MATRIX #########################"<<endl;
                        for(auto& ic : cam_) ic->clearRTEF();
                        for(int ic1=0; ic1<n_cam_; ic1++){
                            for(int ic2=0; ic2<n_cam_; ic2++){
                                cout<<"Between "<<ic1<<"th camera and "<<ic2<<"th camera"<<endl;
                                calib_->getFundamentalMatrix(it_seq, cam_[ic1].get(), cam_[ic2].get(), particles_vsc);
                            }
                        }

                        cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
                        for(auto& ic : cam_)     ic->saveParamsToYamlFile(imgDir_, true);
                    }

                    cout<<"########### COMPUTE FUNDAMENTAL MATRIX ERROR ###############"<<endl;
                    for(int ic1=0; ic1<n_cam_; ic1++){
                        for(int ic2=0; ic2<n_cam_; ic2++){
                            calib_->computeFundamentalMatrixError(it_seq, cam_[ic1].get(), cam_[ic2].get(), particles_vsc, err_betw_cams);
                        }
                    }
                }
                    
                for(int ic1=0; ic1<n_cam_; ic1++){
                    for(int ic2=0; ic2<n_cam_; ic2++){
                        if(ic1!=ic2){
                            cout<<"Error dist 2D(px) between "<<ic1<<"th camera and "<<ic2<<"th camera is ";
                            cout<<err_betw_cams[ic1][ic2]/static_cast<double>(it_tot_)<<endl;
                        }
                    }
                }
            }

            //######################################################################################################
            if(stereo_test_3Dto2D_flag_){
                cout<<"########### PROCESSING DAVIS PARTICLE FIELD#################"<<endl;
                dataset_->readSyntheticData(imgDir_, it_deb_, it_tot_);

                int              vsc_size_avg=0;
                std::vector<int> cam3Dto2D_size_avg(n_cam_, 0);

                int              num_ss = 4;
                for(int it_seq=it_bkg_[0]; it_seq<=it_bkg_[0]+num_ss; ++it_seq){
                    cout<<"#################################################################"<<endl;
                    cout<<"For "<<it_seq<<"th snapshot"<<endl;
                    ptv_->clearDataForNextTimeLevel();

                    cout<<"Find the brightest particles in 3D spaces"<<endl;
                    auto particles_vsc=track::findTheBrightestParticles(num_brightest_, it_seq, dataset_->particles_ref(), 
                                                                        Xc_doi_top_, Xc_doi_bottom_);

                    vsc_size_avg+=particles_vsc.size();

                    cout<<"Find the brightest points on images"<<endl;
                    for(auto& ic : cam_)  ic->readRecord(it_seq, imgDir_, true);
                    ptv_->readRecordsFromCamera(cam_);
                    ptv_->findParticleImagePeak();

                    for(auto& ic : cam_){
                        int num_found_3Dto2D = 0;
                        cout<<"For cam "<<ic->i_cam()<<endl;

                        std::vector<cv::Point2d> ProjPoints;

                        for(const auto& pv: particles_vsc){
                            ProjPoints.push_back( ic->mappingFunction(pv->getCurrentXcoord(it_seq)) );
                        }

                        auto points_cloud = pcl::PointCloud<pcl::PointXY>::Ptr(new pcl::PointCloud<pcl::PointXY>);
                        auto pt_d2 = pcl::PointXY();
                        for(const auto& pt: ptv_->Points_xy()[ic->i_cam()]){
                            pt_d2.x = pt.x;
                            pt_d2.y = pt.y;
                            points_cloud->push_back(pt_d2);
                        }

                        pcl::KdTreeFLANN<pcl::PointXY>     Points_kdtree=pcl::KdTreeFLANN<pcl::PointXY>();
                        Points_kdtree.setInputCloud(points_cloud);

                        double max_threshold=1.;//px
                        auto found_particles_indices = pcl::PointIndices::Ptr(new pcl::PointIndices());
                        std::vector<std::pair<cv::Point2d, cv::Point2d>> results;

                        for(const auto& Pxy: ProjPoints){
                                
                            pcl::PointXY searchPoint;
                            searchPoint.x = Pxy.x;
                            searchPoint.y = Pxy.y;

                            std::vector<int>   pointIdxRadiusSearch;
                            std::vector<float> pointRadiusSquaredDistance;

                            if(Points_kdtree.radiusSearch(searchPoint, max_threshold, pointIdxRadiusSearch, pointRadiusSquaredDistance)){

                                std::vector<std::pair<int, float>> order(pointIdxRadiusSearch.size());

                                for(int i = 0; i < pointIdxRadiusSearch.size(); i++)
                                    order[i] = std::make_pair(pointIdxRadiusSearch[i], pointRadiusSquaredDistance[i]);

                                std::sort(order.begin(), order.end(), [](auto& lop, auto & rop) { return lop.second < rop.second; });

                                int i=0;
                                while(i<order.size())
                                {
                                    auto it = std::find(std::begin(found_particles_indices->indices),
                                                        std::end(found_particles_indices->indices), 
                                                        order[i].first);

                                    if(it == std::end(found_particles_indices->indices)){
                                        auto Pc = cv::Point2d(  (*points_cloud)[order[i].first].x, 
                                                                (*points_cloud)[order[i].first].y );

                                        results.push_back( std::pair<cv::Point2d, cv::Point2d>(Pxy, Pc) );
                                        num_found_3Dto2D++;
                                        found_particles_indices->indices.push_back(order[i].first);
                                        break;
                                    }
                                    else{
                                        i++;
                                    }
                                }
                            }
                    
                        }

                        cam3Dto2D_size_avg[ic->i_cam()]+=num_found_3Dto2D;
                        cout<<"stereo_test_3Dto2D num_found_3Dto2D "<<num_found_3Dto2D<<endl;
                    }
                }

                cout<<"#################################################################"<<endl;
                cout<<"Average 3D VSC particle numbers:             "<<vsc_size_avg/(num_ss+1)<<endl;
                for(int i=0; i<cam3Dto2D_size_avg.size(); i++)
                    cout<<"Average 2D particle numbers found for cam "<<i<<": "<<cam3Dto2D_size_avg[i]/(num_ss+1)<<endl;
            }

            //######################################################################################################
            if(stereo_test_2Dto3D_flag_){
                cout<<"########### PROCESSING DAVIS PARTICLE FIELD#################"<<endl;
                dataset_->readSyntheticData(imgDir_, it_deb_, it_tot_);

                double avg_err=0.;
                int    avg_num=0, vsc_size_avg=0;

                auto reduced_flag   = false;
                auto dup_check_flag = true;
                double search_radius_in_mm = 2.*cv::norm(dXc_px_);
                cout<<"search_radius_in_mm(mm) "<<search_radius_in_mm<<endl;

                int              num_ss = 4;
                for(int it_seq=it_bkg_[0]; it_seq<=it_bkg_[0]+num_ss; ++it_seq){
                    cout<<"#################################################################"<<endl;
                    cout<<"For "<<it_seq<<"th snapshot"<<endl;
                    ptv_->clearDataForNextTimeLevel();

                    cout<<"Find the brightest particles in 3D spaces"<<endl;
                    auto particles_vsc=track::findTheBrightestParticles(num_brightest_, it_seq, dataset_->particles_ref(), 
                                                                        Xc_doi_top_, Xc_doi_bottom_);

                    vsc_size_avg+=particles_vsc.size();

                    cout<<"Find the brightest points on images"<<endl;
                    ptv_->setiterative_triangulation_flag(true);
                    ptv_->setlsq_triangulation_flag(true);

                    for(auto& ic : cam_)  ic->readRecord(it_seq, imgDir_, true);
                    ptv_->readRecordsFromCamera(cam_);

                    if(!spawned_particles_in_one_frame_cloud_->empty()) spawned_particles_in_one_frame_cloud_->clear();

                    cout<<"Find peak... "<<endl;
                    ptv_->findParticleImagePeak();

                    cout<<"Stereo Matching and triangulation... "<<endl;
                    if(ptv_->stereoMatching(cam_, reduced_flag)){

                        ptv_->setTriangulatedParticleField(it_seq, dup_check_flag);
                        ptv_->setFrameField(spawned_particles_in_one_frame_cloud_.get());
                        
                        auto spawned_particles_in_one_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(1.);
                        spawned_particles_in_one_frame_octree.defineBoundingBox(Xc_bottom_.x, Xc_bottom_.y, Xc_bottom_.z, Xc_top_.x, Xc_top_.y, Xc_top_.z); 
                        spawned_particles_in_one_frame_octree.setInputCloud(spawned_particles_in_one_frame_cloud_);
                        spawned_particles_in_one_frame_octree.addPointsFromInputCloud();

                        int num_found_2Dto3D = 0;
                        std::vector<std::pair<cv::Point3d, cv::Point3d>> results;
                        auto found_particles_indices = pcl::PointIndices::Ptr(new pcl::PointIndices());

                        for(const auto& pv: particles_vsc){
                            
                            auto pc = pv->getCurrentXcoord(it_seq);

                            std::vector<int>   result_index;
                            std::vector<float> sqr_distance;                                                                                              
                        
                            if(spawned_particles_in_one_frame_octree.radiusSearch(cv2pcl(pc,0), search_radius_in_mm, result_index, sqr_distance) > 0){

                                std::vector<std::pair<int, float>> order(result_index.size());

                                for(int i = 0; i < result_index.size(); i++)
                                    order[i] = std::make_pair(result_index[i], sqr_distance[i]);

                                std::sort(order.begin(), order.end(), [](auto& lop, auto & rop) { return lop.second < rop.second; });

                                int i=0;
                                while(i<order.size())
                                {
                                    auto it = std::find(std::begin(found_particles_indices->indices),
                                                        std::end(found_particles_indices->indices), 
                                                        order[i].first);

                                    if(it == std::end(found_particles_indices->indices)){
                                        auto Xc = cv::Point3d( spawned_particles_in_one_frame_cloud_->points[order[i].first].x, 
                                                               spawned_particles_in_one_frame_cloud_->points[order[i].first].y,
                                                               spawned_particles_in_one_frame_cloud_->points[order[i].first].z );

                                        results.push_back( std::pair<cv::Point3d, cv::Point3d>(pc, Xc) );
                                        num_found_2Dto3D++;
                                        found_particles_indices->indices.push_back(order[i].first);
                                        break;
                                    }
                                    else{
                                        i++;
                                    }
                                }

                            }

                        }

                        if(!results.empty()){
                            std::vector<double> err_vec;
                            for(auto& pp: results){
                                err_vec.push_back(cv::norm(pp.first-pp.second)/cv::norm(dXc_px_));
                            }

                            auto [min_err, max_err] = std::minmax_element(std::begin(err_vec), std::end(err_vec));
                            cout<<"min  error 3D(px) "<<*min_err<<endl
                                <<"max  error 3D(px) "<<*max_err<<endl;

                            auto mean_err = std::accumulate(std::begin(err_vec), std::end(err_vec), 0.0)/err_vec.size();
                            cout<<"mean error 3D(px) "<<mean_err<<endl;

                            avg_err += mean_err;
                            avg_num += num_found_2Dto3D;
                        }
                        cout<<"stereo_test_2Dto3D num_found_2Dto3D "<<num_found_2Dto3D<<endl;
                    }
                }

                cout<<"#################################################################"<<endl;
                cout<<"Average 2D-3D errors of 3D particles   : "<<avg_err/(num_ss+1)<<endl;
                cout<<"Average 3D VSC particle numbers:         "<<vsc_size_avg/(num_ss+1)<<endl;
                cout<<"Average 2D-3D number of particles found: "<<avg_num/(num_ss+1)<<endl;
            }

            //######################################################################################################
            if(otf_calib_flag_){
                cout<<"###########DO OTF CALIBRATION###############################"<<endl;
                ptv_->setiterative_triangulation_flag(true);
                ptv_->setlsq_triangulation_flag(true);

                std::vector<cv::Point3d> obj;
                std::vector<std::vector<cv::Point2d>> img_cams(cam_.size(), std::vector<cv::Point2d>());

                int num_ss=0;
                for(int it_seq=it_bkg_[0]; it_seq<=it_bkg_[0]+num_ss; ++it_seq){
                    cout<<"###########For "<<it_seq<<"th snapshot #####################"<<endl;
                    ptv_->clearDataForNextTimeLevel();

                    for(auto& ic : cam_) ic->readRecord(it_seq, imgDir_, true);
                    ptv_->readRecordsFromCamera(cam_);
                
                    cout<<"Find peak... "<<endl;
                    ptv_->findParticleImagePeak();

                    cout<<"Stereo Matching and triangulation... "<<endl;
                    if(ptv_->stereoMatching(cam_, false)){
                        cout<<"collect samples for OTF"<<endl;
                        calib_->collectSamplePointsForOTF(ptv_->CorrespondentWorldPoints(),
                                                            ptv_->CorrespondentImagePoints(),
                                                            obj, img_cams);
                    }
                }

                for(auto& ic : cam_){
                    int idx_c=0;
                    if(ic->i_cam()==cam_seq_[0])      idx_c=0;
                    else if(ic->i_cam()==cam_seq_[1]) idx_c=1;
                    else if(ic->i_cam()==cam_seq_[2]) idx_c=2;
                    else if(ic->i_cam()==cam_seq_[3]) idx_c=3;

                    calib_->calibrateOTF(ic.get(), OTF_calib_type_, obj, img_cams[idx_c]);
                }

                cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
                for(auto& ic : cam_) ic->saveParamsToYamlFile(imgDir_, true);

                calib_->testOTF(cam_, 10, cam_seq_, obj, img_cams, dXc_px_);
            }
            else{
                if(case_name_=="lptchal4500"){
                    std::string calib_dir=str(boost::format("%1%/data/%2%/calib_results") % rootDir_ % case_name_);
                    cout<<"Copy cameras' parameter files from "<<calib_dir<<endl;
                    for(int i=0; i<n_cam_; i++){
                        auto src_path=calib_dir+"/camera"+std::to_string(i+1)+"_preproc.yaml";
                        auto dst_path=imgDir_+"/camera"+std::to_string(i+1)+"_preproc.yaml";
                        boost::filesystem::copy_file(src_path, dst_path, boost::filesystem::copy_option::overwrite_if_exists);
                    }
                }
            }
        }
};

}//namespace PTL

int main(){
    auto app=std::make_unique<PTL::PreProc_App>();

    app->initialize();

    auto t_deb=Clock::now();
    app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of preprocessing: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}