#!/bin/bash

testfile=""

while getopts ":f:" option
do
	case $option in
		f) testfile=$OPTARG;;
			\?) exit 1;;
	esac
done

testcmd="cxxtestgen --error-printer -o ${STBENSPTV_PATH}/unit_tests/test.cpp ${testfile}"

echo $testcmd
eval $testcmd
