#ifndef DATASET
#define DATASET

#include "Camera.hpp"

class Calibration;

namespace PTL{

///  Dataset class
class Dataset
{
    public:
        enum class Source_Type      	{FromLPTChallenge};
        enum class Init_Track_Type  	{Hacker, Triangulation};
        enum class Data_Format_Type 	{NPZ, TXT_ONEFILE, TXT};

	protected:
		double PPP_ = 0.;
        std::vector<Particle_ptr> particles_ref_;
        std::vector<Particle_ptr> particles_bkg_;

	public:
		Dataset(){};

		~Dataset(){};

		virtual void setIMGData(const std::string&, int, int, const std::vector<Camera_ptr>&, const std::string&){};

		virtual void readOTF(const std::string&, Camera*){};
		virtual void readPolynomialMF(const std::string&, Camera*){};
		virtual void readPinholeMF(const std::string&, Camera*){};

		virtual void readMarkPositionTable(const std::string&, int, 
											std::vector<std::vector<std::vector<cv::Point3d>>>&,
											std::vector<std::vector<std::vector<cv::Point2d>>>&, int nx_rig=0, int ny_rig=0){};

		virtual	void setRigIMGData(const std::string&, int, const std::string&){};

		void readParticleData(const std::string&, int, int, Data_Format_Type, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);

        void readBackgroundData(const std::string&, const std::vector<int>&, bool, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);
        void readSyntheticData(const std::string&, int, int, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);
        void saveSyntheticData(const std::string&, int, const std::vector<int>&);

		void readBackgroundDataTXT(const std::string&, const std::vector<int>&, bool, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);

        const std::vector<Particle_ptr>&           particles_bkg(void) const { return particles_bkg_; }
        const std::vector<Particle_ptr>&           particles_ref(void) const { return particles_ref_; }

        std::vector<Particle_ptr>&                 get_particles_bkg(void) { return particles_bkg_; }
        std::vector<Particle_ptr>&                 get_particles_ref(void) { return particles_ref_; }

    protected:
    	void readParticleDataNPZ(const std::string&, int, int);
     	void readParticleDataTXT(const std::string&, int, int);
      	void readParticleDataTXT_ONEFILE(const std::string&, int, int, const std::optional<cv::Point3d>&, const std::optional<cv::Point3d>&);
};

inline void Dataset::readParticleData(const std::string& path, int it_deb, int it_tot, Data_Format_Type data_format_type, 
							const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom){
	switch(data_format_type){
		case Data_Format_Type::NPZ: 		this->readParticleDataNPZ(path, it_deb, it_tot); break;
		case Data_Format_Type::TXT_ONEFILE:	this->readParticleDataTXT_ONEFILE(path, it_deb, it_tot, Xc_top, Xc_bottom); break;
		case Data_Format_Type::TXT: 		this->readParticleDataTXT(path, it_deb, it_tot); break;
	}
}

inline void Dataset::readParticleDataNPZ(const std::string& imgdir, int it_deb, int it_tot){

    std::map<int, Particle_ptr> particles_ref_map;

    std::string refdir(imgdir+"/ref");

    for(int it_seq=it_deb; it_seq<=it_tot; it_seq++){
        auto refPhaseFile=str(boost::format("%1%/ref_it%2$03d.npz") % refdir % it_seq );
        utils::readFrameDataNPZ(it_seq, it_deb, refPhaseFile, particles_ref_map);
    }

    if(!particles_ref_.empty()) particles_ref_.clear();
    for(const auto& pr_m: particles_ref_map){
        particles_ref_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
    }

    cout<<"Read fin"<<endl
        <<"TOTAL particle number for ALL TIME STEPS "<<particles_ref_map.size()<<endl;
}

inline void Dataset::readParticleDataTXT(const std::string& data_path, int it_deb, int it_tot){

    if(!particles_ref_.empty()) particles_ref_.clear();

	std::string X, Y, Z, track_id, line;

	int p_id;
    std::vector<int> p_id_vec;
    cv::Point3d Xc;

	cout<<"Read debut ..."<<endl;

	auto PPP_str = PPP_*1000;

	for(int it=it_deb; it<=it_tot; it++){

		int it_seq=it-1;

		auto data_file=str(boost::format("%1$sDA_ppp_0_%2$03d/DA_ppp_0_%2$03d_PartFile_%3$04d.dat") % data_path % PPP_str % it_seq);
		cout<<"data_file "<<data_file<<endl;

		std::ifstream dataFile(data_file);

		it_seq++;
		cout<<"Read snapshot "<<it_seq<<endl;

		if(dataFile.is_open()){
			while(std::getline(dataFile, line)){

				std::string::size_type nc=line.find("Title");
				if(nc != std::string::npos)
					line.clear();

				nc=line.find("Variables");
				if(nc != std::string::npos)
					line.clear();

				nc=line.find("Zone");
				if(nc != std::string::npos)
					line.clear();

				if(!line.empty()){
					std::istringstream is(line);
					if(is >> X >> Y >> Z >> track_id){						
						p_id=std::stoi(track_id);
						Xc=cv::Point3d( std::stod(X), std::stod(Y), std::stod(Z) );

						if(it_seq==it_deb){
							// if(Xc.x >= Lim_bottom.x && Xc.x <= Lim_top.x && Xc.y >= Lim_bottom.y && Xc.y <= Lim_top.y && Xc.z >= Lim_bottom.z && Xc.z <= Lim_top.z){
							p_id_vec.push_back(p_id);
							particles_ref_.push_back( std::make_unique<Particle>( p_id, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{cv::Point3d(0,0,0)}, std::vector<double>{1.} ) );						
							// }
						}
						else{
							auto it=std::find(p_id_vec.begin(), p_id_vec.end(), p_id);
							if(it != p_id_vec.end()){
								auto p_idx=std::distance(p_id_vec.begin(), it);
								particles_ref_[p_idx]->setit_seq_fin(it_seq);
								particles_ref_[p_idx]->push_backXcoordElement(Xc, it_seq);
								particles_ref_[p_idx]->push_backVElement(cv::Point3d(0,0,0), it_seq);
								particles_ref_[p_idx]->push_backEvElement(1., it_seq);
							}
							else{
								// if(Xc.x >= Lim_bottom.x && Xc.x <= Lim_top.x && Xc.y >= Lim_bottom.y && Xc.y <= Lim_top.y && Xc.z >= Lim_bottom.z && Xc.z <= Lim_top.z){
								p_id_vec.push_back(p_id);
								particles_ref_.push_back( std::make_unique<Particle>( p_id, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{cv::Point3d(0,0,0)}, std::vector<double>{1.} ) );						
								// }
							}
						}
					}
				}
			}

		    dataFile.close();
		}
		else{
			std::cerr<<"Error opening data file "<<data_file<<endl;
			std::abort();
		}
	}

	cout<<"Read fin"<<endl
		<<"TOTAL particle number for ALL TIME STEPS "<<p_id_vec.size()<<endl;

	assert(p_id_vec.size()==particles_ref_.size());
}

inline void Dataset::readParticleDataTXT_ONEFILE(const std::string& data_file, int it_deb, int it_tot, 
	                             				const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom){

    if(!particles_ref_.empty()) particles_ref_.clear();

    std::map<int, Particle_ptr> particles_ref_map;

	std::ifstream dataFile(data_file);

	std::string X, Y, Z, I, u, v, w, Vabs, track_id, ax, ay, az, aabs, line;

	int it_seq=it_deb-1;

	int p_part_index;
	double E;
    cv::Point3d Xc, Vp;

	cout<<"Read debut ..."<<endl;

	double Lim_E=0.;

	if(dataFile.is_open()){
		while(std::getline(dataFile, line)){

			std::string::size_type nc=line.find("TITLE");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("VARIABLES");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("STRANDID");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("I=");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("DATAPACKING");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("ZONE");
			if(nc != std::string::npos){
				it_seq++;
				cout<<"Read snapshot "<<it_seq<<endl;
			}
			
			if(it_seq<=it_tot && !line.empty()){

				std::istringstream is(line);
				if(is >> X >> Y >> Z >> I >> u >> v >> w >> Vabs >> track_id >> ax >> ay >> az >> aabs){

					p_part_index=std::stoi(track_id);
					Xc=cv::Point3d( std::stod(X), std::stod(Y), std::stod(Z) );
					Vp=cv::Point3d( std::stod(u), std::stod(v), std::stod(w) );
					E=std::stod(I);

					if(it_seq==it_deb){
						if(Xc.x >= Xc_bottom.value().x && Xc.x <= Xc_top.value().x && Xc.y >= Xc_bottom.value().y && Xc.y <= Xc_top.value().y && Xc.z >= Xc_bottom.value().z && Xc.z <= Xc_top.value().z && E>=Lim_E){
							particles_ref_map.insert({p_part_index, std::make_unique<Particle>( p_part_index, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{Vp}, std::vector<double>{E} )});
						}
					}
					else{
						auto it=particles_ref_map.find(p_part_index);
						if(it != particles_ref_map.end()){
							it->second->setit_seq_fin(it_seq);
							it->second->push_backXcoordElement(Xc, it_seq);
							it->second->push_backVElement(Vp, it_seq);
							it->second->push_backEvElement(E, it_seq);
						}
						else{
							if(Xc.x >= Xc_bottom.value().x && Xc.x <= Xc_top.value().x && Xc.y >= Xc_bottom.value().y && Xc.y <= Xc_top.value().y && Xc.z >= Xc_bottom.value().z && Xc.z <= Xc_top.value().z && E>=Lim_E){
								particles_ref_map.insert({p_part_index, std::make_unique<Particle>( p_part_index, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{Vp}, std::vector<double>{E} )});
							}
						}

					}
				}	
			}
		}
	}
	else{
		std::cerr<<"Error opening data file "<<data_file<<endl;
		std::abort();
	}

	for(const auto& pr_m: particles_ref_map){
		particles_ref_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
	}

	cout<<"Read fin"<<endl
		<<"TOTAL particle number for ALL TIME STEPS "<<particles_ref_map.size()<<endl;
}


inline void Dataset::readSyntheticData(const std::string& imgdir, int it_deb, int it_tot, 
                                     const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom){

    std::map<int, Particle_ptr> particles_ref_map;

    std::string syndir(imgdir+"/syn");

    for(int it_seq=it_deb; it_seq<=it_tot; it_seq++){
        auto synPhaseFile=str(boost::format("%1%/syn_it%2$03d.npz") % syndir % it_seq );
        utils::readFrameDataNPZ(it_seq, it_deb, synPhaseFile, particles_ref_map);
    }

    if(!particles_ref_.empty()) particles_ref_.clear();
    for(const auto& pr_m: particles_ref_map){
        particles_ref_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
    }

    cout<<"total particle: particles_ref_.size() "<<particles_ref_.size()<<endl;

    if(Xc_top && Xc_bottom){
        for(int p=0; p<particles_ref_.size(); p++){
            auto it_doi_out = std::find_if(particles_ref_[p]->Xcoord().begin(), particles_ref_[p]->Xcoord().end(), 
                                            [=](const auto& Xc){ return checkParticleIsOutOfRange(Xc, Xc_top.value(), Xc_bottom.value()); });

            if(it_doi_out!=particles_ref_[p]->Xcoord().end()){
                particles_ref_[p]->deleteData(true);
                particles_ref_[p]->markAsGhost();
            }
        }
    }
}

inline void Dataset::readBackgroundData(const std::string& imgdir, const std::vector<int>& it_bkg, bool reconstructed_flag,
                                     const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom)
{
    std::map<int, Particle_ptr> particles_bkg_map;
    std::string bkgdir(imgdir);
    bkgdir+=(reconstructed_flag ? "/bkg_recon" : "/bkg");

    for(int it_seq=it_bkg.at(0); it_seq<=it_bkg.back(); it_seq++){
        auto bkgPhaseFile=str(boost::format("%1%/bkg_it%2$03d.npz") % bkgdir % it_seq );
        utils::readFrameDataNPZ(it_seq, it_bkg[0], bkgPhaseFile, particles_bkg_map);
    }

    if(!particles_bkg_.empty()) particles_bkg_.clear();
    for(const auto& pr_m: particles_bkg_map){
        particles_bkg_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
    }

    cout<<"bkg particles: particles_bkg_.size() "<<particles_bkg_.size()<<endl;

    if(Xc_top && Xc_bottom){
        for(int p=0; p<particles_bkg_.size(); p++){
            auto it_doi_out = std::find_if(particles_bkg_[p]->Xcoord().begin(), particles_bkg_[p]->Xcoord().end(), 
                                            [=](const auto& Xc){ return checkParticleIsOutOfRange(Xc, Xc_top.value(), Xc_bottom.value()); });

            if(it_doi_out!=particles_bkg_[p]->Xcoord().end()){
                particles_bkg_[p]->deleteData(true);
                particles_bkg_[p]->markAsGhost();
            }
        }
    }
}

inline void Dataset::saveSyntheticData(const std::string& imgdir, int it_tot, const std::vector<int>& it_bkg){

    std::string syndir(imgdir+"/syn");
    if(!boost::filesystem::exists(syndir)){
        boost::filesystem::create_directories(syndir);
    }

    std::string bkgdir(imgdir+"/bkg");
    if(!boost::filesystem::exists(bkgdir)){
        boost::filesystem::create_directories(bkgdir);
    }

    for(int it=it_bkg[0]; it<=it_tot; it++){
        auto synPhaseFile=str(boost::format("%1%/syn_it%2$03d.npz") % syndir % it );
        utils::saveFrameDataNPZ(it, synPhaseFile, particles_ref_);

        if(it<=it_bkg.back()){
            auto bkgPhaseFile=str(boost::format("%1%/bkg_it%2$03d.npz") % bkgdir % it );
            utils::saveFrameDataNPZ(it, bkgPhaseFile, particles_ref_);
        }
    }
}

inline void Dataset::readBackgroundDataTXT(const std::string& imgdir, const std::vector<int>& it_bkg, bool reconstructed_flag,
                                		const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom)
{
    std::string line;
    std::vector<std::string> par_index_v, it0_v, itf_v, X_v, Y_v, Z_v, E_v;
    std::string par_index, it0, itf, X, Y, Z, E; 

    std::string bkgPhaseFile;
    std::string bkgdir(imgdir);
    bkgdir+=(reconstructed_flag ? "/bkg_recon" : "/bkg");

    int n_particles_bkg=0;

    for(int i=it_bkg.at(0); i<it_bkg.at(0)+it_bkg.size(); i++){
        bkgPhaseFile=str(boost::format("%1%/bkg_it%2$03d.txt") % bkgdir % i );
        std::ifstream bkgFile(bkgPhaseFile);

        if(bkgFile.is_open()){
            while(std::getline(bkgFile, line)){
                std::string::size_type nc=line.find("#");
                if(nc != std::string::npos)
                    line.erase(nc);

                std::istringstream is(line);
                if(is >> par_index >> it0 >> itf >> X >> Y >> Z >> E){
                    par_index_v.push_back(par_index);
                    it0_v.push_back(it0);
                    itf_v.push_back(itf);
                    X_v.push_back(X);
                    Y_v.push_back(Y);
                    Z_v.push_back(Z);
                    E_v.push_back(E);               
                }           
            }
        }
        else{
            std::cerr<<"Error opening background data file "<<bkgPhaseFile<<endl;
            std::abort();
        }

        if(i==it_bkg[0])
            n_particles_bkg=par_index_v.size();
    }

    for(int p=0; p<n_particles_bkg; p++){
        auto p_part_index=std::stoi(par_index_v[p]);
        auto p_it0=std::stoi(it0_v[p]);
        auto p_itf=std::stoi(itf_v[p]);

        std::vector<cv::Point3d> Xcoord, Xcoord_sub;
        std::vector<double>      Ev, Ev_sub;

        for(int it=0; it<it_bkg.size(); it++){
            auto Xc=cv::Point3d( std::stod(X_v[it*n_particles_bkg+p]), std::stod(Y_v[it*n_particles_bkg+p]), std::stod(Z_v[it*n_particles_bkg+p]) );
            Xcoord.push_back(Xc);
            Ev.push_back(std::stod(E_v[it*n_particles_bkg+p]));
        }

        if(p_it0 <= it_bkg.back()){
            auto iter_begin = p_it0-it_bkg.at(0);
            auto iter_end   = std::max(0, it_bkg.back()-p_itf);

            Xcoord_sub.assign(Xcoord.begin()+iter_begin, Xcoord.end()-iter_end);
            Ev_sub.assign(Ev.begin()+iter_begin, Ev.end()-iter_end);
            particles_bkg_.push_back( std::make_unique<Particle>( p_part_index, p_it0, p_itf, Xcoord_sub, Ev_sub ) );
        }
    }

    cout<<"n_particles_bkg "<<n_particles_bkg<<endl;
    cout<<"bkg particles: particles_bkg_.size() "<<particles_bkg_.size()<<endl;

    if(Xc_top && Xc_bottom){
        for(int p=0; p<particles_bkg_.size(); p++){
            auto it_doi_out = std::find_if(particles_bkg_[p]->Xcoord().begin(), particles_bkg_[p]->Xcoord().end(), 
                                            [=](const auto& Xc){ return checkParticleIsOutOfRange(Xc, Xc_top.value(), Xc_bottom.value()); });

            if(it_doi_out!=particles_bkg_[p]->Xcoord().end()){
                particles_bkg_[p]->deleteData(true);
                particles_bkg_[p]->markAsGhost();
            }
        }
    }

}

}//namespace PTL
#endif