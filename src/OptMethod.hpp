#ifndef OPTMETHOD
#define OPTMETHOD

#include "Calibration.hpp"
#include "DynModel.hpp"
#include "PTV.hpp"

namespace PTL{

class OptMethod
{
	public:
        enum class Method_Type              {STB, ENS};
		enum class ControlVector_Type		{LagrangianLocation};

	protected:
        Method_Type          					method_type_;
		ControlVector_Type   					control_vector_type_;
		bool 									intensity_control_flag_;
		int								   	    n_particles_;
		int 									n_particles_per_snapshot_;
		cv::Point3d 		 					Xc_top_;
		cv::Point3d 		 					Xc_bottom_;
        double 								    pixel_lost_;
        double 				 					dtObs_;
        cv::Point3d          					dXc_px_;
		std::string 							res_path_;
		int 									eval_window_warm_start_=3;
		int 									eval_window_fine_search_;

		bool 									warm_start_flag_;
		int 									warm_start_max_iter_;
		double  								warm_start_pixel_;
		double  								warm_start_relaxfactor_;
		double  								warm_start_ratio_valve_=0.1;

		bool 									martlike_intensity_flag_=false;

		double 									avg_part_dist_;
        double 									characteristic_length_;

        std::vector<double>  					res_glob_;
        int 				 					lMax_;
        int  				 					kMax_;

        MatrixXd 							    res_parti_;
        cv::Point3d 		 					velo_max_;
        cv::Point3d			 					velo_min_;

        std::vector<int> 						n_detected_particles_;
        std::vector<int> 					    n_lost_particles_;
	    std::vector<int> 						n_new_particles_;
	    std::vector<int> 						n_append_particles_;
	    int 									total_tracks_; 

	    std::mutex 								m_mutex_;

		bool									ipr_full_flag_=false;

		double 									intensity_average_=100;

		double 									intensity_min_threshold_=0.1;
		double 									intensity_deletion_threshold_=0.1;

		double 									intensity_min_threshold_ratio_=0.15;
		double 									intensity_deletion_threshold_ratio_=0.05;
		double 									intensity_variation_ratio_threshold_=0;

		double 									num_avg_dist_=3.;
		int 									num_min_voisin_=2;
		int 									num_max_voisin_=15;

		double									num_pixels_radius_with_predictor_=4.;
		double 									num_pixels_radius_without_predictor_=18.;

	    bool									ptv_min_intensity_threshold_flag_=false;
	    bool 								    ptv_stereoMatching_reduced_flag_=false;
	    bool									ptv_FrameField_duplicate_check_flag_=false;

	    bool 									addNewParticles_before_final_iteration_flag_=false;

	    std::vector<pcl::PointXYZI>    			spawned_particles_in_one_frame_;
	    std::vector<pcl::PointXYZI>	  		    tracked_particles_coord_in_one_frame_; 

	    pcl::PointCloud<pcl::PointXYZI>::Ptr 		   spawned_particles_in_one_frame_cloud_ = pcl::make_shared<pcl::PointCloud<pcl::PointXYZI>>();
        pcl::PointCloud<pcl::PointXYZHSV>::Ptr         tracked_particles_coord_velo_in_one_frame_cloud_  = pcl::make_shared<pcl::PointCloud<pcl::PointXYZHSV>>();

        double 										   velo_mag_norm_=1.;
        cv::Point3d 								   velo_mag_abs_variation_;

	    int                                            stable_long_track_length_spatial_;
	    int 										   stable_long_track_length_temporal_;
        int 										   min_num_near_velo_;
        double 										   max_err_v_ratio_;
        double 										   min_err_v_ratio_;
		double 										   max_err_V_angle_;
		double 										   min_err_V_angle_;
		double 										   linear_fit_error_threshold_;
		double                                         one_pixel_velo_norm_;

		std::vector<Particle_ptr> 					   particles_new_;

        std::map<int, Particle_ptr> 				   particles_field_map_;

    private:
	    std::vector<int> 							   warm_start_do_flag_;
	    cv::Point3d              					   warm_start_ratio_;
    	std::vector<double> 						   res_prev_;

	public:
		OptMethod();

		virtual ~OptMethod() = 0;

		OptMethod(Method_Type, ControlVector_Type, bool, 
				  int, int, const cv::Point3d&, const cv::Point3d&,
				  double, double, 
				  const cv::Point3d&, const std::string&, int,
  				  bool, int, double, double);

		OptMethod(Method_Type, ControlVector_Type, bool,
		 		  int, int, const cv::Point3d&, const cv::Point3d&, 
		 		  const cv::Point3d&, const std::string&, int);
		
		virtual	void initialization(std::vector<Particle_ptr>&,  int, bool use_in_lapiv=false) = 0;
		virtual void predict(DynModel*, int, int) = 0;
		virtual	void prepare(int, const std::vector<Camera_ptr>&, int lapiv_it=0) = 0;
		
		virtual void selectWarmStart(int, const std::vector<Camera_ptr>&) = 0;

		virtual void core(int, int, const std::vector<Camera_ptr>&, bool) {};
		virtual void postIPRResidual(int, int, const std::vector<Camera_ptr>&) {};
		virtual void postIPRFull(int) {};

		virtual void clearParticleData(void) = 0;
		virtual void checkIfEnsembleParticlesAppearOnSnapshot(int){};
		virtual void maintainPositionInRange(int){};

		virtual const std::vector<Particle_ptr>& particles_field(void) const = 0;
		virtual std::vector<Particle_ptr>& get_particles_field(void) = 0;

		virtual const int& opt_max_iter(void) const = 0;
		
		virtual bool checkIfParticleIsToBeOptimized(Particle* pp){
			return (pp->is_to_be_tracked() || pp->is_tracked() || pp->is_triangulated());
		}

		virtual	void update(Particle*, int, int, const std::vector<Camera_ptr>&, double& dummy) = 0;

		virtual void projectParticlesToImageSpace(int, const std::vector<Particle_ptr>&, const std::vector<Camera_ptr>&);
		virtual	void updateParticles(int, const std::vector<Particle_ptr>&, const std::vector<Camera_ptr>&, std::vector<double>& dummy);

		void clear(void){ 
			std::vector<double>().swap(res_glob_);
			n_detected_particles_.clear();
			n_lost_particles_.clear();
			n_new_particles_.clear();
			n_append_particles_.clear();
			spawned_particles_in_one_frame_cloud_->clear();
		}

		void predictIntensity(int);

		void saveGlobalResidual(int, bool);
		
		void predictParticleUsingLinearPredictor(int, std::vector<Particle_ptr>&);

		void predictParticlesUsingNeighboringInformation(int, std::vector<Particle_ptr>&);
	    
	    void appendParticles(int, std::vector<Particle_ptr>&);

		void addNewParticles(int, std::vector<Particle_ptr>&);

		void printTrackInformation(int);

		void rescaleIntensityUsingTarget(Particle*, int, const std::vector<Camera_ptr>&,  bool only_scale_large_variation_flag=false);

		void removeParticlesThatAreLostOrGhost(int, const std::vector<Particle_ptr>&);

		void processingResidualImage(const std::vector<Camera_ptr>&, int, 
                                	const std::vector<Particle_ptr>&, int,
                                	bool, bool save_flag=false, int lapiv_it=0);

		void processingImageResidualParticle(const std::vector<Particle_ptr>&, int, const std::vector<Camera_ptr>&);

		void processingLostParticles(int, const std::vector<Particle_ptr>&, const std::vector<Camera_ptr>&);

        void computeImageResidualParticle(Particle*, int, const std::vector<Camera_ptr>&);

		double correctIntensityMARTlike(Particle*, int, const std::vector<Camera_ptr>&);

        double jumpToMinimumResidual(Particle*, const cv::Point3d&, int, const std::vector<Camera_ptr>&);

		void setres_parti(int, int, const std::vector<Particle_ptr>&, const std::vector<Camera_ptr>&);

		virtual bool checkConvergence(int, int, double delta_res_threshold=0.);

		void doWarmStart(int, const std::vector<Camera_ptr>&, const std::vector<Particle_ptr>&);

		void computeParticleVelocity(int);

		VectorXd solve(const MatrixXd&, const MatrixXd&);

		void setintensity_threshold(const std::vector<Particle_ptr>&);

		double computeAverageIntensity(const std::vector<Particle_ptr>&);

		void removeOutlier(int, const std::vector<Particle_ptr>&, int k);

		void setRemoveOutlierParams(int stable_long_track_length_spatial=20, int stable_long_track_length_temporal=10,
									int min_num_near_velo=3, double max_err_v_ratio=5., double min_err_v_ratio=2.,
									double max_err_V_angle=CV_PI/2., double min_err_V_angle=CV_PI/2.,
									double linear_fit_error_threshold=0.15,
									double one_pixel_velo_norm=50){
			stable_long_track_length_spatial_=stable_long_track_length_spatial;
			stable_long_track_length_temporal_=stable_long_track_length_temporal;
			min_num_near_velo_=min_num_near_velo;
			max_err_v_ratio_=max_err_v_ratio;
			min_err_v_ratio_=min_err_v_ratio;
			max_err_V_angle_=max_err_V_angle;
			min_err_V_angle_=min_err_V_angle;
			linear_fit_error_threshold_=linear_fit_error_threshold;
			one_pixel_velo_norm_=one_pixel_velo_norm;
		}

		void assertVelocityQualityCheck(int, const std::vector<Particle_ptr>&, bool verbal_flag=false);

	private:
		void doWarmStartInnerLoop(Particle*, int, int, int, const std::vector<Camera_ptr>&);
		
		bool qualifyDiscrepancyComparedToAnchorVelocity(const cv::Point3d&, const cv::Point3d&, bool verbal_flag=false);
	
		void checkSpatialCoherence(Particle*, const pcl::octree::OctreePointCloudSearch<pcl::PointXYZHSV>&, int, int, bool verbal_flag=false);
		void checkTemporalCoherence(Particle*, int, int, bool verbal_flag=false);

		bool checkIfParticleVelocityIsCoherentWithItsNeighbors(Particle*, int, const std::vector<int>&, const std::vector<double>, double,
                                                                std::vector<cv::Point3d>&, cv::Point3d&, bool verbal_flag=false);

		bool qualifyLinearFitErrorsOfLastFourSnapshots(Particle*, int, bool verbal_flag=false);

		void reestimateParticlePositionUsingSmoothSpline(Particle*, int, const std::vector<cv::Point3d>&, bool verbal_flag=false);
		void reestimateParticlePositionUsingNeighborAverage(Particle*, int, const cv::Point3d&, double relax_factor=1.);
		void reestimateParticlePositionUsingPolynomial(Particle*, int, bool verbal_flag=false);
		void reestimateParticlePositionUsingLinearPredictor(Particle*, int, int, bool verbal_flag=false);
		
	public:
		const int& n_particles(void) const { return n_particles_; }
		const int& n_particles_per_snapshot(void) const { return n_particles_per_snapshot_; }
		const std::vector<double>& res_glob(void) const { return res_glob_; }
		const Method_Type& method_type(void) const { return method_type_; }
		const bool& warm_start_flag(void) const { return warm_start_flag_; }
		const bool& addNewParticles_before_final_iteration_flag(void) const { return addNewParticles_before_final_iteration_flag_; }
		const bool& ptv_min_intensity_threshold_flag(void) const { return ptv_min_intensity_threshold_flag_; }
		const bool& ptv_stereoMatching_reduced_flag(void) const { return ptv_stereoMatching_reduced_flag_; }
		const bool& ptv_FrameField_duplicate_check_flag(void) const { return ptv_FrameField_duplicate_check_flag_; }

		const bool& ipr_full_flag(void) const { return ipr_full_flag_; }

	    const pcl::PointCloud<pcl::PointXYZI>::Ptr& spawned_particles_in_one_frame_cloud(void) const { return spawned_particles_in_one_frame_cloud_; }

		const int& lMax(void) const { return lMax_; }
		const int& kMax(void) const { return kMax_; }

		const cv::Point3d& Xc_top(void) const { return Xc_top_; }
		const cv::Point3d& Xc_bottom(void) const { return Xc_bottom_; }
		const cv::Point3d& dXc_px(void) const { return dXc_px_; }

		const std::vector<int>& n_append_particles(void) const { return n_append_particles_; }
		const std::vector<int>& n_new_particles(void) const { return n_new_particles_; }

		const std::vector<Particle_ptr>& particles_new(void) const { return particles_new_; };

		void  push_backn_append_particles(int n){ n_append_particles_.push_back(n); }
		void  push_backn_new_particles(int n){ n_new_particles_.push_back(n); }

        void  setlMax(int lMax){ lMax_=lMax; }
        void  setkMax(int kMax){ kMax_=kMax; }

        void  setn_particles(int n_particles){ n_particles_=n_particles; n_particles_per_snapshot_=n_particles; }
		void  setvelo_maxmin(const cv::Point3d& velo_max, const cv::Point3d& velo_min){ velo_max_=velo_max;  velo_min_=velo_min; }
		void  setintensity_min_threshold_ratio(double intensity_min_threshold_ratio){ intensity_min_threshold_ratio_=intensity_min_threshold_ratio; };		
		void  setintensity_deletion_threshold_ratio(double intensity_deletion_threshold_ratio){ intensity_deletion_threshold_ratio_=intensity_deletion_threshold_ratio; };		
		void  setintensity_variation_ratio_threshold(double intensity_variation_ratio_threshold){ intensity_variation_ratio_threshold_=intensity_variation_ratio_threshold; };
		void  setnum_pixels_radius_with_predictor(double num_pixels_radius_with_predictor){ num_pixels_radius_with_predictor_=num_pixels_radius_with_predictor; }
		void  setnum_pixels_radius_without_predictor(double num_pixels_radius_without_predictor){ num_pixels_radius_without_predictor_=num_pixels_radius_without_predictor; }
		void  setptv_min_intensity_threshold_flag(bool ptv_min_intensity_threshold_flag){ ptv_min_intensity_threshold_flag_=ptv_min_intensity_threshold_flag; }
		void  setptv_stereoMatching_reduced_flag(bool ptv_stereoMatching_reduced_flag){ ptv_stereoMatching_reduced_flag_=ptv_stereoMatching_reduced_flag; }
		void  setptv_FrameField_duplicate_check_flag(bool ptv_FrameField_duplicate_check_flag){ ptv_FrameField_duplicate_check_flag_=ptv_FrameField_duplicate_check_flag; }
		void  setparticles_new(std::vector<Particle_ptr>& particles_new){ if(!particles_new_.empty()){ particles_new_.clear(); } particles_new_=std::move(particles_new); }
		void  setaddNewParticles_before_final_iteration_flag(bool addNewParticles_before_final_iteration_flag){ addNewParticles_before_final_iteration_flag_=addNewParticles_before_final_iteration_flag; }
        void  setnum_avg_dist(double num_avg_dist){ num_avg_dist_=num_avg_dist; }
};

}//namespace PTL
#endif