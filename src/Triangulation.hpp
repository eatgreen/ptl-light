#ifndef TRIANGULATION
#define TRIANGULATION

#include "Camera.hpp"

namespace PTL{

namespace triang{

    class NonlinearMappingFunctionZPlaneTriangulationCostFunctor : public ceres::SizedCostFunction<1,3> {
        public:
            NonlinearMappingFunctionZPlaneTriangulationCostFunctor(double PixelPerMmFactor, const std::vector<double>& ZPosZ, const cv::Point2d& originZ, const cv::Point2d& normalizationFactorZ, const VectorXd& aZ0, const VectorXd& aZ1, const VectorXd& bZ0, const VectorXd& bZ1, const cv::Point2d& xc_real, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom):
                PixelPerMmFactor_(PixelPerMmFactor), ZPosZ_(ZPosZ), originZ_(originZ), normalizationFactorZ_(normalizationFactorZ), aZ0_(aZ0), aZ1_(aZ1), bZ0_(bZ0), bZ1_(bZ1), xc_real_(xc_real), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom) {}

            virtual ~NonlinearMappingFunctionZPlaneTriangulationCostFunctor(){}

            virtual bool Evaluate(double const* const* parameters, double* residual, double** jacobians) const {
                const double X = parameters[0][0];
                const double Y = parameters[0][1];
                const double Z = parameters[0][2];

                if(X>Xc_bottom_.x && X<Xc_top_.x && Y>Xc_bottom_.y && Y<Xc_top_.y && Z>Xc_bottom_.z && Z<Xc_top_.z){
                    auto kX=PixelPerMmFactor_*X;
                    auto kY=PixelPerMmFactor_*Y;
                    auto kZ=PixelPerMmFactor_*Z;

                    auto Xv= kX+originZ_.x;
                    auto Yv=-kY+originZ_.y;
                    auto Zv=kZ;

                    double dsdX =  2.*PixelPerMmFactor_/normalizationFactorZ_.x;
                    double dtdY = -2.*PixelPerMmFactor_/normalizationFactorZ_.y;

                    double s = dsdX*X;
                    double t = dtdY*Y;

                    VectorXd design_mat(10);

                    design_mat<<1, s, std::pow(s,2), std::pow(s,3),
                                t, std::pow(t,2), std::pow(t,3),
                                s*t, std::pow(s,2)*t, std::pow(t,2)*s; 

                    double dx_Z[2]={0,0}, dy_Z[2]={0,0};

                    dx_Z[0] = aZ0_.dot(design_mat);
                    dx_Z[1] = aZ1_.dot(design_mat);
                    dy_Z[0] = bZ0_.dot(design_mat);
                    dy_Z[1] = bZ1_.dot(design_mat);
                    
                    auto Z_ratio=(Zv-ZPosZ_[0])/(ZPosZ_[1]-ZPosZ_[0]);

                    auto dx=dx_Z[0]*(1.-Z_ratio) + dx_Z[1]*Z_ratio;
                    auto dy=dy_Z[0]*(1.-Z_ratio) + dy_Z[1]*Z_ratio;

                    double F=Xv-dx;
                    double G=Yv-dy;

                    double I=F-xc_real_.x;
                    double J=G-xc_real_.y;

                    double H = 0.5*(I*I + J*J);

                    residual[0] = H;

                    if (!jacobians) return true;
                    double* jacobian = jacobians[0];
                    if (!jacobian) return true;

                    VectorXd d_design_mat_ds(10);

                    d_design_mat_ds<<0, 1, 2*s, 3*s*s,
                                        0, 0, 0,
                                        t, 2*s*t, t*t; 

                    VectorXd d_design_mat_dt(10);

                    d_design_mat_dt<<0, 0, 0, 0,
                                        1, 2*t, 3*t*t,
                                        s, s*s, 2*s*t; 

                    double ddx_Z0ds = aZ0_.dot(d_design_mat_ds);
                    double ddx_Z0dX = dsdX*ddx_Z0ds;

                    double ddx_Z0dt = aZ0_.dot(d_design_mat_dt);
                    double ddx_Z0dY = dtdY*ddx_Z0dt;

                    double ddx_Z1ds = aZ1_.dot(d_design_mat_ds);
                    double ddx_Z1dX = dsdX*ddx_Z1ds;

                    double ddx_Z1dt = aZ1_.dot(d_design_mat_dt);
                    double ddx_Z1dY = dtdY*ddx_Z1dt;

                    double ddy_Z0ds = bZ0_.dot(d_design_mat_ds);
                    double ddy_Z0dX = dsdX*ddy_Z0ds;

                    double ddy_Z0dt = bZ0_.dot(d_design_mat_dt);
                    double ddy_Z0dY = dtdY*ddy_Z0dt;

                    double ddy_Z1ds = bZ1_.dot(d_design_mat_ds);
                    double ddy_Z1dX = dsdX*ddy_Z1ds;

                    double ddy_Z1dt = bZ1_.dot(d_design_mat_dt);
                    double ddy_Z1dY = dtdY*ddy_Z1dt;

                    double dFdX = PixelPerMmFactor_ - ((1. - Z_ratio)*ddx_Z0dX + Z_ratio*ddx_Z1dX);
                    double dGdX = - ((1. - Z_ratio)*ddy_Z0dX + Z_ratio*ddy_Z1dX);

                    double dFdY = - ((1. - Z_ratio)*ddx_Z0dY + Z_ratio*ddx_Z1dY);
                    double dGdY = -1.*PixelPerMmFactor_ - ((1. - Z_ratio)*ddy_Z0dY + Z_ratio*ddy_Z1dY);

                    double dZ_ratiodZ = PixelPerMmFactor_/(ZPosZ_[1]-ZPosZ_[0]);

                    double dFdZ = - (-dZ_ratiodZ*dx_Z[0] + dZ_ratiodZ*dx_Z[1]);
                    double dGdZ = - (-dZ_ratiodZ*dy_Z[0] + dZ_ratiodZ*dy_Z[1]);

                    jacobian[0] = I*dFdX + J*dGdX;
                    jacobian[1] = I*dFdY + J*dGdY;
                    jacobian[2] = I*dFdZ + J*dGdZ;
                    return true;
                }
                else
                    return false;
            }

        private:
            double                 PixelPerMmFactor_;
            std::vector<double>    ZPosZ_;
            cv::Point2d            originZ_;
            cv::Point2d            normalizationFactorZ_;
            VectorXd               aZ0_;
            VectorXd               aZ1_;
            VectorXd               bZ0_;
            VectorXd               bZ1_;
            cv::Point2d            xc_real_;
            cv::Point3d            Xc_top_;
            cv::Point3d            Xc_bottom_;
    };

    class NonlinearMappingFunctionSingleTriangulationCostFunctor : public ceres::SizedCostFunction<1,3> {
        public:
            NonlinearMappingFunctionSingleTriangulationCostFunctor(double PixelPerMmFactor, const cv::Point3d& normalizationFactor, const VectorXd& a, const VectorXd& b, const cv::Point2d& xc_real, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom):
                PixelPerMmFactor_(PixelPerMmFactor), normalizationFactor_(normalizationFactor), a_(a), b_(b), xc_real_(xc_real), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom) {}

            virtual ~NonlinearMappingFunctionSingleTriangulationCostFunctor(){}

            virtual bool Evaluate(double const* const* parameters, double* residual, double** jacobians) const {
                const double X = parameters[0][0];
                const double Y = parameters[0][1];
                const double Z = parameters[0][2];

                if(X>Xc_bottom_.x && X<Xc_top_.x && Y>Xc_bottom_.y && Y<Xc_top_.y && Z>Xc_bottom_.z && Z<Xc_top_.z){
                    auto kX=PixelPerMmFactor_*X;
                    auto kY=PixelPerMmFactor_*Y;
                    auto kZ=PixelPerMmFactor_*Z;

                    auto s=kX/normalizationFactor_.x;
                    auto t=kY/normalizationFactor_.y;
                    auto u=kZ/normalizationFactor_.z;

                    double dsdX = PixelPerMmFactor_/normalizationFactor_.x;
                    double dtdY = PixelPerMmFactor_/normalizationFactor_.y;
                    double dudZ = PixelPerMmFactor_/normalizationFactor_.z;

                    VectorXd design_mat(19);

                    design_mat<<1, s, t, u, std::pow(s,2), s*t, std::pow(t,2), 
                                s*u, t*u, std::pow(u,2), std::pow(s,3), std::pow(s,2)*t, 
                                s*std::pow(t,2), std::pow(t,3), std::pow(s,2)*u, s*t*u, 
                                std::pow(t,2)*u, s*std::pow(u,2), t*std::pow(u,2);

                    auto dx=a_.dot(design_mat);
                    auto dy=b_.dot(design_mat);

                    double F=kX-dx;
                    double G=kY-dy;

                    double I=F-xc_real_.x;
                    double J=G-xc_real_.y;

                    double H = 0.5*(I*I + J*J);

                    residual[0] = H;

                    if (!jacobians) return true;
                    double* jacobian = jacobians[0];
                    if (!jacobian) return true;

                    VectorXd d_design_mat_ds(19);

                    d_design_mat_ds<<0, 1, 0, 0, 2*s, t, 0,
                                        u, 0, 0, 3*s*s, 2*s*t,
                                        t*t, 0, 2*s*u, t*u,
                                        0, u*u, 0;  

                    VectorXd d_design_mat_dt(19);

                    d_design_mat_dt<<0, 0, 1, 0, 0, s, 2*t,
                                        0, u, 0, 0, s*s,
                                        2*s*t, 3*t*t, 0, s*u,
                                        2*t*u, 0, u*u;  

                    VectorXd d_design_mat_du(19);

                    d_design_mat_du<<0, 0, 0, 1, 0, 0, 0,
                                        s, t, 2*u, 0, 0,
                                        0, 0, s*s, s*t,
                                        t*t, 2*s*u, 2*t*u;  

                    double d_dxds = a_.dot(d_design_mat_ds);
                    double d_dxdX = dsdX*d_dxds;

                    double d_dxdt = a_.dot(d_design_mat_dt);
                    double d_dxdY = dtdY*d_dxdt;

                    double d_dxdu = a_.dot(d_design_mat_du);
                    double d_dxdZ = dudZ*d_dxdu;

                    double d_dyds = b_.dot(d_design_mat_ds);
                    double d_dydX = dsdX*d_dyds;

                    double d_dydt = b_.dot(d_design_mat_dt);
                    double d_dydY = dtdY*d_dydt;

                    double d_dydu = b_.dot(d_design_mat_du);
                    double d_dydZ = dudZ*d_dydu;

                    double dFdX = PixelPerMmFactor_ - d_dxdX;
                    double dGdX = - d_dydX;

                    double dFdY = - d_dxdY;
                    double dGdY = PixelPerMmFactor_ - d_dydY;

                    double dFdZ = - d_dxdZ;
                    double dGdZ = - d_dydZ;

                    jacobian[0] = I*dFdX + J*dGdX;
                    jacobian[1] = I*dFdY + J*dGdY;
                    jacobian[2] = I*dFdZ + J*dGdZ;
                    return true;
                }
                else
                    return false;
            }

        private:
            double                 PixelPerMmFactor_;
            cv::Point3d            normalizationFactor_;
            VectorXd               a_;
            VectorXd               b_;
            cv::Point2d            xc_real_;
            cv::Point3d            Xc_top_;
            cv::Point3d            Xc_bottom_;
    };

	inline cv::Point3d triangulateTwoPointsNonlinear(const std::vector<Camera_ptr>& cam, int idx1, int idx2,
											const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
	                                        const cv::Point2d& xd1, const cv::Point2d& xd2, const cv::Point3d& Xc_init){

	    double initial_X[3]={Xc_init.x, Xc_init.y, Xc_init.z};
	    double X[3]={Xc_init.x, Xc_init.y, Xc_init.z};

	    ceres::Problem problem;

	    for(int i=0; i<cam.size(); i++){

	        if(cam[i]->i_cam()==idx1 || cam[i]->i_cam()==idx2){

	            auto   xc_real = cam[i]->i_cam()==idx1 ? xd1 : xd2;

	            // ceres::CostFunction* cost_function = new NonlinearMappingFunctionZPlaneTriangulationCostFunctor( cam[i]->PixelPerMmFactor(),
	            //                                                                                 cam[i]->ZPosZ(),
	            //                                                                                 cam[i]->originZ()[0], 
	            //                                                                                 cam[i]->normalizationFactorZ()[0], 
	            //                                                                                 cam[i]->aZ()[0],
	            //                                                                                 cam[i]->aZ()[1],
	            //                                                                                 cam[i]->bZ()[0],
	            //                                                                                 cam[i]->bZ()[1],
	            //                                                                                 xc_real, 
	            //                                                                                 Xc_top,
	            //                                                                                 Xc_bottom );

	            ceres::CostFunction* cost_function = new NonlinearMappingFunctionSingleTriangulationCostFunctor( cam[i]->PixelPerMmFactor(),
	                                                                                            cam[i]->normalizationFactor(), 
	                                                                                            cam[i]->a(),
	                                                                                            cam[i]->b(),
	                                                                                            xc_real, 
	                                                                                            Xc_top,
	                                                                                            Xc_bottom );

	            problem.AddResidualBlock( cost_function, new ceres::TrivialLoss(), &X[0]);       
	        }

	    }

	    ceres::Solver::Options options;
	    // options.minimizer_progress_to_stdout = true;
	    options.minimizer_type = ceres::LINE_SEARCH;
	    // options.use_nonmonotonic_steps = true;
	    // options.trust_region_strategy_type = ceres::DOGLEG;
	    options.max_num_iterations = 100;
	    // options.initial_trust_region_radius = 1e3;
	    options.function_tolerance = 1e-9;

	    ceres::Solver::Summary summary;
	    ceres::Solve(options, &problem, &summary);
	    // cout << summary.BriefReport() << "\n";
	    // cout << summary.FullReport() << "\n";

	    return cv::Point3d(X[0], X[1], X[2]);
	}

	inline cv::Point3d triangulateThreeOrFourPointsNonlinear(const std::vector<Camera_ptr>& cam, int idx1, int idx2, int idx3, int idx4, 
															const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
	                                                        const cv::Point2d& xd1, const cv::Point2d& xd2, const cv::Point2d& xd3, const cv::Point3d& Xc_init, const cv::Point2d& xd4=cv::Point2d(NAN, NAN)){

	    double initial_X[3]={Xc_init.x, Xc_init.y, Xc_init.z};
	    double X[3]={Xc_init.x, Xc_init.y, Xc_init.z};

	    ceres::Problem problem;

	    for(int i=0; i<cam.size(); i++){

	        cv::Point2d xc_real;
	        if(i==idx1)      xc_real=xd1;
	        else if(i==idx2) xc_real=xd2;
	        else if(i==idx3) xc_real=xd3;
	        else if(i==idx4) xc_real=xd4;
	        else             continue;

	        // ceres::CostFunction* cost_function = new NonlinearMappingFunctionZPlaneTriangulationCostFunctor( cam[i]->PixelPerMmFactor(),
	        //                                                                                 cam[i]->ZPosZ(),
	        //                                                                                 cam[i]->originZ()[0], 
	        //                                                                                 cam[i]->normalizationFactorZ()[0], 
	        //                                                                                 cam[i]->aZ()[0],
	        //                                                                                 cam[i]->aZ()[1],
	        //                                                                                 cam[i]->bZ()[0],
	        //                                                                                 cam[i]->bZ()[1],
	        //                                                                                 xc_real, 
	        //                                                                                 Xc_top,
	        //                                                                                 Xc_bottom );
	        
	        ceres::CostFunction* cost_function = new NonlinearMappingFunctionSingleTriangulationCostFunctor( cam[i]->PixelPerMmFactor(),
	                                                                                        cam[i]->normalizationFactor(), 
	                                                                                        cam[i]->a(),
	                                                                                        cam[i]->b(),
	                                                                                        xc_real, 
	                                                                                        Xc_top,
	                                                                                        Xc_bottom );

	        problem.AddResidualBlock( cost_function, new ceres::TrivialLoss(), &X[0]);       

	    }

	    ceres::Solver::Options options;
	    // options.minimizer_progress_to_stdout = true;
	    options.minimizer_type = ceres::LINE_SEARCH;
	    // options.use_nonmonotonic_steps = true;
	    // options.trust_region_strategy_type = ceres::DOGLEG;
	    options.max_num_iterations = 100;
	    // options.initial_trust_region_radius = 1e3;
	    options.function_tolerance = 1e-9;

	    ceres::Solver::Summary summary;
	    ceres::Solve(options, &problem, &summary);
	    // cout << summary.BriefReport() << "\n";
	    // cout << summary.FullReport() << "\n";

	    return cv::Point3d(X[0], X[1], X[2]);
	}

	inline cv::Point3d triangulateTwoPointsCore(const cv::Mat& cameraMatrix1, const cv::Mat& cameraMatrix2, 
	                                            const cv::Mat& distCoeffs1, const cv::Mat& distCoeffs2,
	                                            const cv::Mat& R1, const cv::Mat& R2, 
	                                            const cv::Mat& T1, const cv::Mat& T2,
	                                            const cv::Point2d& xd1, const cv::Point2d& xd2){

	    cv::Mat_<double> PL1, PL2, PR1, PR2;
	    cv::Mat rMat1, rMat2;
	    cv::Rodrigues(R1, rMat1);
	    cv::Rodrigues(R2, rMat2);
	    
	    cv::Point2d xc1(xd1), xc2(xd2);

	    PL1=cameraMatrix1*rMat1;
	    PR1=cameraMatrix1*T1;

	    PL2=cameraMatrix2*rMat2;
	    PR2=cameraMatrix2*T2;

	    cv::Mat_<double> design_mat, design_vec;

	    design_mat.create(4, 3);

	    design_vec=(cv::Mat_<double>(1,3) << xc1.x*PL1(2,0)-PL1(0,0), 
	                                         xc1.x*PL1(2,1)-PL1(0,1), 
	                                         xc1.x*PL1(2,2)-PL1(0,2));              
	    design_vec.copyTo(design_mat.row(0));

	    design_vec=(cv::Mat_<double>(1,3) << xc1.y*PL1(2,0)-PL1(1,0), 
	                                         xc1.y*PL1(2,1)-PL1(1,1), 
	                                         xc1.y*PL1(2,2)-PL1(1,2));  
	    design_vec.copyTo(design_mat.row(1));

	    design_vec=(cv::Mat_<double>(1,3) << xc2.x*PL2(2,0)-PL2(0,0), 
	                                         xc2.x*PL2(2,1)-PL2(0,1), 
	                                         xc2.x*PL2(2,2)-PL2(0,2));  
	    design_vec.copyTo(design_mat.row(2));
	        
	    design_vec=(cv::Mat_<double>(1,3) << xc2.y*PL2(2,0)-PL2(1,0), 
	                                         xc2.y*PL2(2,1)-PL2(1,1), 
	                                         xc2.y*PL2(2,2)-PL2(1,2));  
	    design_vec.copyTo(design_mat.row(3));

	    cv::Mat_<double> b=(cv::Mat_<double>(4,1) << PR1(0,0)-xc1.x*PR1(2,0),
	                                                 PR1(1,0)-xc1.y*PR1(2,0), 
	                                                 PR2(0,0)-xc2.x*PR2(2,0), 
	                                                 PR2(1,0)-xc2.y*PR2(2,0));

	    cv::Mat_<double> Xcv;

	    cv::solve(design_mat, b, Xcv, cv::DECOMP_SVD);

	    return cv::Point3d(Xcv(0,0), Xcv(1,0), Xcv(2,0));
	}

	inline cv::Point3d triangulateTwoPoints(const std::vector<Camera_ptr>& cam, int idx1, int idx2,
	                                    const cv::Point2d& xd1, const cv::Point2d& xd2,
	                                    int nview){

	    return triangulateTwoPointsCore(cam[idx1]->cameraMatrix(), cam[idx2]->cameraMatrix(), 
	                                cam[idx1]->distCoeffs(), cam[idx2]->distCoeffs(),
	                                cam[idx1]->rvecs()[nview], cam[idx2]->rvecs()[nview],
	                                cam[idx1]->tvecs()[nview], cam[idx2]->tvecs()[nview],
	                                xd1, xd2);
	}

	inline cv::Point3d triangulateTwoPoints(const std::vector<Camera_ptr>& cam, int idx1, int idx2,
	                   		                const cv::Point2d& xd1, const cv::Point2d& xd2, 
	                        	            bool iterative_triangulation_flag=false,
	                            	        bool verbal_flag=false){

	    if(!iterative_triangulation_flag)
	        return triangulateTwoPointsCore(cam[idx1]->cameraMatrix(), cam[idx2]->cameraMatrix(), 
	                                        cam[idx1]->distCoeffs(), cam[idx2]->distCoeffs(),
	                                        cam[idx1]->R(), cam[idx2]->R(), 
	                                        cam[idx1]->T(), cam[idx2]->T(),
	                                        xd1, xd2);
	    else{
	        auto Xc0 = triangulateTwoPointsCore(cam[idx1]->cameraMatrix(), cam[idx2]->cameraMatrix(), 
	                                        cam[idx1]->distCoeffs(), cam[idx2]->distCoeffs(),
	                                        cam[idx1]->R(), cam[idx2]->R(), 
	                                        cam[idx1]->T(), cam[idx2]->T(),
	                                        xd1, xd2);
	        if(verbal_flag) cout<<"Xc0 "<<Xc0<<endl;
	     
	        std::vector<cv::Point3d> Xc_new_vec;
	        std::vector<int>         ZViews_vec;

	        auto ZViews  = cam[idx1]->ZViews();

	        int it_max = 5;

	        for(int i=0; i<it_max; i++){
	            if(i!=0)     Xc0 = Xc_new_vec.back();

	            std::vector<double> Z_diff;
	            for(int i=0; i<ZViews.size(); i++){
	                Z_diff.push_back(std::abs(ZViews[i]-Xc0.z));
	            }

	            auto p = std::min_element(Z_diff.begin(), Z_diff.end());
	            int Zidx = std::distance(Z_diff.begin(), p);

	            ZViews_vec.push_back(Zidx);

	            if(Xc0.z>=ZViews[0] && Xc0.z<ZViews.back() && Zidx!=ZViews.size()-1){
	                auto Xc_left  = triangulateTwoPoints(cam, idx1, idx2, xd1, xd2, Zidx);
	                Xc_left.z = Xc_left.z + ZViews[Zidx];        
	                auto Xc_right = triangulateTwoPoints(cam, idx1, idx2, xd1, xd2, Zidx+1);
	                Xc_right.z = Xc_right.z + ZViews[Zidx+1];        
	                if(verbal_flag)  cout<<"Xc_left  "<<Xc_left<<endl
                                        <<"Xc_right "<<Xc_right<<endl;

	                cv::Point3d Xc_interp = Xc_left;
	                if(i==it_max-1) Xc_interp = (Xc_left+Xc_right)/2.;                    
	                if(verbal_flag)  cout<<"Xc_interp"<<Xc_interp<<endl;
	                Xc_new_vec.push_back( Xc_interp ); 
	            }
	            else{
	                Xc_new_vec.push_back( triangulateTwoPoints(cam, idx1, idx2, xd1, xd2, Zidx) ); 
	                Xc_new_vec.back().z = Xc_new_vec.back().z + ZViews[Zidx];        
	            }

	            if(verbal_flag)  cout<<"ZViews[Zidx]      "<<ZViews[Zidx]<<endl
	                                 <<"Xc_new_vec.back() "<<Xc_new_vec.back()<<endl;
	        }

	        cv::Point3d Xc_fin;
	        if(ZViews_vec.back() != ZViews_vec.rbegin()[1])
	            Xc_fin = (Xc_new_vec.back() + Xc_new_vec.rbegin()[1])/2.;
	        else
	            Xc_fin = Xc_new_vec.back();

	        if(verbal_flag) cout<<"Xc_fin "<<Xc_fin<<endl;

	        return Xc_fin;
	    }
	}

	inline cv::Point3d triangulateThreeOrFourPoints(const std::vector<Camera_ptr>& cam, int idx1, int idx2, int idx3, int idx4,
	                                                const cv::Point2d& xd1, const cv::Point2d& xd2, const cv::Point2d& xd3, const cv::Point2d& xd4=cv::Point2d(NAN, NAN)){

	    int num_rows= (isnan(xd4.x) ?  6 : 8);

	    cv::Mat_<double> PL1, PR1, PL2, PR2, PL3, PR3, PL4, PR4;

	    cv::Mat rMat1, rMat2, rMat3, rMat4;
	    cv::Rodrigues(cam[idx1]->R(), rMat1);
	    cv::Rodrigues(cam[idx2]->R(), rMat2);
	    cv::Rodrigues(cam[idx3]->R(), rMat3);
	    if(idx4!=-1) cv::Rodrigues(cam[idx4]->R(), rMat4);

	    cv::Point2d xc1(xd1), xc2(xd2), xc3(xd3), xc4(xd4);

	    PL1=cam[idx1]->cameraMatrix()*rMat1;
	    PR1=cam[idx1]->cameraMatrix()*cam[idx1]->T();

	    PL2=cam[idx2]->cameraMatrix()*rMat2;
	    PR2=cam[idx2]->cameraMatrix()*cam[idx2]->T();

	    PL3=cam[idx3]->cameraMatrix()*rMat3;
	    PR3=cam[idx3]->cameraMatrix()*cam[idx3]->T();

	    if(idx4!=-1){
	        PL4=cam[idx4]->cameraMatrix()*rMat4;
	        PR4=cam[idx4]->cameraMatrix()*cam[idx4]->T();
	    }

	    cv::Mat_<double> design_mat, design_vec, b;

	    design_mat.create(num_rows, 3);
	        
	    design_vec=(cv::Mat_<double>(1,3) << xc1.x*PL1(2,0)-PL1(0,0), xc1.x*PL1(2,1)-PL1(0,1), xc1.x*PL1(2,2)-PL1(0,2));                
	    design_vec.copyTo(design_mat.row(0));
	    
	    design_vec=(cv::Mat_<double>(1,3) << xc1.y*PL1(2,0)-PL1(1,0), xc1.y*PL1(2,1)-PL1(1,1), xc1.y*PL1(2,2)-PL1(1,2));    
	    design_vec.copyTo(design_mat.row(1));
	    
	    design_vec=(cv::Mat_<double>(1,3) << xc2.x*PL2(2,0)-PL2(0,0), xc2.x*PL2(2,1)-PL2(0,1), xc2.x*PL2(2,2)-PL2(0,2));    
	    design_vec.copyTo(design_mat.row(2));
	    
	    design_vec=(cv::Mat_<double>(1,3) << xc2.y*PL2(2,0)-PL2(1,0), xc2.y*PL2(2,1)-PL2(1,1), xc2.y*PL2(2,2)-PL2(1,2));    
	    design_vec.copyTo(design_mat.row(3));
	            
	    design_vec=(cv::Mat_<double>(1,3) << xc3.x*PL3(2,0)-PL3(0,0), xc3.x*PL3(2,1)-PL3(0,1), xc3.x*PL3(2,2)-PL3(0,2));                
	    design_vec.copyTo(design_mat.row(4));
	    
	    design_vec=(cv::Mat_<double>(1,3) << xc3.y*PL3(2,0)-PL3(1,0), xc3.y*PL3(2,1)-PL3(1,1), xc3.y*PL3(2,2)-PL3(1,2));    
	    design_vec.copyTo(design_mat.row(5));
	    
	    if(idx4!=-1){
	        design_vec=(cv::Mat_<double>(1,3) << xc4.x*PL4(2,0)-PL4(0,0), xc4.x*PL4(2,1)-PL4(0,1), xc4.x*PL4(2,2)-PL4(0,2));    
	        design_vec.copyTo(design_mat.row(6));
	        
	        design_vec=(cv::Mat_<double>(1,3) << xc4.y*PL4(2,0)-PL4(1,0), xc4.y*PL4(2,1)-PL4(1,1), xc4.y*PL4(2,2)-PL4(1,2));    
	        design_vec.copyTo(design_mat.row(7));

	        b=(cv::Mat_<double>(num_rows,1) << PR1(0,0)-xc1.x*PR1(2,0), PR1(1,0)-xc1.y*PR1(2,0), 
	                                           PR2(0,0)-xc2.x*PR2(2,0), PR2(1,0)-xc2.y*PR2(2,0),
	                                           PR3(0,0)-xc3.x*PR3(2,0), PR3(1,0)-xc3.y*PR3(2,0),
	                                           PR4(0,0)-xc4.x*PR4(2,0), PR4(1,0)-xc4.y*PR4(2,0));
	    }
	    else{
	        b=(cv::Mat_<double>(num_rows,1) << PR1(0,0)-xc1.x*PR1(2,0), PR1(1,0)-xc1.y*PR1(2,0), 
	                                           PR2(0,0)-xc2.x*PR2(2,0), PR2(1,0)-xc2.y*PR2(2,0),
	                                           PR3(0,0)-xc3.x*PR3(2,0), PR3(1,0)-xc3.y*PR3(2,0));
	    }

	    cv::Mat_<double> Xcv;
	        
	    cv::solve(design_mat, b, Xcv, cv::DECOMP_SVD);
	    
	    return cv::Point3d(Xcv(0,0), Xcv(1,0), Xcv(2,0));
	}

}//namespace triang

}//namespace PTL
#endif
