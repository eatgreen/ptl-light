#ifndef TRANSPORT
#define TRANSPORT

#include "Utils.hpp"

namespace PTL{

class Transport
{
    public:
        enum class TimeScheme_Type    				{Euler, Heun};
        enum class VelocityInterpolation_Type		{TriLinear, AGW};

	private:
		int n_particles_;

		VelocityInterpolation_Type	velocity_to_scatter_interp_type_;

		cv::Point3d 		 Xc_top_;
		cv::Point3d 		 Xc_bottom_;

		std::vector<double>  xgrid_;
		std::vector<double>  ygrid_;
		std::vector<double>  zgrid_;

		cv::Point3i			 nXc_;
		cv::Point3d          dXc_;

		TimeScheme_Type	     time_scheme_;
		double 				 dt_;
		double 				 dtObs_;

		std::vector<cv::Point3d> 				grid_coords_;
		std::vector<cv::Point3d> 				velocity_fields_;  

		std::vector<MatrixXd> 	 U_ongrid3D_;
		std::vector<MatrixXd> 	 V_ongrid3D_;
		std::vector<MatrixXd> 	 W_ongrid3D_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dx_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dx_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dx_;
		std::vector<MatrixXd> 	 dU_ongrid3D_dy_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dy_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dy_;
		std::vector<MatrixXd> 	 dU_ongrid3D_dz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dz_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dxdy_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dxdy_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dxdy_;
		std::vector<MatrixXd> 	 dU_ongrid3D_dxdz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dxdz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dxdz_;
		std::vector<MatrixXd> 	 dU_ongrid3D_dydz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dydz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dydz_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dxdydz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dxdydz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dxdydz_;

		cv::Point3d 	    	 velo_max_;
		cv::Point3d				 velo_min_;

    public:
    	Transport();

    	~Transport();

    	Transport(VelocityInterpolation_Type, const cv::Point3d&, const cv::Point3d&, const cv::Point3i&, TimeScheme_Type, double, double);
		
		/// generate background particle tracks
		void predictParticlePositionField(const std::vector<Particle_ptr>&, int, bool repredict_flag=false) const;
		
		void makeGrids(void);

		void setVelocityOnGrids(const std::vector<cv::Point3d>&);

		cv::Point3d integrateDtUsingEulerianVelocityFields(const cv::Point3d&) const;

		const std::vector<cv::Point3d>& velocity_fields(void) const { return velocity_fields_; }
		const std::vector<cv::Point3d>& grid_coords(void) const { return grid_coords_; }
		const cv::Point3i& nXc(void) const { return nXc_; }
		const cv::Point3d& dXc(void) const { return dXc_; }
		
		const std::vector<MatrixXd>& U_ongrid3D(void) const { return U_ongrid3D_; }
		const std::vector<MatrixXd>& V_ongrid3D(void) const { return V_ongrid3D_; }
		const std::vector<MatrixXd>& W_ongrid3D(void) const { return W_ongrid3D_; }

		const std::vector<double>& xgrid(void) const { return xgrid_; }
		const std::vector<double>& ygrid(void) const { return ygrid_; }
		const std::vector<double>& zgrid(void) const { return zgrid_; }

		const cv::Point3d velo_max(void) const { return velo_max_; }
		const cv::Point3d velo_min(void) const { return velo_min_; }
			
		void readVelocityFieldsFromTXTFile(const std::string&, bool starting_flag=false);
		void readVelocityFieldsFromNPZFile(const std::string&, bool starting_flag=false);

		void saveVelocityFieldsToVTKFile(const std::string&) const;
		void saveVelocityFieldsToNPZFile(const std::string&) const;

	private:
		cv::Point3d interpolateVelocityFieldOnParticleCoordinates(const cv::Point3d&) const;

		cv::Point3d trilinearInterpolate(const cv::Point3d&) const;
		cv::Point3d AGWInterpolate(const cv::Point3d&) const;

		void imposeBoundaryCondition(cv::Point3d&) const;

		cv::Point3d doEulerScheme(const cv::Point3d&) const;
		cv::Point3d doHeunScheme(const cv::Point3d&) const;

		void findNeighbors(const std::string&, double, int&, int&, double&, double&) const;

		void computeVelocityDerivatives(void);
};

}//namespace PTL
#endif