#ifndef STBMETHOD
#define STBMETHOD

#include "OptMethod.hpp"

namespace PTL{

class STB: public OptMethod
{
	private:
		std::vector<Particle_ptr>   particles_stb_;

        int  						fine_shake_max_iter_;
        double 						fine_shake_pixel_;
        double				 		fine_shake_relaxfactor_;
        double 						fine_shake_ratio_valve_=0.025;

        cv::Point3d    				fine_shake_ratio_; 
        std::vector<double>         resf_prev_;
        std::vector<double>         res_partf_;

	public:
		STB();

        ~STB();

		STB(Method_Type, ControlVector_Type, bool,
			int, int, const cv::Point3d&, const cv::Point3d&,
			double, double,
			const cv::Point3d&, const std::string&, int,
			bool, int, double, double,
			int, double, double);

		STB(Method_Type, ControlVector_Type, bool,
		 	int, int, const cv::Point3d&, const cv::Point3d&,
			const cv::Point3d&, const std::string&, int,
			int, double, double);

		void initialization(std::vector<Particle_ptr>&, int, bool use_in_lapiv=false) final;

		void predict(DynModel*, int, int) final;

		void prepare(int, const std::vector<Camera_ptr>&, int dummy=0) final;

		void selectWarmStart(int, const std::vector<Camera_ptr>&);

		void core(int, int, const std::vector<Camera_ptr>&, bool) final;
	
		void postIPRResidual(int, int, const std::vector<Camera_ptr>&) final;

		void update(Particle*, int, int, const std::vector<Camera_ptr>&, double&) final;

	private:
        double quadraticFit(Particle*, double, const std::string&, double, int, const std::vector<Camera_ptr>&, bool verbal=false);

	public:
		void clearParticleData(void) final { std::vector<Particle_ptr>().swap(particles_stb_); }
		const int& opt_max_iter(void) const final { return fine_shake_max_iter_; }

		const std::vector<Particle_ptr>& particles_field(void) const final { return particles_stb_; }
		std::vector<Particle_ptr>& get_particles_field(void) final { return particles_stb_; }
};

}//namespace PTL
#endif