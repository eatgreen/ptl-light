#ifndef EVALUATION
#define EVALUATION

#include "Camera.hpp"

namespace PTL{

namespace eval
{
    void evaluateOPTTracks(const std::vector<int>&, const cv::Point3d&, const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&, const std::vector<Camera_ptr>&);

    void evaluateOPTParticles(const std::vector<int>&, const cv::Point3d&,
                            const cv::Point3d&, const cv::Point3d&,
                            const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&,
                            const std::vector<Camera_ptr>&);

    void evaluateTrackedParticles(const std::vector<int>&, const cv::Point3d&, const cv::Point3d&, const cv::Point3d&, const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&);

    void evaluateInitializedTracks(const std::vector<int>&, const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&);

    void evaluateIPRParticles(const std::vector<int>&, const cv::Point3d&, 
                                const std::vector<Camera_ptr>&, 
                                const cv::Point3d&, const cv::Point3d&, 
                                const std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&, 
                                const std::vector<Particle_ptr>&,
                                double ipr_error_ratio=1);

    std::vector<std::pair<int, std::vector<int>>>
    findOPTTracksPairs(const std::vector<int>&, const cv::Point3d&, const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&);
    void computeOPTTracksErrors(const std::vector<int>&, const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&, const std::vector<std::pair<int, std::vector<int>>>&, const std::vector<Camera_ptr>&);
    void computeOPTTracksLength(const std::vector<int>&, const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&, const std::vector<std::pair<int, std::vector<int>>>&);

    void evaluateDetectedTrueParticles(const std::vector<int>&, const cv::Point3d&, const cv::Point3d&, const cv::Point3d&,
                                        const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&, const std::vector<Camera_ptr>&);

    void searchParticleInFrame(const std::vector<int>&, const std::vector<Particle_ptr>&, double, bool, const cv::Point3d&, const cv::Point3d&,
                                std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&);

    void convertParticlesToFrame(const std::vector<int>&, const std::vector<Particle_ptr>&, bool,
                                std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&);

    bool checkFoundPairInNextFrame(int, int, const cv::Point3d&, Particle*, Particle*);

    void evaluateUndetectedTrueParticles(const std::vector<int>&, const cv::Point3d&, const cv::Point3d&, const cv::Point3d&,
                                        const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&);
    void evaluateTrackedGhostParticles(const std::vector<int>&, const cv::Point3d&, const cv::Point3d&, const cv::Point3d&,
                                    const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&);
    void evaluateTotalGhostParticles(const std::vector<int>&, const cv::Point3d&, const cv::Point3d&, const cv::Point3d&,
                                    const std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&);
    
    void computeNearestParticleErrorPerFrame(const pcl::PointXYZI&, double,
                                            pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>&,
                                            pcl::PointCloud<pcl::PointXYZI>::Ptr,
                                            const std::vector<Camera_ptr>&,
                                            int&,
                                            std::vector<double>&,
                                            std::vector<double>&,
                                            std::vector<int>&);
}//namespace eval

}//namespace PTL
#endif