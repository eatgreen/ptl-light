#ifndef TRACK
#define TRACK

#include "Utils.hpp"
#include "Transport.hpp"

namespace PTL{

namespace track{

	std::vector<Particle_ptr> convertFrameToParticles(int, const std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&);
	std::vector<Particle_ptr> makeLinkUsingPredictionField(const std::vector<int>&, const Transport&, const cv::Point3d&,
	                                const std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&);

	void readIPRDataIntoFrames(const std::vector<int>&, const std::string&, bool, std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&);

	void printTrackInformation(int, const std::vector<Particle_ptr>&, double dtObs=0, const cv::Point3d& dXc_px=cv::Point3d(0,0,0));

    void saveInitTrackedParticles(const std::vector<int>&, const std::string&, const std::vector<Particle_ptr>&);

	std::vector<Particle_ptr> findTheBrightestParticles(int, int, const std::vector<Particle_ptr>&, const cv::Point3d&, const cv::Point3d&);
	std::vector<Particle_ptr> getTracksWithLength2AtCurrentSnapshot(int, const std::vector<Particle_ptr>&);
	std::vector<Particle_ptr> mergeTwoPaticleFields(std::vector<Particle_ptr>&, const std::vector<Particle_ptr>&, int);
	std::vector<Particle_ptr> reorderParticleField(const std::vector<Particle_ptr>&);
	std::vector<Particle_ptr> rmVeryShortTracksFromParticleField(const std::vector<Particle_ptr>&, const cv::Point3d&, const cv::Point3d&);
        
    void saveFrameDataTXT(int, const std::vector<Particle_ptr>&, const std::string&, double, bool, int track_min_length=1, const std::string& modified_str="");
    void saveFrameDataVTK(int, const std::vector<Particle_ptr>&, const std::string&, int track_min_length=1, const std::string& modified_str="");
}//namespace track

}//namespace PTL
#endif
