#ifndef RANDOM
#define RANDOM

#include <iostream>
#include <Eigen/Core>
#include "boost/random/uniform_int_distribution.hpp"
#include "boost/random.hpp"
#include "boost/random/normal_distribution.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;

namespace PTL{

namespace Random{

	inline double StandardNormalRandomNumber(double dummy)
	{
		static boost::mt19937 rng(0.0);
		static boost::normal_distribution<> nd(0,1);
		return nd(rng);
	}

	inline double StandardNormalRandomNumberLAPIV(double dummy)
	{
		static boost::mt19937 rng(0.0);
		static boost::normal_distribution<> nd(0,1);
		return nd(rng);
	}

	inline MatrixXd NormalRandomMatrix(double rows, double cols, double mean, double std)
	{
		MatrixXd target;
		target=MatrixXd::Zero(rows,cols).unaryExpr(std::ref(StandardNormalRandomNumber))*std+MatrixXd::Ones(rows,cols)*mean;
		return target;
	}

	inline double UniformDistributionNumber(double min, double max, double seed=0.0)
	{
		static boost::mt19937 rng(seed);
		boost::random::uniform_real_distribution<> dist(min, max);
	    return dist(rng);
	}

}//namespace Random

}//namespace PTL
#endif