#ifndef UTILS
#define UTILS

#include "Particle.hpp"
#include "Random.hpp"

namespace PTL{

namespace utils{

    const std::vector<int> compression_params{cv::IMWRITE_PNG_STRATEGY, cv::IMWRITE_PNG_STRATEGY_DEFAULT};

    inline Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");

    inline bool checkVelocityIsOutOfRange(const cv::Point3d& V, const cv::Point3d& V_max, const cv::Point3d& V_min, double tolerance=5e-2){
        return ( V.x - V_max.x > tolerance*V_max.x || V.x - V_min.x < tolerance*V_min.x ||
                 V.y - V_max.y > tolerance*V_max.y || V.y - V_min.y < tolerance*V_min.y ||
                 V.z - V_max.z > tolerance*V_max.z || V.z - V_min.z < tolerance*V_min.z );
    }

    inline    static bool compare_x(const cv::Point3d& a, const cv::Point3d& b){ return (a.x < b.x); }
    inline    static bool compare_y(const cv::Point3d& a, const cv::Point3d& b){ return (a.y < b.y); }
    inline    static bool compare_z(const cv::Point3d& a, const cv::Point3d& b){ return (a.z < b.z); }

    inline double angle(const cv::Point3d& v1, const cv::Point3d& v2)
    {
        double cosAngle = v1.dot(v2) / (cv::norm(v1) * cv::norm(v2));
        if (cosAngle > 1.0)
            return 0.0;
        else if (cosAngle < -1.0)
            return CV_PI;
        return std::acos(cosAngle);
    }

    inline int constchar2int(const char* value){
        std::stringstream strValue;
        strValue << value;
        int intvalue;
        strValue >> intvalue;
        return intvalue;
    }

    inline double constchar2double(const char* value){
        std::stringstream strValue;
        strValue << value;
        double doublevalue;
        strValue >> doublevalue;
        return doublevalue;
    }

    inline void showInfoOnDeletedParticles(bool print_flag=true){
        if(print_flag){
            cout<<endl<<"number of particles deleted: "<<Particle::num_deleted_<<endl;
            Particle::num_deleted_tot_+=Particle::num_deleted_;
        }
        Particle::delete_verbal_guard_=true;
        Particle::num_deleted_=0;
    }

    inline double OTFCore(const Eigen::Vector4f& thetai, const cv::Point2d& xc, double E, cv::Mat& Imgi_smpl_Grey, cv::Rect& pos){

        double   x_bottom_smpl, x_top_smpl, y_bottom_smpl, y_top_smpl, nx_smpl, ny_smpl;
        VectorXd x_smpl, y_smpl;

        double   radius=std::ceil(1.5*( 1.0/std::sqrt( thetai(0) ) + 1.0/std::sqrt( thetai(2) ) ));
        if(radius > 2) radius=2.0;

        x_bottom_smpl = std::floor(xc.x-radius);
        x_top_smpl    = std::ceil(xc.x+radius);

        y_bottom_smpl = std::floor(xc.y-radius);
        y_top_smpl    = std::ceil(xc.y+radius);

        nx_smpl = x_top_smpl-x_bottom_smpl+1;
        ny_smpl = y_top_smpl-y_bottom_smpl+1;

        x_smpl.setLinSpaced(nx_smpl, x_bottom_smpl, x_top_smpl);
        y_smpl.setLinSpaced(ny_smpl, y_bottom_smpl, y_top_smpl);

        pos = cv::Rect(x_bottom_smpl, y_bottom_smpl, nx_smpl, ny_smpl);

        cv::Mat_<float> Imgi_smpl_raw=cv::Mat_<float>::zeros(y_smpl.size(), x_smpl.size());

        Eigen::VectorXf Imgi_x_smpl, Imgi_y_smpl;
        Imgi_x_smpl.resize(x_smpl.size());
        Imgi_y_smpl.resize(y_smpl.size());

        for(int i=0; i<x_smpl.size(); i++)
            Imgi_x_smpl(i)=erf(sqrt(0.5)*sqrt(thetai(0))*( x_smpl(i)+0.5-xc.x )) - erf(sqrt(0.5)*sqrt(thetai(2))*( x_smpl(i)-0.5-xc.x ));
        
        for(int j=0; j<y_smpl.size(); j++)
            Imgi_y_smpl(j)=erf(sqrt(0.5)*sqrt(thetai(0))*( y_smpl(j)+0.5-xc.y )) - erf(sqrt(0.5)*sqrt(thetai(2))*( y_smpl(j)-0.5-xc.y ));

        for(int i=0; i<x_smpl.size(); i++){
            for(int j=0; j<y_smpl.size(); j++){
                Imgi_smpl_raw(j,i)=Imgi_x_smpl(i)*Imgi_y_smpl(j)*0.25*thetai(3);
            }
        }

        Imgi_smpl_Grey=Imgi_smpl_raw*E;

        return std::ceil(radius);
    }

    /// used in opt_driver
    
    inline void checkIfParticlesAppearOnSnapshot(int it_seq, const std::vector<Particle_ptr>& particles_tgt,
                                        const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, bool is_LAPIV){

        int track_min_number = (is_LAPIV ? 1 : 3);
        int non_side_number  = 3;

        for(const auto& pt: particles_tgt){
            if(!pt->Xcoord().empty()){
                pt->checkIfParticleAppearsOnSnapshot(it_seq, Xc_top, Xc_bottom);
            }
            if(pt->is_ontrack()){
                if(pt->is_side() && pt->Xcoord().size()>=non_side_number){
                    pt->markAsNonSide();
                }

                if(pt->Xcoord().size()>=track_min_number){
                    pt->markAsToBeTracked();
                }
                else{
                    pt->markAsToBeTriangulated();
                    pt->setis_tracked_flag_as(false);
                }
            }
            else{
                if(!pt->is_deleted()){
                    pt->deleteData(false);
                    pt->markAsTerminated();
                    pt->setit_seq_fin(it_seq-1);
                    if(pt->length()!=pt->it_seq_fin()-pt->it_seq_deb()+1) pt->pop_back_Xcoord();
                }
            }
        }
        utils::showInfoOnDeletedParticles();
    }

    /// used in inittrack_driver & postproc_driver & function: findTheBrightestParticles when track information are available

    inline void checkIfParticlesAppearOnSnapshot(int it_seq, const std::vector<Particle_ptr>& particles_tgt){

        for(const auto& pt: particles_tgt){
            pt->checkIfParticleAppearsOnSnapshot(it_seq);
        }
    }

    inline void checkIfParticlesAppearOnSnapshotInterface(int it_seq, const std::vector<Particle_ptr>& particles_tgt,
                                        const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, bool is_LAPIV=false){

        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_tgt, Xc_top, Xc_bottom, is_LAPIV);
    }
    
    inline void maintainPositionInRange(cv::Point3d& Xc, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom){

        Xc.x = std::max(Xc.x, Xc_bottom.x);
        Xc.x = std::min(Xc.x, Xc_top.x);
        Xc.y = std::max(Xc.y, Xc_bottom.y);
        Xc.y = std::min(Xc.y, Xc_top.y);
        Xc.z = std::max(Xc.z, Xc_bottom.z);
        Xc.z = std::min(Xc.z, Xc_top.z);
    }

    inline void maintainVelocityInRange(cv::Point3d& V, const cv::Point3d& velo_max, const cv::Point3d& velo_min){

        V.x = std::max(V.x, velo_min.x*1.2);
        V.x = std::min(V.x, velo_max.x*1.2);
        V.y = std::max(V.y, velo_min.y*1.2);
        V.y = std::min(V.y, velo_max.y*1.2);
        V.z = std::max(V.z, velo_min.z*1.2);
        V.z = std::min(V.z, velo_max.z*1.2);
    }

    inline void getPositionAndSizeOfOverlapedAreaOfOneLocalPatchAndIres(const cv::Rect& pos_n, int window_length, 
                                                                        int n_width, int n_height,
                                                                        cv::Rect& pos_ol_n, cv::Rect& pos_ol_Ires,
                                                                        bool verbal=false){
    
        //Beginning index of big canvas of Ires
        auto pos_n_deb1_BigCanvas=std::max(0, pos_n.y+pos_n.height/2-window_length);
        auto pos_n_deb0_BigCanvas=std::max(0, pos_n.x+pos_n.width/2-window_length);

        if(verbal) cout<<"pos_n_deb1_BigCanvas "<<pos_n_deb1_BigCanvas<<endl;
        if(verbal) cout<<"pos_n_deb0_BigCanvas "<<pos_n_deb0_BigCanvas<<endl;

        //Relative beginning and ending index of small canvas
        auto pos_n_local_deb1=std::max(pos_n.height/2-window_length, 0);
        auto pos_n_local_fin1=std::min(pos_n.height/2+window_length, pos_n.height-1);
        auto pos_n_local_deb0=std::max(pos_n.width/2-window_length, 0);
        auto pos_n_local_fin0=std::min(pos_n.width/2+window_length, pos_n.width-1);

        if(verbal) cout<<"pos_n_local_deb1 "<<pos_n_local_deb1<<endl;
        if(verbal) cout<<"pos_n_local_fin1 "<<pos_n_local_fin1<<endl;
        if(verbal) cout<<"pos_n_local_deb0 "<<pos_n_local_deb0<<endl;
        if(verbal) cout<<"pos_n_local_fin0 "<<pos_n_local_fin0<<endl;

        auto pos_n_deb1 = pos_n_local_deb1; 
        auto pos_n_fin1 = pos_n_local_fin1;
        auto pos_n_deb0 = pos_n_local_deb0; 
        auto pos_n_fin0 = pos_n_local_fin0;

        //Set a second beginning index of the small canvas if it is outside the big canvas
        if(pos_n.y<0) pos_n_deb1=-pos_n.y;
        if(pos_n.x<0) pos_n_deb0=-pos_n.x;

        //Take the bigger one of the two
        pos_n_deb1=std::max(pos_n_deb1, pos_n_local_deb1);
        pos_n_deb0=std::max(pos_n_deb0, pos_n_local_deb0);

        //Set a second ending index of the small canvas if it is outside the big canvas
        if(pos_n.y+pos_n.height-1>n_height-1) pos_n_fin1=n_height-1-pos_n.y;
        if(pos_n.x+pos_n.width-1>n_width-1)   pos_n_fin0=n_width-1-pos_n.x;

        //Take the smaller one of the two
        pos_n_fin1=std::min(pos_n_fin1, pos_n_local_fin1);
        pos_n_fin0=std::min(pos_n_fin0, pos_n_local_fin0);

        if(verbal) cout<<"pos_n_deb1 "<<pos_n_deb1<<endl;
        if(verbal) cout<<"pos_n_fin1 "<<pos_n_fin1<<endl;
        if(verbal) cout<<"pos_n_deb0 "<<pos_n_deb0<<endl;
        if(verbal) cout<<"pos_n_fin0 "<<pos_n_fin0<<endl;

        auto overlap_window_size1=pos_n_fin1-pos_n_deb1+1;
        auto overlap_window_size0=pos_n_fin0-pos_n_deb0+1;
        
        pos_ol_Ires = cv::Rect(pos_n_deb0_BigCanvas, pos_n_deb1_BigCanvas, overlap_window_size0, overlap_window_size1);
        pos_ol_n    = cv::Rect(pos_n_deb0, pos_n_deb1, overlap_window_size0, overlap_window_size1);

        if(verbal) cout<<"pos_ol_Ires "<<pos_ol_Ires<<endl;
        if(verbal) cout<<"pos_ol_n    "<<pos_ol_n<<endl;

        // if(pos_ol_Ires.width!=pos_ol_Ires.height) cout<<"pos_n outside image domain"<<endl;
    }

    //m compared to n
    inline void getPositionAndSizeOfOverlapedAreaOfTwoLocalPatches(const cv::Rect& pos_n, const cv::Rect& pos_m, 
                                                                    cv::Rect& pos_ol_n, cv::Rect& pos_ol_m){
        
        pos_ol_n = cv::Rect(0,0,0,0);
        pos_ol_m = cv::Rect(0,0,0,0);

        pos_ol_m.y=std::max(0, pos_n.y-pos_m.y);
        pos_ol_m.x=std::max(0, pos_n.x-pos_m.x);

        pos_ol_n.y=std::max(0, pos_m.y-pos_n.y);
        pos_ol_n.x=std::max(0, pos_m.x-pos_n.x);

        if(pos_n.y>pos_m.y)
            if(pos_n.y+pos_n.height<=pos_m.y+pos_m.height)
                pos_ol_m.height=pos_n.height;
            else
                pos_ol_m.height=std::max(0, pos_m.height-std::abs(pos_n.y-pos_m.y));
        else if(pos_n.y<pos_m.y)
            if(pos_n.y+pos_n.height>=pos_m.y+pos_m.height)
                pos_ol_m.height=pos_m.height;
            else
                pos_ol_m.height=std::max(0, pos_n.height-std::abs(pos_n.y-pos_m.y));
        else
            pos_ol_m.height=std::min(pos_n.height, pos_m.height);

        pos_ol_n.height=pos_ol_m.height;

        if(pos_n.x>pos_m.x)
            if(pos_n.x+pos_n.width<=pos_m.x+pos_m.width)
                pos_ol_m.width=pos_n.width;
            else
                pos_ol_m.width=std::max(0, pos_m.width-std::abs(pos_n.x-pos_m.x));
        else if(pos_n.x<pos_m.x)
            if(pos_n.x+pos_n.width>=pos_m.x+pos_m.width)
                pos_ol_m.width=pos_m.width;
            else
                pos_ol_m.width=std::max(0, pos_n.width-std::abs(pos_n.x-pos_m.x));
        else
            pos_ol_m.width=std::min(pos_n.width, pos_m.width);

        pos_ol_n.width=pos_ol_m.width;
    }

    inline cv::Rect getImageSmpl(const Eigen::Vector4f& thetai, const cv::Point2d& xc, double E, int width, int height, cv::Mat& img_smpl){
        cv::Mat img_smpl_big;
        cv::Rect pos;

        double windowlength = utils::OTFCore(thetai, xc, E, img_smpl_big, pos);

        cv::Rect pos_img, pos_BigCanvas;
        utils::getPositionAndSizeOfOverlapedAreaOfOneLocalPatchAndIres(pos, windowlength, width, height, pos_img, pos_BigCanvas);

        img_smpl = img_smpl_big(pos_img);
        return pos_BigCanvas;
    }

    inline cv::Rect getLocalPatchPos(const cv::Point2d& xc, int width, int height, double windowlength){

        auto x_bottom_smpl = std::max(std::floor(xc.x-windowlength), 0.);
        auto x_top_smpl    = std::min(std::ceil(xc.x+windowlength), double(width)-1);

        auto y_bottom_smpl = std::max(std::floor(xc.y-windowlength), 0.);
        auto y_top_smpl    = std::min(std::ceil(xc.y+windowlength), double(height)-1);

        auto nx_smpl = x_top_smpl-x_bottom_smpl+1;
        auto ny_smpl = y_top_smpl-y_bottom_smpl+1;

        return cv::Rect(x_bottom_smpl, y_bottom_smpl, nx_smpl, ny_smpl);
    }

    inline cv::Mat getLocalPatch(const cv::Mat& Irec, const cv::Point2d& xc, int width, int height, double windowlength=2){

        cv::Mat img=Irec(utils::getLocalPatchPos(xc, width, height, windowlength));
        return img;
    }

    inline void readFrameDataNPZ(int it_seq, int it_deb, const std::string& PhaseFile, std::map<int, Particle_ptr>& particles_map){

        auto load_npz = cnpy::npz_load(PhaseFile);

        auto id_v = load_npz["ID"].as_vec<int>();
        auto X_v = load_npz["X"].as_vec<double>();
        auto Y_v = load_npz["Y"].as_vec<double>();
        auto Z_v = load_npz["Z"].as_vec<double>();
        auto E_v = load_npz["E"].as_vec<double>();

        if(it_seq==it_deb){
            for(int p=0; p<id_v.size(); p++){
                particles_map.insert({id_v[p], std::make_unique<Particle>( id_v[p], it_seq, it_seq, 
                                                                            std::vector<cv::Point3d>{cv::Point3d(X_v[p], Y_v[p], Z_v[p])},
                                                                            std::vector<double>{E_v[p]} )} );
            }
        }
        else{
            for(int p=0; p<id_v.size(); p++){
                auto it=particles_map.find(id_v[p]);
                if(it != particles_map.end()){
                    it->second->setit_seq_fin(it_seq);
                    it->second->push_backXcoordElement(cv::Point3d(X_v[p], Y_v[p], Z_v[p]), it_seq);
                    it->second->push_backEvElement(E_v[p], it_seq);
                }
                else{
                    particles_map.insert({ id_v[p], std::make_unique<Particle>( id_v[p], it_seq, it_seq,
                                                                                std::vector<cv::Point3d>{cv::Point3d(X_v[p], Y_v[p], Z_v[p])},
                                                                                std::vector<double>{E_v[p]} )} );
                }
            }
        }
    }

    inline void saveFrameDataNPZ(int it_seq, const std::string& PhaseFile, const std::vector<Particle_ptr>& particles_tgt){

        std::vector<int>    ID_vec; 
        std::vector<double> X_vec, Y_vec, Z_vec, E_vec;

        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_tgt);

        int num_part_frame = 0;
        for(const auto& pr: particles_tgt){
            if(pr->is_ontrack()){
                ID_vec.push_back(pr->ID());
                auto Xc = pr->getCurrentXcoord(it_seq);
                auto E = pr->getCurrentE(it_seq);
                X_vec.push_back(Xc.x);
                Y_vec.push_back(Xc.y);
                Z_vec.push_back(Xc.z);
                E_vec.push_back(E);
                num_part_frame++;
            }
        }

        auto NX = static_cast<long unsigned int>(num_part_frame);
        cnpy::npz_save(PhaseFile, "ID", &ID_vec[0], {NX,1}, "w");
        cnpy::npz_save(PhaseFile, "X", &X_vec[0], {NX,1}, "a");
        cnpy::npz_save(PhaseFile, "Y", &Y_vec[0], {NX,1}, "a");
        cnpy::npz_save(PhaseFile, "Z", &Z_vec[0], {NX,1}, "a");
        cnpy::npz_save(PhaseFile, "E", &E_vec[0], {NX,1}, "a");
    }

   inline void addParticlesToVTKDataSet(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt,
                                         vtkSmartPointer<vtkPoints>& pts, 
                                         vtkSmartPointer<vtkUnstructuredGrid>& dataSet,
                                         int track_min_length){

        auto ID = vtkDoubleArray::New();
        ID->SetNumberOfComponents(1);
        ID->SetNumberOfTuples(particles_tgt.size());
        ID->SetName("TrackID");

        auto velo = vtkDoubleArray::New();
        velo->SetNumberOfComponents(3);
        velo->SetNumberOfTuples(particles_tgt.size());
        velo->SetName("Velocity");

        int part=0;

        double vec[3], vel[3];
        for(const auto& pt: particles_tgt){

            pt->checkIfParticleAppearsOnSnapshot(it_seq_cnt);

            if(pt->is_ontrack() && pt->Xcoord().size()>=track_min_length){

                vec[0]=pt->getCurrentXcoord(it_seq_cnt).x;           
                vec[1]=pt->getCurrentXcoord(it_seq_cnt).y;
                vec[2]=pt->getCurrentXcoord(it_seq_cnt).z;

                auto vtk_id=pts->InsertNextPoint(vec);

                ID->InsertValue(vtk_id, pt->ID());
                part++;

                if(!pt->V().empty()){
                    vel[0]=pt->getCurrentV(it_seq_cnt).x;           
                    vel[1]=pt->getCurrentV(it_seq_cnt).y;
                    vel[2]=pt->getCurrentV(it_seq_cnt).z;
                }            
                else{
                    vel[0]=0;           
                    vel[1]=0;
                    vel[2]=0;
                }
                
                // velo->InsertTuple(vtk_id, vec);
                velo->InsertTuple(vtk_id, vel);
            }
        }

        dataSet->GetPointData()->AddArray(ID);
        dataSet->GetPointData()->AddArray(velo);
    }

    inline cv::Point2d applyWeightedAverage(const cv::Point2d& pt, const cv::Mat_<double>& Img, int N){

        const int np=2*N+1;

        VectorXd  Px(np), Py(np);        
        for(int i=0; i<np; i++){
            Px(i) = ( Px(i)<0 || Px(i)>Img.cols-1 ) ? 0 : pt.x + i - N; 
            Py(i) = ( Py(i)<0 || Py(i)>Img.rows-1 ) ? 0 : pt.y + i - N;    
        }

        MatrixXd  PI(MatrixXd::Zero(np,np));
        for(int i=0; i<np; i++){
            for(int j=0; j<np; j++){
                int x_idx = static_cast<int>(Px(j));
                int y_idx = static_cast<int>(Py(i));
                if( x_idx>=0 && x_idx<=Img.cols-1 && y_idx>=0 && y_idx<=Img.rows-1 ){
                    PI(i,j) = Img(y_idx, x_idx);
                }
            }           
        }

        MatrixXd xc = Px.transpose().replicate(Px.rows(), 1).cwiseProduct(PI);
        MatrixXd yc = Py.replicate(1, Py.rows()).cwiseProduct(PI);

        auto Wxc=cv::Point2d( xc.sum()/PI.sum(), yc.sum()/PI.sum() );

        if(Wxc.x<-0.5 || Wxc.x>Img.cols-0.5 || Wxc.y<-0.5 || Wxc.y>Img.rows-0.5 || isnan(Wxc.x) || isnan(Wxc.y)){
            PI = (PI.array() < 0).select(0, PI);
            xc = Px.transpose().replicate(Px.rows(), 1).cwiseProduct(PI);
            yc = Py.replicate(1, Py.rows()).cwiseProduct(PI);

            auto Wxc_c=cv::Point2d( xc.sum()/PI.sum(), yc.sum()/PI.sum() );
            cout<<str(boost::format("%1%%2%%3%\n") % pt % Wxc % Wxc_c);

            if(Wxc_c.x<-0.5 || Wxc_c.x>Img.cols-0.5 || Wxc_c.y<-0.5 || Wxc_c.y>Img.rows-0.5 || isnan(Wxc_c.x) || isnan(Wxc_c.y)){
                cout<<"applyWeightedAverage"<<endl 
                    <<"pt    "<<pt<<endl
                    <<"Wxc   "<<Wxc_c<<endl
                    <<"Px    "<<Px.transpose()<<endl
                    <<"Py    "<<Py.transpose()<<endl
                    <<"PI"<<endl<<PI<<endl;
                std::abort();
            }
            else
                Wxc=Wxc_c;
        }

        return Wxc;
    }

    inline cv::Point2d applyGaussianFit(const cv::Point2d& pd, const cv::Mat_<double>& Img){

        Eigen::Vector4d LnOfIntenties;
        Eigen::Matrix4d AA;
        Eigen::Matrix<double, 5, 3> PointsMat;

        std::vector<int>    T1_gaussian{-1,0,1,0,0};
        std::vector<int>    T2_gaussian{0,0,0,-1,1};

        Eigen::Matrix<double, 4, 5, Eigen::DontAlign> 
        K_gaussian( (Eigen::Matrix<double, 4, 5, Eigen::DontAlign>() << 1,-1,0,0,0,
                                                                        0,1,-1,0,0,
                                                                        0,0,1,-1,0,
                                                                        0,0,0,1,-1).finished() );

        bool use_weighted_average=false;
        for(int f = 0; f < 5; f++){
            PointsMat(f,0) = static_cast<double>( pd.x + T1_gaussian[f] ); 
            PointsMat(f,1) = static_cast<double>( pd.y + T2_gaussian[f] );

            if( PointsMat(f,0)>=0 && PointsMat(f,0)<=Img.cols-1 && PointsMat(f,1)>=0 && PointsMat(f,1)<=Img.rows-1 ){
                PointsMat(f,2) = Img( static_cast<int>(PointsMat(f,1)), static_cast<int>(PointsMat(f,0)) ); 

                if(PointsMat(f,2)<=0 || (f==1 && PointsMat(1,2)<=PointsMat(0,2)) || (f > 1 && PointsMat(1,2)<PointsMat(f,2))){
                    use_weighted_average=true;
                    break;
                }

                if(f>0) LnOfIntenties(f-1) = -2*log(PointsMat(f-1,2)/PointsMat(f,2));
            }
            else{
                use_weighted_average=true;
                break;
            }
        }

        cv::Point2d Wxc;
        if(!use_weighted_average){
            AA.col(0) =  K_gaussian*(PointsMat.col(1).cwiseProduct(PointsMat.col(1)));
            AA.col(1) =  -2*K_gaussian*PointsMat.col(1);
            AA.col(2) =  K_gaussian*(PointsMat.col(0).cwiseProduct(PointsMat.col(0)));
            AA.col(3) =  -2*K_gaussian*PointsMat.col(0);

            Eigen::Vector4d Variables = AA.lu().solve(LnOfIntenties);
            auto Wxc = cv::Point2d(Variables(3)/Variables(2), Variables(1)/Variables(0));

            if(Wxc.x<-0.5 || Wxc.x>Img.cols-0.5 || Wxc.y<-0.5 || Wxc.y>Img.rows-0.5 || isnan(Wxc.x) || isnan(Wxc.y)){
                cout<<"applyGaussianFit"<<endl 
                    <<"Wxc   "<<Wxc<<endl
                    <<"pd    "<<pd<<endl
                    <<"PointsMat"<<endl<<PointsMat<<endl;
                std::abort();
            }

            return Wxc;
        } 
        else
            return applyWeightedAverage(pd, Img, 1);
    }

    inline cv::Point2d applyWeightedAverageToPatch(cv::Mat img, int N){
        
        if(img.type()!=CV_64FC1) img.convertTo(img, CV_64FC1);
        return applyWeightedAverage(cv::Point2d(1,1), img, N);
    }

    inline cv::Point2d applyGaussianFitToPatch(cv::Mat img){

        if(img.type()!=CV_64FC1) img.convertTo(img, CV_64FC1);
        return applyGaussianFit(cv::Point2d(1,1), img);
    }
}//namespace utils

}//namespace PTL
#endif