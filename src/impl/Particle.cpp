#include "../Particle.hpp"
#include "../Camera.hpp"

namespace PTL{

bool   Particle::delete_verbal_guard_=true;
int    Particle::num_deleted_=0;
int    Particle::num_deleted_tot_=0;

Particle::Particle(){}

Particle::~Particle(){}

Particle::Particle(int part_index, int it_seq_deb, int it_seq_fin)
: part_index_(part_index), it_seq_deb_(it_seq_deb), it_seq_fin_(it_seq_fin){}

Particle::Particle(int part_index, int it_seq_deb, int it_seq_fin, double p_E)
: part_index_(part_index), it_seq_deb_(it_seq_deb), it_seq_fin_(it_seq_fin){
    Ev_.push_back(p_E);
}

Particle::Particle(int part_index, int it_seq_deb, int it_seq_fin, const std::vector<double>& p_Ev)
: part_index_(part_index), it_seq_deb_(it_seq_deb), it_seq_fin_(it_seq_fin){
    Ev_=p_Ev;
}

Particle::Particle(int part_index, int it_seq_deb, int it_seq_fin, const std::vector<cv::Point3d>& Xcoord, const std::vector<double>& p_Ev)
: part_index_(part_index), it_seq_deb_(it_seq_deb), it_seq_fin_(it_seq_fin){
    Xcoord_=Xcoord;
    Ev_=p_Ev;
}

Particle::Particle(int part_index, int it_seq_deb, int it_seq_fin, const std::vector<cv::Point3d>& Xcoord, const std::vector<cv::Point3d>& V, const std::vector<double>& p_Ev)
: part_index_(part_index), it_seq_deb_(it_seq_deb), it_seq_fin_(it_seq_fin){
    Xcoord_=Xcoord;
    V_=V;
    Ev_=p_Ev;
}

Particle::Particle(int part_index, int it_seq_deb, int it_seq_fin, const cv::Point2d& xc1, const cv::Point2d& xc2)
: part_index_(part_index), it_seq_deb_(it_seq_deb), it_seq_fin_(it_seq_fin){
    xcoord_.push_back(std::vector<cv::Point2d>{xc1});
    xcoord_.push_back(std::vector<cv::Point2d>{xc2});
    xcoord_.push_back(std::vector<cv::Point2d>{cv::Point2d(-1,-1)});
    xcoord_.push_back(std::vector<cv::Point2d>{cv::Point2d(-1,-1)});
}

Particle::Particle(int part_index, int it_seq_deb, int it_seq_fin, const cv::Point2d& xc1, const cv::Point2d& xc2,
					const cv::Point2d& xc3, const cv::Point2d& xc4)
: part_index_(part_index), it_seq_deb_(it_seq_deb), it_seq_fin_(it_seq_fin){
    xcoord_.push_back(std::vector<cv::Point2d>{xc1});
    xcoord_.push_back(std::vector<cv::Point2d>{xc2});
    xcoord_.push_back(std::vector<cv::Point2d>{xc3});
    xcoord_.push_back(std::vector<cv::Point2d>{xc4});
}


void Particle::computeVelocity(int it_seq_cnt, double delta_T, Velocity_scheme_Type velo_scheme, bool full_velo_flag){
	
  	VectorXd Vx(this->length()), Vy(this->length()), Vz(this->length()), 
  			 Ax(this->length()), Ay(this->length()), Az(this->length());

	if(velo_scheme == Velocity_scheme_Type::FiniteDifference){
		if(full_velo_flag){
			if(!V_.empty())   V_.clear();
			if(!acc_.empty()) acc_.clear();
			
			assert(this->length()>=2);

			VectorXd Vx_c(this->length()-1), Vy_c(this->length()-1), Vz_c(this->length()-1);

		  	for(int i=0; i<Vx_c.size(); i++){
		      	Vx_c[i] = (Xcoord_.at(i+1).x - Xcoord_.at(i).x)/delta_T;
		      	Vy_c[i] = (Xcoord_.at(i+1).y - Xcoord_.at(i).y)/delta_T;
		      	Vz_c[i] = (Xcoord_.at(i+1).z - Xcoord_.at(i).z)/delta_T;
		  	}

		  	Vx[0]=Vx_c[0];
		  	Vx[this->length()-1]=Vx_c[this->length()-2];
		  	Vy[0]=Vy_c[0];
		  	Vy[this->length()-1]=Vy_c[this->length()-2];      	
		  	Vz[0]=Vz_c[0];
		  	Vz[this->length()-1]=Vz_c[this->length()-2];

		  	for(int i=1; i<Vx_c.size(); i++){
		      	Vx[i] = (Vx_c[i-1] + Vx_c[i])/2.;
		      	Vy[i] = (Vy_c[i-1] + Vy_c[i])/2.;
		      	Vz[i] = (Vz_c[i-1] + Vz_c[i])/2.;
		  	}

		    for(int i=0; i<this->length(); i++){
		    	V_.push_back(cv::Point3d(Vx[i], Vy[i], Vz[i]));
		    	acc_.push_back(cv::Point3d(0,0,0));
				
				if( cv::norm(V_.back())<1e-9 ){
					cout<<"Velocity too small!!!"<<endl;
		            this->printSummary();
		            this->printLastNXcoord(2);
					std::abort();
				}
		    }

			assert(V_.size()==this->length());
		}
		else{
			assert(this->length()>=V_.size());
			V_[it_seq_cnt-it_seq_deb_]   = (this->getCurrentXcoord(it_seq_cnt) - this->getPreviousXcoord(it_seq_cnt))/delta_T;
			V_[it_seq_cnt-it_seq_deb_-1] = (this->getCurrentXcoord(it_seq_cnt) - this->getPreviousOfPreviousXcoord(it_seq_cnt))/(2*delta_T);
		}
	}
	else if(velo_scheme == Velocity_scheme_Type::CurveFitDerivative){
		assert(full_velo_flag);
		if(!V_.empty())   V_.clear();
		if(!acc_.empty()) acc_.clear();

	    VectorXd X(this->length()), Y(this->length()), Z(this->length());
	    for(int i=0; i<this->length(); i++){
	        X(i)=Xcoord_.at(i).x;
	        Y(i)=Xcoord_.at(i).y;
	        Z(i)=Xcoord_.at(i).z;
	    }

	    if(this->length()>=3){
		    polynomialFit1D(X, Vx, Ax);
		    polynomialFit1D(Y, Vy, Ay);
		    polynomialFit1D(Z, Vz, Az);

		    for(int i=0; i<this->length(); i++){
		    	V_.push_back(cv::Point3d(Vx[i], Vy[i], Vz[i]));
		    	acc_.push_back(cv::Point3d(Ax[i], Ay[i], Az[i]));

				if( cv::norm(V_.back())<1e-9 ){
					cout<<"Velocity too small!!!"<<endl;
		            this->printSummary();
		            this->printLastNXcoord(2);
					std::abort();
				}
		    }
	    }
	    else{
	    	V_.assign(2, (Xcoord_[1]-Xcoord_[0])/delta_T);
	    	acc_.assign(2, cv::Point3d(0,0,0));
	    }

		assert(V_.size()==this->length());
	}
	else
		std::abort();
}

void Particle::assignInitialIntensity(int it_seq_cnt){

	if(Ev_.empty()){
		std::cerr<<"Empty intensity vector"<<endl;
		this->printSummary();
		std::abort();
	}
	else{
		this->push_backEvElement(Ev_.back(), it_seq_cnt);
	}
}

double Particle::sumResidualForParticlep(int it_seq_cnt, int n_cam, int eval_window){
    
    double res=0.;

    cv::Mat img_Res_par, imgp, imgp_onimgRespar_canvas;

    cv::Rect pos_overlap_img, pos_ovelap_imgRespar;

    int it_img = it_seq_cnt - it_seq_fin_;
    assert(it_img==0);

    for(int i_cam=0; i_cam<n_cam; i_cam++){

        if(on_image_[i_cam] == 1){

            img_Res_par.create(imgRespar_[i_cam][0].size(), CV_32FC1);

            auto pos_img=imgPos_[i_cam][it_img];
            img_[i_cam][it_img].convertTo(imgp, CV_32FC1);

            auto pos_imgRespar=imgResparPos_[i_cam][it_img];
            imgRespar_[i_cam][it_img].copyTo(img_Res_par);

            imgp_onimgRespar_canvas=cv::Mat::zeros(img_Res_par.size(), CV_32FC1);

            cv::Rect pos_imgRespar_win = pos_imgRespar;
            pos_imgRespar_win.y        = pos_imgRespar.y+std::max(0, static_cast<int>(pos_imgRespar.height)/2-eval_window);
            pos_imgRespar_win.x        = pos_imgRespar.x+std::max(0, static_cast<int>(pos_imgRespar.width)/2 -eval_window);
            pos_imgRespar_win.height   = std::min(2*eval_window+1, pos_imgRespar.height);
            pos_imgRespar_win.width    = std::min(2*eval_window+1, pos_imgRespar.width);

            if( std::abs(pos_imgRespar_win.y-pos_img.y)<std::max(pos_img.height, pos_imgRespar_win.height) &&
                std::abs(pos_imgRespar_win.x-pos_img.x)<std::max(pos_img.width, pos_imgRespar_win.width) ){

                utils::getPositionAndSizeOfOverlapedAreaOfTwoLocalPatches(pos_imgRespar_win, pos_img, pos_ovelap_imgRespar, pos_overlap_img);

                if(pos_ovelap_imgRespar.area()!=0){

                    imgp(pos_overlap_img).copyTo( imgp_onimgRespar_canvas(pos_ovelap_imgRespar) );

                    cv::subtract(imgRespar_[i_cam][it_img], imgp_onimgRespar_canvas, img_Res_par);

                    auto window_rect_y=std::max(0, static_cast<int>(pos_imgRespar.height)/2-eval_window);
                    auto window_rect_x=std::max(0, static_cast<int>(pos_imgRespar.width)/2-eval_window);
                    auto window_rect_size1=std::min(2*eval_window+1, pos_imgRespar.height);
                    auto window_rect_size0=std::min(2*eval_window+1, pos_imgRespar.width);

                    res+=std::pow(cv::norm( img_Res_par(  cv::Rect( window_rect_x, window_rect_y, window_rect_size0, window_rect_size1) ) ), 2);
                }
                else
                    res=0.;
            }
            else
                res=0.;
        }
    }

    return res;
}


void Particle::projectToImageSpace(int it_seq_cnt, const std::vector<Camera_ptr>& cam){
	assert(Xcoord_.size()>=it_seq_cnt-it_seq_deb_+1);
	assert(Ev_.size()>=it_seq_cnt-it_seq_deb_+1);

	this->projectToImageSpaceCore(cam, Xcoord_.at(it_seq_cnt-it_seq_deb_), Ev_.at(it_seq_cnt-it_seq_deb_));
}

void Particle::projectToImageSpaceCore(const std::vector<Camera_ptr>& cam, const cv::Point3d& Xc_il, double E_il){

	int n_cam=cam.size();

	assert(Camera::psf_type_==Camera::psf_Type::ErrorFunction);

	cv::Mat Imgi_smpl_Grey;
    cv::Rect pos;

	on_image_.assign(n_cam, 1);

	double E_cnt=E_il;

	for(int i_cam=0; i_cam<n_cam; i_cam++){

		auto xc=cam[i_cam]->mappingFunction(Xc_il);

		if(xc.x < 0. || xc.x > static_cast<double>(cam[i_cam]->n_width() )-1. ||
		   xc.y < 0. || xc.y > static_cast<double>(cam[i_cam]->n_height())-1. )
		{
			std::cerr<<"Particle "<<part_index_<<" coordinate out of image domain at camera "<<i_cam<<"!"<<endl
				<<"with Xc "<<Xc_il<<" and xc "<<xc<<endl;

			on_image_[i_cam]=0;
			Imgi_smpl_Grey=cv::Mat(1, 1, CV_32FC1, cv::Scalar(0));
			pos = cv::Rect(0,0,0,0);
		}
		else
		{
			utils::OTFCore(cam[i_cam]->getPSFelement(Xc_il), xc, E_cnt, Imgi_smpl_Grey, pos);

			Imgi_smpl_Grey.convertTo(Imgi_smpl_Grey, Camera::opencv_mat_type_);
		}

		if(img_.size()<n_cam)		img_.push_back(std::vector<cv::Mat>(1, Imgi_smpl_Grey));
		else						img_.at(i_cam).push_back(Imgi_smpl_Grey);

		if(imgPos_.size()<n_cam)	imgPos_.push_back(std::vector<cv::Rect>(1, pos));
		else						imgPos_.at(i_cam).push_back(pos);

		Imgi_smpl_Grey.release();
	}

	if(std::count(on_image_.begin(), on_image_.end(), 0) > 2){
		std::cerr<<"Particle "<<part_index_<<" does not appear on more than 2 images!"<<endl;
		// std::abort();
	}

    assert(img_.size()==n_cam);
    assert(imgPos_.size()==n_cam);
}
}//namespace PTL