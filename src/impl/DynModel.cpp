#include "../DynModel.hpp"

namespace PTL{

DynModel::DynModel(){}

DynModel::~DynModel(){}

DynModel::DynModel(DynModel_Type dynmodel_type):dynmodel_type_(dynmodel_type){
    if(dynmodel_type_== DynModel::DynModel_Type::LinearPolynomial)     cout<<"using Linear Polynomials for forecast"<<endl;
    else if(dynmodel_type_==DynModel::DynModel_Type::WienerFilter)     cout<<"using Wiener Filter for forecast"<<endl;
}

void DynModel::predictNextPaticlesPosition(const std::vector<Particle_ptr>& particles_ana_in, int it_seq_cnt){
    assert(dynmodel_type_==DynModel::DynModel_Type::LinearPolynomial || dynmodel_type_==DynModel::DynModel_Type::WienerFilter || dynmodel_type_==DynModel::DynModel_Type::LCSTracker);
#ifdef INTELTBB
    tbb::parallel_for( tbb::blocked_range<int>(0, particles_ana_in.size()),
                        [&](const tbb::blocked_range<int>& r){ for(int p=r.begin(); p!=r.end(); ++p){
#else
    for(int p=0; p<particles_ana_in.size(); ++p){
#endif
        switch(dynmodel_type_){
        case DynModel_Type::LinearPolynomial:
        this->LinearPolynomialPredictorForSingleParticle(particles_ana_in[p].get(), it_seq_cnt); break;
        case DynModel_Type::WienerFilter:
        this->WienerFilterPredictorForSingleParticle(particles_ana_in[p].get(), it_seq_cnt); break;
        default:
        std::cerr<<"Bad configuration for dynmodel_type_"<<endl;
        }
    }
#ifdef INTELTBB
    });   
#endif
}

void DynModel::LinearPolynomialPredictorForSingleParticle(Particle* particle_in, int it_seq_cnt){

    if(particle_in->is_ontrack() && particle_in->is_to_be_tracked()){

        VectorXd X(particle_in->length()), Y(particle_in->length()), Z(particle_in->length());
        for(int i=0; i<particle_in->length(); i++){
            X(i)=particle_in->Xcoord().at(i).x;
            Y(i)=particle_in->Xcoord().at(i).y;
            Z(i)=particle_in->Xcoord().at(i).z;
        }

        int Nsample = 3;
        int Nbasis  = 3;

        auto Xp = polynomialFit1D(X, false, Nsample, Nbasis);
        auto Yp = polynomialFit1D(Y, false, Nsample, Nbasis);
        auto Zp = polynomialFit1D(Z, false, Nsample, Nbasis);

        particle_in->push_backXcoordElement(cv::Point3d(Xp, Yp, Zp), it_seq_cnt);
        particle_in->setit_seq_fin(it_seq_cnt);
    }
}

void DynModel::WienerFilterPredictorForSingleParticle(Particle* particle_in, int it_seq_cnt){

    if(particle_in->is_ontrack() && particle_in->is_to_be_tracked()){
        int Nbasis = (particle_in->length()==3 ? 2 : (particle_in->length()>10 ? 5 : 3)); 
        int Nsample = Nbasis + 1;

        VectorXd X(Nsample),Y(Nsample),Z(Nsample);

        for(int i=0; i<Nsample; i++){
            X[i] = particle_in->Xcoord().at(particle_in->length()-Nsample+i).x;
            Y[i] = particle_in->Xcoord().at(particle_in->length()-Nsample+i).y;
            Z[i] = particle_in->Xcoord().at(particle_in->length()-Nsample+i).z;
        }

        auto Xp = WienerLMSFit1D(X, Nbasis);        
        auto Yp = WienerLMSFit1D(Y, Nbasis);
        auto Zp = WienerLMSFit1D(Z, Nbasis);

        particle_in->push_backXcoordElement(cv::Point3d(Xp, Yp, Zp), it_seq_cnt);
        particle_in->setit_seq_fin(it_seq_cnt);
    }
}

}//namespace PTL
