#include "../LPTCHAL.hpp"

namespace PTL{

LPTCHAL::LPTCHAL(){}

LPTCHAL::~LPTCHAL(){}

LPTCHAL::LPTCHAL(int n_part, Chal_case_Type case_type)
	:Dataset(),case_type_(case_type){
	switch(n_part){
		case 10000:  PPP_=0.005; break;
		case 50000:  PPP_=0.025; break;
		case 100000: PPP_=0.05;  break;
		case 160000: PPP_=0.08;  break;
		case 240000: PPP_=0.12;  break;
		case 320000: PPP_=0.16;  break;
		case 400000: PPP_=0.2;   break;
	}
}

void LPTCHAL::setIMGData(const std::string& img_path, int it_deb, int it_tot, const std::vector<Camera_ptr>& cam, const std::string& img_dir){

	for(auto& ic: cam){
		if(!ic->Iproj().empty()) ic->clearIproj();
		for(int it_rec=it_deb; it_rec<=it_tot; it_rec++){

			auto PPP_str = PPP_*1000;
			auto it      = it_rec-1;
			auto case_type_str = case_type_==Chal_case_Type::TR ? "TR" : "TP";

			auto img_file=str(boost::format("%1$s%5$s_ppp_0_%2$03d/%5$s_ppp_0_%2$03d_I%3$04d_%4$d.tif") % img_path % PPP_str % it % ic->i_cam_ % case_type_str);
			cout<<"img_file "<<img_file<<endl;
			
			cv::Mat img=cv::imread(img_file, cv::IMREAD_ANYDEPTH);

			ic->setIprojElement(img);
			ic->saveImageProjection(it_rec, "cam", img_dir, true);
		}
	}
}

void LPTCHAL::readMarkPositionTable(const std::string& calib_file, int nViews,
									std::vector<std::vector<std::vector<cv::Point3d>>>& Xcoord_obj_cams,
									std::vector<std::vector<std::vector<cv::Point2d>>>& xcoord_img,
									int nXc_x_rig, int nXc_y_rig){

	std::ifstream calibFile(calib_file);
    std::string line;
    std::vector<std::string> X_v, Y_v, Z_v, x1_v, y1_v, x2_v, y2_v, x3_v, y3_v, x4_v, y4_v;
    std::string X, Y, Z, x1, y1, x2, y2, x3, y3, x4, y4;

	if(calibFile.is_open()){
        while(std::getline(calibFile, line)){
            std::string::size_type nc=line.find("X");
            if(nc != std::string::npos)
                line.erase(nc);

            std::istringstream is(line);
            if(is >> X >> Y >> Z >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 >> x4 >> y4){
                X_v.push_back(X);
                Y_v.push_back(Y);
                Z_v.push_back(Z);
                x1_v.push_back(x1);
                y1_v.push_back(y1);
                x2_v.push_back(x2);
                y2_v.push_back(y2);
                x3_v.push_back(x3);
                y3_v.push_back(y3);
                x4_v.push_back(x4);
                y4_v.push_back(y4);
            }           
        }
	}
	else{
        std::cerr<<"Error opening calibration data file "<<calib_file<<endl;
        std::abort();
    }

	std::vector<std::vector<cv::Point3d>> Xcoord_per_cam(nViews, std::vector<cv::Point3d>());
	std::vector<std::vector<cv::Point2d>> xcoord_per_cam1(nViews, std::vector<cv::Point2d>()),
										  xcoord_per_cam2(nViews, std::vector<cv::Point2d>()), 
										  xcoord_per_cam3(nViews, std::vector<cv::Point2d>()),
										  xcoord_per_cam4(nViews, std::vector<cv::Point2d>());
	
	for(int j=0; j<nXc_y_rig; j++){
		for(int z=0; z<nViews; z++){
			for(int i=0; i<nXc_x_rig; i++){
				int idx = i+z*nXc_x_rig+j*nXc_x_rig*nViews;

				Xcoord_per_cam[z].push_back( cv::Point3d(std::stod(X_v[idx]), std::stod(Y_v[idx]), std::stod(Z_v[idx])) );
				xcoord_per_cam1[z].push_back( cv::Point2d(std::stod(x1_v[idx]), std::stod(y1_v[idx]) ) );
				xcoord_per_cam2[z].push_back( cv::Point2d(std::stod(x2_v[idx]), std::stod(y2_v[idx]) ) );
				xcoord_per_cam3[z].push_back( cv::Point2d(std::stod(x3_v[idx]), std::stod(y3_v[idx]) ) );
				xcoord_per_cam4[z].push_back( cv::Point2d(std::stod(x4_v[idx]), std::stod(y4_v[idx]) ) );
			}
		}
	}

	Xcoord_obj_cams.assign(4, Xcoord_per_cam);
	xcoord_img.push_back(xcoord_per_cam1);
	xcoord_img.push_back(xcoord_per_cam2);
	xcoord_img.push_back(xcoord_per_cam3);
	xcoord_img.push_back(xcoord_per_cam4);
}

}//namespace PTL