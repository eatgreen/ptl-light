#include "../Calibration.hpp"

namespace PTL{

class MappingFunctionCostFunctor{
    public:
        MappingFunctionCostFunctor(double* XW, double* xi):
            X(XW[0]), Y(XW[1]), Z(XW[2]), x(xi[0]), y(xi[1]) {}

        template<typename T>
        bool operator()(const T* params, T* residual) const {
            const T r1 = params[0];
            const T r2 = params[1];
            const T r3 = params[2];
            const T t1 = params[3];
            const T t2 = params[4];
            const T t3 = params[5];
            const T d1 = params[6];
            const T d2 = params[7];
            const T d3 = params[8];
            const T c1 = params[9];
            const T c2 = params[10];
            const T c3 = params[11];
            const T c4 = params[12];
            if(c1>0. && c2>0. && c3>0. && c4>0. && r1*r1 + r2*r2 + r3*r3>0.){
                T theta = sqrt( r1*r1 + r2*r2 + r3*r3 );
                T r1n = r1/theta;
                T r2n = r2/theta;
                T r3n = r3/theta;

                T R11 = cos(theta) + (1.-cos(theta))*r1n*r1n;
                T R12 = (1.-cos(theta))*r1n*r2n - sin(theta)*r3n;
                T R13 = (1.-cos(theta))*r1n*r3n + sin(theta)*r2n;

                T R21 = (1.-cos(theta))*r2n*r1n + sin(theta)*r3n;
                T R22 = cos(theta) + (1.-cos(theta))*r2n*r2n;
                T R23 = (1.-cos(theta))*r2n*r3n - sin(theta)*r1n;

                T R31 = (1.-cos(theta))*r3n*r1n - sin(theta)*r2n;
                T R32 = (1.-cos(theta))*r3n*r2n + sin(theta)*r1n;
                T R33 = cos(theta) + (1.-cos(theta))*r3n*r3n;

                T XC1 = R11*X + R12*Y + R13*Z + t1;
                T XC2 = R21*X + R22*Y + R23*Z + t2;
                T XC3 = R31*X + R32*Y + R33*Z + t3;

                T xu = XC1/XC3;
                T yu = XC2/XC3;

                T r_sq = xu*xu+yu*yu;

                T xd = xu*(1. + d1*r_sq + d2*r_sq*r_sq + d3*r_sq*r_sq*r_sq);
                T yd = yu*(1. + d1*r_sq + d2*r_sq*r_sq + d3*r_sq*r_sq*r_sq);

                T xi = c1*xd + c2;
                T yi = c3*yd + c4;

                T I = xi - x;
                T J = yi - y;

                T H = 0.5*(I*I + J*J);

                residual[0] = H;

                return true;
            }
            else
                return false;
        }

    private:
        const double X, Y, Z, x, y;
};

//TODO
class OTFCostFunctor {
    public:
        OTFCostFunctor(int width, int height, const cv::Mat& Imgi_real_float_i, const cv::Point2d& xc_i, double E):
            width_(width), height_(height), Imgi_real_float_i_(Imgi_real_float_i), xc_i_(xc_i), E_(E) {}

        bool operator()(const double* const x, double* residual) const {
            if(x[0]>0 && x[0]<1 && x[1]>0 && x[1]<1 && x[2]>0 && x[2]<1){
                
                Eigen::Vector4f psf;
                psf<<float(x[0]), 0, float(x[1]), float(x[2]); 

                cv::Mat  Imgi_smpl_i;
                utils::getImageSmpl(psf, xc_i_, E_, width_, height_, Imgi_smpl_i);

                cv::Mat Ires_i;
                cv::subtract(Imgi_real_float_i_, Imgi_smpl_i, Ires_i);

                residual[0] = double( std::pow(cv::norm(Ires_i),2) );
                return true;
            }
            else
                return false;
        }

    private:
        int         width_;
        int         height_;
        cv::Mat     Imgi_real_float_i_;
        cv::Point2d xc_i_;
        double      E_;
};

class OTFIntensityCostFunctor {
    public:
        OTFIntensityCostFunctor(float i, float j, int width, int height, const cv::Mat& Imgi_real_float_i, const cv::Point2d& xc_i, double E):
            i_(i), j_(j), width_(width), height_(height), Imgi_real_float_i_(Imgi_real_float_i), xc_i_(xc_i), E_(E) {}

        bool operator()(const double* const x, double* residual) const {
            if(x[0]>1000 && x[0]<65535){
                Eigen::Vector4f psf;
                psf<<i_, 0, j_, float(x[0]); 

                cv::Mat  Imgi_smpl_i;
                utils::getImageSmpl(psf, xc_i_, E_, width_, height_, Imgi_smpl_i);

                cv::Mat Ires_i;
                cv::subtract(Imgi_real_float_i_, Imgi_smpl_i, Ires_i);

                residual[0] = double( std::pow(cv::norm(Ires_i),2) );
                return true;
            }
            else
                return false;
        }

    private:
        float       i_;
        float       j_;
        int         width_;
        int         height_;
        cv::Mat     Imgi_real_float_i_;
        cv::Point2d xc_i_;
        double      E_;
};

class OTFRadiusCostFunctor {
    public:
        OTFRadiusCostFunctor(float k, int width, int height, const cv::Mat& Imgi_real_float_i, const cv::Point2d& xc_i, double E):
            k_(k), width_(width), height_(height), Imgi_real_float_i_(Imgi_real_float_i), xc_i_(xc_i), E_(E) {}

        bool operator()(const double* const x, double* residual) const {
            if(x[0]>0.9 && x[0]<9 && x[1]>0.9 && x[1]<9){
                Eigen::Vector4f psf;
                psf<<float(x[0]), 0, float(x[1]), k_; 

                cv::Mat Imgi_smpl_i;
                utils::getImageSmpl(psf, xc_i_, E_, width_, height_, Imgi_smpl_i);

                cv::Mat Ires_i;
                cv::subtract(Imgi_real_float_i_, Imgi_smpl_i, Ires_i);

                residual[0] = double( std::pow(cv::norm(Ires_i),2) );
                return true;
            }
            else
                return false;
        }

    private:
        float       k_;
        int         width_;
        int         height_;
        cv::Mat     Imgi_real_float_i_;
        cv::Point2d xc_i_;
        double      E_;
};

Calibration::Calibration(){}

Calibration::~Calibration(){}

Calibration::Calibration(Source_Type source, int n_cam, 
                         const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, 
                         const cv::Point2i& nXc_rig, int nViews, bool Right_handed_flag)
                        : source_(source), n_cam_(n_cam), 
                          Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), nXc_rig_(nXc_rig), nViews_(nViews), Right_handed_flag_(Right_handed_flag) {}

void Calibration::getFundamentalMatrix(int it_seq, Camera* cam1, Camera* cam2, 
                                        const std::vector<Particle_ptr>& particles_vsc){

    if(cam1->i_cam_==cam2->i_cam_){
        cam1->F_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
    }
    else{
        cout<<"get Fundamental Matrix"<<endl;

        std::vector<cv::Point2f> ProjPoints_idx1, ProjPoints_idx2;

        int num_train_set = 50;
        for(int i=0; i<num_train_set; i++){
            auto xc1 = cam1->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            auto xc2 = cam2->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            ProjPoints_idx1.push_back( cv::Point2f(float(xc1.x), float(xc1.y)) );
            ProjPoints_idx2.push_back( cv::Point2f(float(xc2.x), float(xc2.y)) );
        }

        std::vector<cv::Point2f>   xc1_u, xc2_u;
        cv::undistortPoints(ProjPoints_idx1, xc1_u, cam1->cameraMatrix(), cam1->distCoeffs(), cv::noArray(), cam1->cameraMatrix());
        cv::undistortPoints(ProjPoints_idx2, xc2_u, cam2->cameraMatrix(), cam2->distCoeffs(), cv::noArray(), cam2->cameraMatrix());

        cam1->F_betw_cams_.push_back( cv::findFundamentalMat( xc1_u, xc2_u, cv::FM_RANSAC, 1, 0.99) );
    }
}

void Calibration::computeFundamentalMatrixError(int it_seq, Camera* cam1, Camera* cam2,
                                        const std::vector<Particle_ptr>& particles_vsc,
                                        std::vector<std::vector<double>>& err_betw_cams){

    bool undistort_flag = (cv::norm(cam1->distCoeffs())<1e-3 && cv::norm(cam2->distCoeffs())<1e-3);

    if(cam1->i_cam_!=cam2->i_cam_){
        cout<<"Between "<<cam1->i_cam_<<"th camera and "<<cam2->i_cam_<<"th camera ";

        std::vector<cv::Point2d>    Points_idx1_d, Points_idx1_u;
        std::vector<cv::Point2d>    Points_idx2_d, Points_idx2_u;

        for(int i=0; i<particles_vsc.size(); i++){
            auto xc1 = cam1->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            auto xc2 = cam2->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            
            Points_idx1_d.push_back(xc1);
            Points_idx2_d.push_back(xc2);
        }
            
        if(undistort_flag){
            cout<<"Ignore small distortion ####################"<<endl;
            Points_idx1_u = Points_idx1_d;
            Points_idx2_u = Points_idx2_d;
        }
        else{
            cv::undistortPoints(Points_idx1_d, Points_idx1_u, cam1->cameraMatrix(), cam1->distCoeffs(), cv::noArray(), cam1->cameraMatrix()); 
            cv::undistortPoints(Points_idx2_d, Points_idx2_u, cam2->cameraMatrix(), cam2->distCoeffs(), cv::noArray(), cam2->cameraMatrix()); 
        }   

        // cv::Mat_<double> epiline12 = cam1->F_betw_cams().at(cam2->i_cam())*homo_xc1;
        // double epi_norm = sqrt(epiline12(0)*epiline12(0)+epiline12(1)*epiline12(1));
        // auto   err_dist = abs(homo_xc2.dot(epiline12))/epi_norm;

        cv::Mat F12_f;
        cam1->F_betw_cams().at(cam2->i_cam()).convertTo(F12_f, CV_32F);

        std::vector<cv::Point2f> Point_idx1_f_u;
        for(auto& p: Points_idx1_u){
            Point_idx1_f_u.push_back(cv::Point2f(float(p.x), float(p.y)));
        }

        cv::Mat epiline12_vec;
        cv::computeCorrespondEpilines(Point_idx1_f_u, 1, F12_f, epiline12_vec);

        double err_dist_sum=0;
        for(int i=0; i<particles_vsc.size(); i++){
            auto vec3f = epiline12_vec.at<cv::Vec3f>(0,i);
            cv::Mat_<double> epiline_double = (cv::Mat_<double>(3,1) << double(vec3f(0)), double(vec3f(1)), double(vec3f(2))); 
            cv::Mat_<double> homo_xc2 = (cv::Mat_<double>(3,1)<< Points_idx2_u[i].x, Points_idx2_u[i].y, 1);
            auto err_dist = abs(homo_xc2.dot(epiline_double));
            err_dist_sum += err_dist;
        }
        cout<<"err dist 2D(px) "<<err_dist_sum/static_cast<double>(particles_vsc.size())<<endl;

        err_betw_cams[cam1->i_cam_][cam2->i_cam_] += err_dist_sum/static_cast<double>(particles_vsc.size());
    }

}

void Calibration::collectSamplePointsForVSC(int it_seq, Camera* cam,
                                            const std::vector<cv::Point2d>& PTVPoints,
                                            const std::vector<Particle_ptr>& particles_vsc,
                                            double max_threshold,
                                            std::vector<cv::Point3d>& obj, std::vector<cv::Point2d>& img){

    cout<<"For cam "<<cam->i_cam_<<endl;

    std::vector<cv::Point2d> ProjPoints;
    auto ProjPoints_cloud = pcl::make_shared<pcl::PointCloud<pcl::PointXY>>();
    for(const auto& pv: particles_vsc){
        ProjPoints.push_back( cam->mappingFunction(pv->getCurrentXcoord(it_seq)) );
        ProjPoints_cloud->push_back( cv2pcl(ProjPoints.back()) );
    }

    auto ProjPoints_cloud_kdtree = pcl::KdTreeFLANN<pcl::PointXY>();
    ProjPoints_cloud_kdtree.setInputCloud(ProjPoints_cloud);

    std::vector<int> VSCPoints_flag(particles_vsc.size(), 1);

    double max_threshold_vsc = 0.01;

    cout<<"start with max_threshold_vsc ";
    while(max_threshold_vsc<=max_threshold){
        cout<<" "<<max_threshold_vsc;
        for(const auto& xc: PTVPoints){

            std::vector<int>   pointIdxNKNSearch(1);
            std::vector<float> pointNKNSquaredDistance(1);

            if( ProjPoints_cloud_kdtree.nearestKSearch(cv2pcl(xc), 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
            {
                if(sqrt(pointNKNSquaredDistance[0])<max_threshold_vsc){
                    if(VSCPoints_flag[pointIdxNKNSearch[0]] != 0){
                        VSCPoints_flag[pointIdxNKNSearch[0]] = 0;
                        ProjPoints[pointIdxNKNSearch[0]] = xc;
                    }
                }
            }
        }

        max_threshold_vsc*=2;

        if(std::count(VSCPoints_flag.begin(), VSCPoints_flag.end(), 0) > 0.8*particles_vsc.size()){
            max_threshold_vsc = 10*max_threshold;
        }
    }
    cout<<endl;
    cout<<"Found "<<std::count(VSCPoints_flag.begin(), VSCPoints_flag.end(), 0)<<" number correspondences"<<endl;

    for(int i=0; i<ProjPoints.size(); i++){
        if( VSCPoints_flag[i] == 0 ){
            obj.push_back(particles_vsc[i]->getCurrentXcoord(it_seq));
            img.push_back(ProjPoints[i]);
        }
    }
}

void Calibration::volumeSelfCalibration(Camera* cam, const std::vector<cv::Point3d>& obj, const std::vector<cv::Point2d>& img){

    double initial_X[13]={1,1,1,0,0,0,0,0,0,4000,640,4000,400};
    double X[13]={1,1,1,0,0,0,0,0,0,4000,640,4000,400};

    ceres::Problem problem;

    for(int i=0; i<obj.size(); i++){

        double XW[3], xi[2];
        XW[0] = obj[i].x;
        XW[1] = obj[i].y;
        XW[2] = obj[i].z;
        xi[0] = img[i].x;
        xi[1] = img[i].y;

        ceres::CostFunction* cost_function = new ceres::AutoDiffCostFunction<MappingFunctionCostFunctor, 1, 13>(
                                                 new MappingFunctionCostFunctor(XW, xi));
       
        problem.AddResidualBlock( cost_function, new ceres::TrivialLoss(), &X[0]);       
    }

    ceres::Solver::Options options;
    // options.minimizer_progress_to_stdout = true;
    // options.minimizer_type = ceres::LINE_SEARCH;
    // options.use_nonmonotonic_steps = true;
    // options.trust_region_strategy_type = ceres::DOGLEG;
    options.max_num_iterations = 100;
    // options.initial_trust_region_radius = 1e3;
    options.function_tolerance = 1e-9;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    // cout << summary.BriefReport() << "\n";
    cout << summary.FullReport() << "\n";
}


void Calibration::volumeSelfCalibration(Camera* cam, const std::vector<cv::Point3d>& obj, const std::vector<cv::Point2d>& img, 
                                        double PixelPerMmFactor, const cv::Point3d& normalizationFactor){

    double err=0.;
    for(int i=0; i<obj.size(); i++){
        err += cv::norm( img[i] - cam->mappingFunction(obj[i]) );
    }
    cam->num_samples_tot_ = obj.size();
    cout<<"Sample points in VSC: "<<cam->num_samples_tot_<<endl;
    cout<<"Before VSC, mean error is "<<err/static_cast<double>(cam->num_samples_tot_)<<" px"<<endl; 

    if(cam->z_order_==2)        cam->n_coef_=19;
    else if(cam->z_order_==3)   cam->n_coef_=20;

    if(cam->num_samples_tot_>=cam->n_coef_){

        cam->polynomial_type_     = Camera::Polynomial_Type::Single;
        cam->PixelPerMmFactor_    = PixelPerMmFactor;
        cam->normalizationFactor_ = normalizationFactor;

        cv::Mat_<double> design_mat,b_x,b_y,wx,wy,design_vec;

        design_mat.create(cam->num_samples_tot_, cam->n_coef_);

        int num_sample=0;
        for(const auto& Xc: obj){

            auto kX=cam->PixelPerMmFactor_*Xc.x;
            auto kY=cam->PixelPerMmFactor_*Xc.y;
            auto kZ=cam->PixelPerMmFactor_*Xc.z;

            auto X=kX/cam->normalizationFactor_.x;
            auto Y=kY/cam->normalizationFactor_.y;
            auto Z=kZ/cam->normalizationFactor_.z;

            if(cam->n_coef_==19)
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                                                                std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2));
            else
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z,
                                                                std::pow(Y,2)*Z, Xc.x*std::pow(Z,2), Y*std::pow(Z,2), std::pow(Z,3));
            
            design_vec.copyTo( design_mat.row(num_sample) );
            num_sample++;
        }

        b_x.create(cam->num_samples_tot_, 1);
        b_y.create(cam->num_samples_tot_, 1);
        for(int i=0; i<obj.size(); i++){
            auto xc = cv::Point2d(obj[i].x, obj[i].y)*cam->PixelPerMmFactor_ - img[i];
            b_x(i)=xc.x;
            b_y(i)=xc.y;
        }

        wx.create(cam->n_coef_, 1);
        wy.create(cam->n_coef_, 1);

        cv::solve(design_mat, b_x, wx, cv::DECOMP_SVD);
        cv::solve(design_mat, b_y, wy, cv::DECOMP_SVD);

        VectorXd delta_a, delta_b;

        cv::cv2eigen(wx, delta_a);
        cv::cv2eigen(wy, delta_b);

        cam->a_ = delta_a;
        cam->b_ = delta_b;

        err=0.;
        for(int i=0; i<obj.size(); i++){
            err += cv::norm( img[i] - cam->mappingFunction(obj[i]) );
        }
        cout<<"After  VSC, mean error is "<<err/static_cast<double>(cam->num_samples_tot_)<<" px"<<endl; 
    }
    else{
        cout<<"Too few points"<<endl;
        std::abort();
    }
}

std::vector<std::vector<int>>
Calibration::getIdxInEachSubBlock(const std::vector<cv::Point3d>& obj){

    std::vector<double> xgrid_psf(Camera::xgrid_psf_), ygrid_psf(Camera::ygrid_psf_), zgrid_psf(Camera::zgrid_psf_);
    cv::Point3d dXc_psf(Camera::dXc_psf_);

    int num_block=(xgrid_psf.size()-1)*(ygrid_psf.size()-1)*(zgrid_psf.size()-1);
    std::vector<std::vector<int>> idx_block(num_block);

    for(int j=0; j<obj.size(); j++){
        auto Xc = obj[j];
        utils::maintainPositionInRange(Xc, Xc_top_, Xc_bottom_);

        auto pxl=find_if(std::begin(xgrid_psf), std::end(xgrid_psf), [=](const double& x) { return Xc.x-x<dXc_psf.x; } );
        auto pyl=find_if(std::begin(ygrid_psf), std::end(ygrid_psf), [=](const double& y) { return Xc.y-y<dXc_psf.y; } );
        auto pzl=find_if(std::begin(zgrid_psf), std::end(zgrid_psf), [=](const double& z) { return Xc.z-z<dXc_psf.z; } );

        if(pxl==std::end(xgrid_psf)-1) pxl--;
        if(pyl==std::end(ygrid_psf)-1) pyl--;
        if(pzl==std::end(zgrid_psf)-1) pzl--;

        auto xl_idx=std::distance(std::begin(xgrid_psf), pxl);
        auto yl_idx=std::distance(std::begin(ygrid_psf), pyl);
        auto zl_idx=std::distance(std::begin(zgrid_psf), pzl);

        int psf_idx=zl_idx*(xgrid_psf.size()-1)*(ygrid_psf.size()-1) + yl_idx*(xgrid_psf.size()-1) + xl_idx;
        idx_block.at(psf_idx).push_back(j);
    }

    return idx_block;
}

void Calibration::collectSamplePointsForOTF(
#ifdef INTELTBB
        const tbb::concurrent_vector<pcl::PointXYZI>&                  CorrespondentWorldPoints,
        const std::vector<tbb::concurrent_vector<cv::Point2d>>&        CorrespondentImagePoints,
#else
        const std::vector<pcl::PointXYZI>&                             CorrespondentWorldPoints,
        const std::vector<std::vector<cv::Point2d>>&                   CorrespondentImagePoints,
#endif
        std::vector<cv::Point3d>& obj,
        std::vector<std::vector<cv::Point2d>>& img_cams){

    for(int j=0; j<CorrespondentWorldPoints.size(); j++){
        obj.push_back( pcl2cv(CorrespondentWorldPoints[j]) );
        img_cams[0].push_back(CorrespondentImagePoints[0][j]);
        img_cams[1].push_back(CorrespondentImagePoints[1][j]);
        img_cams[2].push_back(CorrespondentImagePoints[2][j]);
        img_cams[3].push_back(CorrespondentImagePoints[3][j]);
    }
}


void Calibration::calibrateOTF(Camera* cam, const OTFCalib_Type& otf_calib_type,
                                const std::vector<cv::Point3d>& obj, const std::vector<cv::Point2d>& img){

    auto idx_block = this->getIdxInEachSubBlock(obj);

    std::vector<int> idx_total;
    if(otf_calib_type==Calibration::OTFCalib_Type::Constant){
        for(int j=0; j<obj.size(); j++){
            idx_total.push_back(j);
        }
    }

    const int i_cam = cam->i_cam();
    cout<<"Calibrate for camera "<<i_cam<<" ########################################################################"<<endl;

    if(obj.size()>100){

        Eigen::Vector4f psf_ini;
        psf_ini << 6,0,6,3000;

        for(int i=0; i<idx_block.size(); i++){
            cout<<"psf idx "<<i<<" #####################################################################"<<endl;
            cout<<"number of particles in current PSF block "<<idx_block[i].size()<<endl;

            auto psf_elem = psf_ini;

            for(int k=0; k<3; k++){
                cout<<"####k "<<k<<" #####Calibrate OTF radius ...#############################"<<endl;
                auto psf_opt = this->calibrateOTFRadiusSingleZoneLMSolver(psf_elem, *cam, idx_block[i], img);
                psf_elem = psf_opt;

                cout<<"####k "<<k<<" #####Calibrate OTF intensity ...##########################"<<endl;
                auto thetai3 = this->calibrateOTFIntensitySingleZoneLMSolver(psf_elem, *cam, idx_block[i], img);
                psf_elem[3] = thetai3;
            
                cam->setPSFelement(psf_elem, i);
            }
        }
    }
    else{
        std::cerr<<"Too much or too few particles for OTF calibration!!!"<<endl;
        std::abort();
    }
}

Eigen::Vector4f Calibration::calibrateOTFRadiusSingleZoneLMSolver(const Eigen::Vector4f& psf_elem, const Camera& cam,
                                                                const std::vector<int>& Idx_InBlock,
                                                                const std::vector<cv::Point2d>& img){

    double initial_x[2]={double(psf_elem[0]), double(psf_elem[2])};
    double x[2]={double(psf_elem[0]), double(psf_elem[2])};

    ceres::Problem problem;

    cv::Mat Irec=cam.Irec().clone();
    cv::Mat_<double> Irec_float;
    Irec.convertTo(Irec_float, CV_64FC1);

    for(int i=0; i<Idx_InBlock.size(); i++){
        auto xc_i=img[ Idx_InBlock[i] ];    

        cv::Mat Imgi_real_i = utils::getLocalPatch(Irec_float, xc_i, cam.n_width(), cam.n_height());

        cv::Mat Imgi_real_float_i;
        Imgi_real_i.convertTo(Imgi_real_float_i, CV_32FC1);

        double E = 1.;

        problem.AddResidualBlock( new ceres::NumericDiffCostFunction<OTFRadiusCostFunctor, ceres::CENTRAL, 1, 2>(
                                        new OTFRadiusCostFunctor(   psf_elem[3], 
                                                                    cam.n_width(),
                                                                    cam.n_height(),
                                                                    Imgi_real_float_i,
                                                                    xc_i,
                                                                    E ) ),
                                 // new ceres::TrivialLoss(),
                                 new ceres::CauchyLoss(0.5),
                                 &x[0]);
    }

    ceres::Solver::Options options;
    // options.minimizer_progress_to_stdout = true;
    options.minimizer_type = ceres::LINE_SEARCH;
    // options.use_nonmonotonic_steps = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    // cout << summary.BriefReport() << "\n";
    // cout << summary.FullReport() << "\n";
    cout << "x0 : " << initial_x[0] << " -> " << x[0] << "\n";
    cout << "x1 : " << initial_x[1] << " -> " << x[1] << "\n";

    auto psf_opt = psf_elem;
    psf_opt[0] = x[0];
    psf_opt[2] = x[1];
    return psf_opt;
}                

double Calibration::calibrateOTFIntensitySingleZoneLMSolver(const Eigen::Vector4f& psf_elem, const Camera& cam,
                                                            const std::vector<int>& Idx_InBlock, 
                                                            const std::vector<cv::Point2d>& img){

    double initial_x=psf_elem[3];
    double x=initial_x;

    ceres::Problem problem;

    auto Irec=cam.Irec().clone();
    cv::Mat_<double> Irec_float;
    Irec.convertTo(Irec_float, CV_64FC1);

    for(int i=0; i<Idx_InBlock.size(); i++){
        auto xc_i=img[ Idx_InBlock[i] ];    

        cv::Mat Imgi_real_i = utils::getLocalPatch(Irec_float, xc_i, cam.n_width(), cam.n_height());

        cv::Mat Imgi_real_float_i;
        Imgi_real_i.convertTo(Imgi_real_float_i, CV_32FC1);

        double E = 1.;

        problem.AddResidualBlock( new ceres::NumericDiffCostFunction<OTFIntensityCostFunctor, ceres::CENTRAL, 1, 1>(
                                        new OTFIntensityCostFunctor(    psf_elem[0],
                                                                        psf_elem[2], 
                                                                        cam.n_width(),
                                                                        cam.n_height(),
                                                                        Imgi_real_float_i,
                                                                        xc_i,
                                                                        E ) ),
                                 // new ceres::TrivialLoss(),
                                 new ceres::CauchyLoss(0.5),
                                 &x);
    }

    ceres::Solver::Options options;
    // options.minimizer_progress_to_stdout = true;
    options.minimizer_type = ceres::LINE_SEARCH;
    // options.use_nonmonotonic_steps = true;
        
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    // cout << summary.BriefReport() << "\n";
    // cout << summary.FullReport() << "\n";
    cout << "x : " << initial_x << " -> " << x << "\n";
    return x;
}

void Calibration::testOTF(const std::vector<Camera_ptr>& cam, int num_samples, const std::vector<int>& cam_seq,
                            const std::vector<cv::Point3d>& obj,
                            const std::vector<std::vector<cv::Point2d>>& img_cams, const cv::Point3d& dXc_px){

    std::vector<int> sample_idx;
    for(int j=0; j<num_samples; j++){
        sample_idx.push_back( int(Random::UniformDistributionNumber(0, obj.size()-1)) );
    }

    double err=0;
    for(int j=0; j<sample_idx.size(); j++){
        auto Xc = obj[sample_idx[j]];
        cout<<"################################Xc "<<Xc<<endl;

        for(int i_cam=0; i_cam<cam.size(); i_cam++){

            int ic=0;
            if(i_cam==cam_seq[0])      ic=0;
            else if(i_cam==cam_seq[1]) ic=1;
            else if(i_cam==cam_seq[2]) ic=2;
            else if(i_cam==cam_seq[3]) ic=3;

            auto xc_real = img_cams[ic][sample_idx[j]];
            auto xc_proj = cam[i_cam]->mappingFunction(Xc);

            cout<<"####################### i_cam "<<i_cam<<endl
                <<"xc_real "<<xc_real<<endl
                <<"xc_proj "<<xc_proj<<endl;

            err += cv::norm(xc_real-xc_proj)/cv::norm(dXc_px);

            auto psf_elem = cam[i_cam]->getPSFelement(Xc);
            cout<<"psf_elem "<<psf_elem.transpose()<<endl;

            double E = 1.;
            cv::Mat  Imgi_smpl_i;
            utils::getImageSmpl(psf_elem, xc_proj, E, cam[i_cam]->n_width(),  cam[i_cam]->n_height(),
                                Imgi_smpl_i);

            auto Irec=cam[i_cam]->Irec().clone();
            cv::Mat_<double> Irec_float;
            Irec.convertTo(Irec_float, CV_64FC1);
            cv::Mat Imgi_real_i = utils::getLocalPatch(Irec_float, xc_real, cam[i_cam]->n_width(), cam[i_cam]->n_height());

            cout<<"Imgi_real_i"<<endl<<Imgi_real_i<<endl
                <<"Imgi_smpl_i"<<endl<<Imgi_smpl_i<<endl;

        }
    }
}

void Calibration::getPolynomialModelParameters(Camera* cam, double PixelPerMmFactor, const cv::Point3d& normalizationFactor){

    if(cam->z_order_==2)        cam->n_coef_=19;
    else if(cam->z_order_==3)   cam->n_coef_=20;
    else                        std::cerr<<"Calibration::getPolynomialModelParameters z_order not supported (only 2 & 3)"<<endl;

    cam->polynomial_type_ = Camera::Polynomial_Type::Single;

    cv::Mat_<double> design_mat,b_x,b_y,wx,wy;
    cv::Mat design_vec;

    cam->num_samples_tot_=0;
    std::vector<int> num_samples_views;
    if(nXc_rig_.x*nXc_rig_.y!=0){
        cam->num_samples_tot_=nXc_rig_.x*nXc_rig_.y*nViews_;
        num_samples_views.assign(nViews_, nXc_rig_.x*nXc_rig_.y);
    }
    else{
        for(int z=0; z<nViews_; z++){
            num_samples_views.push_back(Xcoord_obj_cams_.at(cam->i_cam_).at(z).size());
            cout<<"View "<<z<<" has "<<num_samples_views.back()<<" sample points"<<endl;
            cam->num_samples_tot_+=num_samples_views.back();
        }
    }
    cout<<"Total "<<cam->num_samples_tot_<<" points"<<endl;

    cam->PixelPerMmFactor_ = PixelPerMmFactor;
    cam->normalizationFactor_ = normalizationFactor;

    design_mat.create(cam->num_samples_tot_, cam->n_coef_);

    int num_samples=0;
    for(int z=0; z<nViews_; z++){
        for(int i=0; i<Xcoord_obj_cams_.at(cam->i_cam_).at(z).size(); i++){
            auto Xc=Xcoord_obj_cams_.at(cam->i_cam_).at(z).at(i);
            
            auto kX=cam->PixelPerMmFactor_*Xc.x;
            auto kY=cam->PixelPerMmFactor_*Xc.y;
            auto kZ=cam->PixelPerMmFactor_*Xc.z;

            auto X=kX/cam->normalizationFactor_.x;
            auto Y=kY/cam->normalizationFactor_.y;
            auto Z=kZ/cam->normalizationFactor_.z;

            if(cam->n_coef_==19)
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                                                                std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2));
            else
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z,
                                                                std::pow(Y,2)*Z, X*std::pow(Z,2), Xc.y*std::pow(Z,2), std::pow(Z,3));
            
            design_vec.copyTo( design_mat.row(num_samples+i) );
        }
        num_samples+=num_samples_views[z];
    }
        
    num_samples=0;
    b_x.create(cam->num_samples_tot_, 1);
    b_y.create(cam->num_samples_tot_, 1);
    for(int z=0; z<nViews_; z++){
        for(int i=0; i<xcoord_img_.at(cam->i_cam_).at(z).size(); i++){
            b_x(num_samples+i) = cam->PixelPerMmFactor_*Xcoord_obj_cams_[cam->i_cam_][z][i].x - xcoord_img_[cam->i_cam_][z][i].x;
            b_y(num_samples+i) = cam->PixelPerMmFactor_*Xcoord_obj_cams_[cam->i_cam_][z][i].y - xcoord_img_[cam->i_cam_][z][i].y;
        }
        num_samples+=num_samples_views[z];
    }

    wx.create(cam->n_coef_, 1);
    wy.create(cam->n_coef_, 1);

    cv::solve(design_mat, b_x, wx, cv::DECOMP_SVD);
    cv::solve(design_mat, b_y, wy, cv::DECOMP_SVD);

    cv::cv2eigen(wx, cam->a_);
    cv::cv2eigen(wy, cam->b_);
}

void Calibration::checkDomainBoundaryOnImageSpace(const Camera& cam, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom){

    auto X0c0 = cv::Point3d(Xc_bottom.x, Xc_bottom.y, Xc_bottom.z);
    auto X0c1 = cv::Point3d(Xc_top.x,    Xc_bottom.y, Xc_bottom.z);
    auto X0c2 = cv::Point3d(Xc_bottom.x, Xc_top.y,    Xc_bottom.z);
    auto X0c3 = cv::Point3d(Xc_top.x,    Xc_top.y,    Xc_bottom.z);

    auto XZc0 = cv::Point3d(Xc_bottom.x, Xc_bottom.y, Xc_top.z);
    auto XZc1 = cv::Point3d(Xc_top.x,    Xc_bottom.y, Xc_top.z);
    auto XZc2 = cv::Point3d(Xc_bottom.x, Xc_top.y,    Xc_top.z);
    auto XZc3 = cv::Point3d(Xc_top.x,    Xc_top.y,    Xc_top.z);

    auto x0c0 = cam.mappingFunction(X0c0);
    auto x0c1 = cam.mappingFunction(X0c1);
    auto x0c2 = cam.mappingFunction(X0c2);
    auto x0c3 = cam.mappingFunction(X0c3);

    auto xZc0 = cam.mappingFunction(XZc0);
    auto xZc1 = cam.mappingFunction(XZc1);
    auto xZc2 = cam.mappingFunction(XZc2);
    auto xZc3 = cam.mappingFunction(XZc3);

    cout<<str(boost::format("            3D O_world | 2D O_image"))<<endl;
    cout<<str(boost::format("---------------------------------------------------------------"))<<endl;
    cout<<str(boost::format("Bottomleft  %1% %2%") %  X0c0 % x0c0 )<<endl;
    cout<<str(boost::format("Bottomright %1% %2%") %  X0c1 % x0c1 )<<endl;
    cout<<str(boost::format("Topleft     %1% %2%") %  X0c2 % x0c2 )<<endl;
    cout<<str(boost::format("Topright    %1% %2%") %  X0c3 % x0c3 )<<endl;

    cout<<str(boost::format("            3D O_world | 2D O_image"))<<endl;
    cout<<str(boost::format("---------------------------------------------------------------"))<<endl;
    cout<<str(boost::format("Bottomleft  %1% %2%") %  XZc0 % xZc0 )<<endl;
    cout<<str(boost::format("Bottomright %1% %2%") %  XZc1 % xZc1 )<<endl;
    cout<<str(boost::format("Topleft     %1% %2%") %  XZc2 % xZc2 )<<endl;
    cout<<str(boost::format("Topright    %1% %2%") %  XZc3 % xZc3 )<<endl;
}

void Calibration::getPinholeModelParameters(Camera* cam, int opencv_calib_flag, int view_zero, double f, double k){
    
    double skew=0.;

   if(f!=0 && k!=0){
        cam->f_=f;
        cam->k_=k;
    }
    
    cv::Mat cameraMatrix;
    cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
    cameraMatrix.at<double>(0,0)=cam->k_*cam->f_;
    cameraMatrix.at<double>(1,1)=cam->k_*cam->f_;
    cameraMatrix.at<double>(0,1)=skew;
    cameraMatrix.at<double>(0,2)=cam->n_width_/2;
    cameraMatrix.at<double>(1,2)=cam->n_height_/2;

    cv::Mat distCoeffs = (cv::Mat_<double>(4, 1)<< 0, 0, 0, 0);

    cout<<"Prior"<<endl;
    cout<<"cameraMatrix"<<endl<<cameraMatrix<<endl
        <<"distCoeffs"<<endl<<distCoeffs<<endl;

    cam->num_samples_tot_=0;
    if(nXc_rig_.x*nXc_rig_.y!=0){
        cam->num_samples_tot_=nXc_rig_.x*nXc_rig_.y*nViews_;
    }
    else{
        for(int z=0; z<nViews_; z++){
            cam->num_samples_tot_+=Xcoord_obj_cams_.at(cam->i_cam_).at(z).size();
        }
    }
    cout<<"Total "<<cam->num_samples_tot_<<" points"<<endl;

    std::vector<cv::Mat> rVec, tVec;
    cv::Mat stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors;

    auto Xcoord_objf=changeXcoordDoubleToFloat(Xcoord_obj_cams_.at(cam->i_cam_));
    auto xcoord_imgf_icam=changexcoordDoubleToFloat(xcoord_img_.at(cam->i_cam_));

    opencv_calib_flag_ = opencv_calib_flag;

    double res=cv::calibrateCamera(Xcoord_objf, xcoord_imgf_icam, cv::Size(cam->n_width_, cam->n_height_),
                        cameraMatrix, distCoeffs, rVec, tVec,
                        stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors,
                        opencv_calib_flag_|cv::CALIB_USE_INTRINSIC_GUESS,
                        cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 120, 1e-12) );

    cam->cameraMatrix_=cameraMatrix;
    cam->distCoeffs_=distCoeffs;

    cam->ZViews_=ZViews_;

    cam->rvecs_=rVec;
    cam->tvecs_=tVec;

    cout<<"Posterior"<<endl;
    cout<<"cam->cameraMatrix_"<<endl<<cam->cameraMatrix_<<endl
        <<"cam->distCoeffs_"<<endl<<cam->distCoeffs_<<endl;

    for(int v=0; v<nViews_; v++){
        cout<<"View "<<v<<endl;
        cout<<"cam->rvec "<<endl<<cam->rvecs_[v]<<endl;
        cv::Mat rmat;
        cv::Rodrigues(cam->rvecs_[v], rmat);
        cout<<"rmat "<<endl<<rmat<<endl;
        cout<<"cam->tvec "<<endl<<cam->tvecs_[v]<<endl; 
    }

    cout<<"ZViews_[view_zero] "<<ZViews_[view_zero]<<endl;

    cam->R_=rVec[view_zero];
    cam->T_=tVec[view_zero];

    cout<<"Calibration mean error "<<res<<endl;
    cout<<"perViewErrors"<<endl<<perViewErrors<<endl;

    this->testSamplePoints(cam, view_zero, true, false);
    this->testSamplePoints(cam, view_zero, false, false);
}

void Calibration::testSamplePoints(Camera* cam, int view_zero, bool zero_Z_flag, bool verbal){
    
    cout<<"####################################test sample points"<<endl;
    double err=0.;
    for(int z=0; z<nViews_; z++){
        double err_view=0;
        if(verbal) cout<<"View "<<z<<endl;
        for(int i=0; i<Xcoord_obj_cams_[cam->i_cam_][z].size(); i++){
            auto Xc = Xcoord_obj_cams_[cam->i_cam_][z][i];

            std::vector<cv::Point3d> p3_in_cv;
            std::vector<cv::Point2d> p2_out;

            if(zero_Z_flag) 
                p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) );
            else{
                if(z==view_zero) p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + ZViews_[z]*cam->deltaX_ );
                // else              p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + (ZViews_[z]-ZViews_[z-1])*cam->deltaX_ );
                else{
                    if(z==0)     p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + (ZViews_[z]-ZViews_[1])*cam->deltaX_ );
                    else         p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + (ZViews_[z]-ZViews_[z-1])*cam->deltaX_ );
                }
            }

            if(zero_Z_flag) 
                cv::projectPoints(p3_in_cv, cam->rvecs_[z], cam->tvecs_[z], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
            else{
                if(z==view_zero) cv::projectPoints(p3_in_cv, cam->R_, cam->T_, cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                // else              cv::projectPoints(p3_in_cv, cam->rvecs_[z-1], cam->tvecs_[z-1], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                else{
                    if(z==0)     cv::projectPoints(p3_in_cv, cam->rvecs_[1], cam->tvecs_[1], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                    else         cv::projectPoints(p3_in_cv, cam->rvecs_[z-1], cam->tvecs_[z-1], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                }
            }

            auto xc = xcoord_img_[cam->i_cam_][z][i];
            auto xc_pinh = (true ? p2_out[0] : cam->mappingFunction(Xc));

            err+=cv::norm(xc_pinh-xc);
            err_view+=cv::norm(xc_pinh-xc);

            if(verbal)  cout<<"Xc "<<p3_in_cv[0]<<"xc "<<xc<<" xc_pinh "<<xc_pinh<<endl;
        }
    
        cout<<"perViewError "<<ZViews_[z]<<" : "<<err_view/static_cast<double>(Xcoord_obj_cams_[cam->i_cam_][z].size())<<endl;
    }

    cout<<"Calibration mean error "<<err/static_cast<double>(cam->num_samples_tot_)<<endl;
}

void Calibration::getStereoCalibrationParameters(Camera* cam1, Camera* cam2){

    if(cam1->i_cam_==cam2->i_cam_){
        cam1->R_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
        cam1->T_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
        cam1->E_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
        cam1->F_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
    }
    else{
        cv::Mat  Camera12Rotation, Camera12Translation, EssentialMatrix, FundementalMatrix;

       // if(ource_==Calibration::FromLPTChallenge){
            //Note: stereocalibrate only takes float32 types
        auto Xcoord_objf=changeXcoordDoubleToFloat(Xcoord_obj_cams_.at(cam1->i_cam_));
        auto xcoord_imgf_icam1=changexcoordDoubleToFloat(xcoord_img_.at(cam1->i_cam_));
        auto xcoord_imgf_icam2=changexcoordDoubleToFloat(xcoord_img_.at(cam2->i_cam_));

        double rms = cv::stereoCalibrate(Xcoord_objf, xcoord_imgf_icam1, xcoord_imgf_icam2, 
                                cam1->cameraMatrix_, cam1->distCoeffs_, cam2->cameraMatrix_, cam2->distCoeffs_, 
                                cv::Size(cam1->n_width_, cam1->n_height_), 
                                Camera12Rotation, Camera12Translation, EssentialMatrix, FundementalMatrix,
                                opencv_calib_flag_|cv::CALIB_FIX_INTRINSIC|cv::CALIB_FIX_FOCAL_LENGTH,
                                cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 60, 1e-6));

        cout<<"StereoRotation"<<endl
            <<Camera12Rotation<<endl
            <<"StereoTranslation"<<endl
            <<Camera12Translation<<endl
            <<"EssentialMatrix"<<endl
            <<EssentialMatrix<<endl
            <<"FundementalMatrix"<<endl
            <<FundementalMatrix<<endl;

            //Manuel computing
            // cv::Mat EssentialMatrix2, FundementalMatrix2, rmat1, rmat2;
            // cv::Rodrigues(cam1->R_, rmat1);
            // cv::Rodrigues(cam2->R_, rmat2);

            // this->computeEssentialMatrix(rmat1, rmat2, cam1->T_, cam2->T_, EssentialMatrix2);

            // FundementalMatrix2   = cam2->cameraMatrix_.inv().t()*EssentialMatrix2*cam1->cameraMatrix_.inv();
            // FundementalMatrix2   = FundementalMatrix2*1./FundementalMatrix2.at<double>(2,2);

            // cout<<"EssentialMatrix2"<<endl
            //  <<EssentialMatrix2<<endl
            //  <<"FundementalMatrix2"<<endl
            //  <<FundementalMatrix2<<endl;
            //Manuel computing 
        // }
        // else{
        //     cv::Mat rmat1, rmat2;
        //     cv::Rodrigues(cam1->R_, rmat1);
        //     cv::Rodrigues(cam2->R_, rmat2);

        //     this->computeEssentialMatrix(rmat1, rmat2, cam1->T_, cam2->T_, EssentialMatrix);

        //     FundementalMatrix = cam2->cameraMatrix_.inv().t()*EssentialMatrix*cam1->cameraMatrix_.inv();
        //     FundementalMatrix = FundementalMatrix*1./FundementalMatrix.at<double>(2,2); 
        // }

        cam1->R_betw_cams_.push_back( Camera12Rotation );
        cam1->T_betw_cams_.push_back( Camera12Translation );
        cam1->E_betw_cams_.push_back( EssentialMatrix );
        cam1->F_betw_cams_.push_back( FundementalMatrix );
    }
}

void Calibration::computeEssentialMatrix(const cv::Mat& rmat1, const cv::Mat& rmat2, const cv::Mat& t1, const cv::Mat& t2, cv::Mat& EssentialMatrix){

    cv::Mat Camera12Rotation    = rmat2*rmat1.inv();
    cv::Mat Camera12Translation = t2 - Camera12Rotation*t1;

    cv::Mat TT=(cv::Mat_<double>(3,3)<< 0, -Camera12Translation.at<double>(2), Camera12Translation.at<double>(1),
                                        Camera12Translation.at<double>(2), 0, -Camera12Translation.at<double>(0),
                                        -Camera12Translation.at<double>(1), Camera12Translation.at<double>(0), 0); 

    EssentialMatrix = TT*Camera12Rotation;
}

void Calibration::setPSFParams(Camera* cam, double theta){

    Eigen::Vector4f theta_ijk;
    theta_ijk << theta, 0, theta, 1;
    theta_ijk[3] = Camera::psf_type_==Camera::psf_Type::ErrorFunction ? 1.05 : 1.;

    int nX=5, nY=5, nZ=2;
    cam->nXcPSF_=cv::Point3i(nX, nY, nZ);
    cam->theta_.assign(nX*nY*nZ, theta_ijk);
}

void Calibration::computeMappingFunctionError(const Camera& cam, bool pinhole_flag){
    
    double err=0.;
    for(int z=0; z<nViews_; z++){
        for(int i=0; i<Xcoord_obj_cams_.at(cam.i_cam_).at(z).size(); i++){
            auto Xc = Xcoord_obj_cams_[cam.i_cam_][z][i];
            auto xc = cam.mappingFunction(Xc, false, pinhole_flag);
            err+=cv::norm(xc - xcoord_img_[cam.i_cam_][z][i]);
        }
    }

    cout<<"Mapping function mean error "<<err/static_cast<double>(cam.num_samples_tot_)<<endl;
}

void Calibration::saveMarkPositionTable(const std::string& dstdir){

    std::string calibdir(dstdir+"/calib");
    if(!boost::filesystem::exists(calibdir)){
        boost::filesystem::create_directories(calibdir);
    }

    std::vector<double> x1_v, y1_v, x2_v, y2_v, x3_v, y3_v, x4_v, y4_v, X_v, Y_v, Z_v;

    auto calibFileName=str(boost::format("%1%/calib_points.npz") % calibdir );

    for(int z=0; z<nViews_; z++){
        for(int i=0; i<nXc_rig_.x; i++){
            for(int j=0; j<nXc_rig_.y; j++){
                auto Xc = Xcoord_obj_cams_[0][z][i*nXc_rig_.y+j];
                X_v.push_back(Xc.x);
                Y_v.push_back(Xc.y);
                Z_v.push_back(Xc.z);
                auto xc = xcoord_img_[0][z][i*nXc_rig_.y+j];
                x1_v.push_back(xc.x);
                y1_v.push_back(xc.y);
                xc = xcoord_img_[1][z][i*nXc_rig_.y+j];
                x2_v.push_back(xc.x);
                y2_v.push_back(xc.y);
                xc = xcoord_img_[2][z][i*nXc_rig_.y+j];
                x3_v.push_back(xc.x);
                y3_v.push_back(xc.y);
                xc = xcoord_img_[3][z][i*nXc_rig_.y+j];
                x4_v.push_back(xc.x);
                y4_v.push_back(xc.y);
            }
        }
    }

    auto NX = static_cast<long unsigned int>(X_v.size());
    cnpy::npz_save(calibFileName, "X", &X_v[0], {NX,1}, "w");
    cnpy::npz_save(calibFileName, "Y", &Y_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "Z", &Z_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x1", &x1_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y1", &y1_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x2", &x2_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y2", &y2_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x3", &x3_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y3", &y3_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x4", &x4_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y4", &y4_v[0], {NX,1}, "a");
}

}//namespace PTL