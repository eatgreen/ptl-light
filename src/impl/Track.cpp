#include "../Track.hpp"

namespace PTL{

void track::saveFrameDataTXT(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt, const std::string& res_path, double dtObs, bool append_flag, int track_min_length, const std::string& modified_str){

    std::string   resfilename( str(boost::format("%1%/result%2%.dat") % res_path % modified_str ) );

    std::ofstream resfile;

    if(append_flag)
        resfile.open(resfilename, std::ios::out | std::ios::app);
    else
        resfile.open(resfilename, std::ios::out);

    if(!append_flag){
        auto title="TITLE = \"KLPT\"";
        auto variables="VARIABLES = \"x[mm]\" \"y[mm]\" \"z[mm]\" \"I\" \"u[m/s]\" \"v[m/s]\" \"w[m/s]\" \"|V|[m/s]\" \"trackID\" \"ax[m/s²]\" \"ay[m/s²]\" \"az[m/s²]\" \"|a|[m/s²]\"";
        
        resfile<<title<<endl;
        resfile<<variables<<endl;
    }

    auto it=it_seq_cnt-1;
    auto time_level=it*dtObs;
    auto zone_t=str(boost::format("ZONE T=\"Snapshot %1$04d\"") % it);
    auto strandid=str(boost::format("STRANDID=1, SOLUTIONTIME=%1$5.10f") % time_level);
    auto IJK="I=980, J=1, K=1, ZONETYPE = Ordered";
    auto datapacking="DATAPACKING = POINT";

    resfile<<zone_t<<endl;
    resfile<<strandid<<endl;
    resfile<<IJK<<endl;
    resfile<<datapacking<<endl;

    for(const auto& pt: particles_tgt){

        pt->checkIfParticleAppearsOnSnapshot(it_seq_cnt);

        if(pt->is_ontrack() && pt->length()>=track_min_length){

            auto it_seq=it_seq_cnt-pt->it_seq_deb();

            std::string line=str(boost::format("%1$5.10f %2$5.10f %3$5.10f %4$5.10f %5$5.10f %6$5.10f %7$5.10f %8$5.10f %9$9d %10$5.10f %11$5.10f %12$5.10f %13$5.10f") 
                                              % pt->Xcoord().at(it_seq).x % pt->Xcoord().at(it_seq).y % pt->Xcoord().at(it_seq).z % pt->Ev().at(it_seq) 
                                              % 0 % 0 % 0 % 0 % pt->part_index() % 0 % 0 % 0 % 0 );
            resfile<<line<<endl;
        }
    }
    resfile.close();
}

void track::saveFrameDataVTK(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt, const std::string& res_path, int track_min_length, const std::string& modified_str){

    auto writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();

    std::string   resfilename( str(boost::format("%1%/result%2%%3$03d.vtu") % res_path % modified_str % it_seq_cnt) );

    auto dataSet = vtkSmartPointer<vtkUnstructuredGrid>::New();

    auto pts = vtkSmartPointer<vtkPoints>::New();

    utils::addParticlesToVTKDataSet(it_seq_cnt, particles_tgt, pts, dataSet, track_min_length);

    dataSet->SetPoints(pts);

    writer->SetFileName(resfilename.c_str());

    writer->SetInputData(dataSet);

    writer->Write();
}

std::vector<Particle_ptr>
track::findTheBrightestParticles(int num, int it_seq, const std::vector<Particle_ptr>& particles_tgt,
                                const cv::Point3d& Xc_doi_top, const cv::Point3d& Xc_doi_bottom){

    utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_tgt);

    std::vector<Particle_ptr> particles_vsc;

    std::vector<double> Ev;
    for(const auto& pt: particles_tgt){
        if(pt->is_ontrack() && !checkParticleIsOutOfRange( pt->getCurrentXcoord(it_seq), Xc_doi_top, Xc_doi_bottom )){
            Ev.push_back( pt->getCurrentE(it_seq) );
        }
        else
            Ev.push_back(-1);
    }

    int num_min = num==0 ?  static_cast<int>(Ev.size()) : 
                            std::min(num, static_cast<int>(Ev.size()));

    std::vector<int> tgt_vec;
    for(int i=0; i<num_min; i++){
        auto it = std::max_element(Ev.begin(), Ev.end());
        if(*it!=-1){
            int dist = std::distance(std::begin(Ev), it);
            tgt_vec.push_back(dist);
            Ev[dist]=-1;
        }
    }

    for(auto& t: tgt_vec){
        particles_vsc.push_back( std::make_unique<Particle>( *particles_tgt[t] ) );
    }

    cout<<"particles_vsc.size() "<<particles_vsc.size()<<endl;

    return particles_vsc;
}

std::vector<Particle_ptr>
track::getTracksWithLength2AtCurrentSnapshot(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt){

    std::vector<Particle_ptr> particles_new;

    utils::checkIfParticlesAppearOnSnapshot(it_seq_cnt, particles_tgt);

    for(const auto& ps: particles_tgt){
        if(ps->is_ontrack() && it_seq_cnt-ps->it_seq_deb()==2){
            auto pt_new = std::make_unique<Particle>( ps->ID(), ps->it_seq_deb(), it_seq_cnt-1, 
                                                        std::vector<cv::Point3d>(ps->Xcoord().begin(), ps->Xcoord().begin()+2),
                                                        std::vector<double>(ps->Ev().begin(), ps->Ev().begin()+2) );

            assert(pt_new->length()==2);
            particles_new.push_back( std::move(pt_new) );
        }
    }

    cout<<"particles_new.size() "<<particles_new.size()<<endl;
    return particles_new;
}

std::vector<Particle_ptr>
track::mergeTwoPaticleFields(std::vector<Particle_ptr>& particles_tgt, const std::vector<Particle_ptr>& particles_app, int it_seq){

    std::vector<Particle_ptr> particles_merge = std::move( particles_tgt );

    for(const auto& pm: particles_merge){
        pm->checkIfParticleAppearsOnSnapshot(it_seq-1);
        if(pm->is_ontrack()){
            for(const auto& pa: particles_app){
                pa->checkIfParticleAppearsOnSnapshot(it_seq);
                if(pa->is_ontrack() && pm->ID()==pa->ID()){
                    pm->push_backXcoordElement( pa->Xcoord()[it_seq-pa->it_seq_deb()], it_seq );
                    pm->push_backEvElement( pa->Ev()[it_seq-pa->it_seq_deb()], it_seq );
                    pm->push_backVElement( pa->V()[it_seq-pa->it_seq_deb()], it_seq );
                    pm->setit_seq_fin( it_seq );
                }
            }
        }
    }

    for(const auto& pm: particles_merge){
        pm->checkIfParticleAppearsOnSnapshot(it_seq-1);
        if(pm->is_ontrack() && pm->it_seq_fin()==it_seq-1){
            // if(pm->length()<=2){
            //     pm->markAsGhost();
            //     pm->deleteData(true);
            // }
            // else{
                pm->markAsLost();
                pm->deleteData(false);
            // }
        }
    }
    utils::showInfoOnDeletedParticles();

    for(const auto& pa: particles_app){
        pa->checkIfParticleAppearsOnSnapshot(it_seq);
        if(pa->is_ontrack() && pa->is_new()){
            particles_merge.push_back( std::make_unique<Particle>(*pa) );
        }
    }

    return particles_merge;
}

std::vector<Particle_ptr>
track::reorderParticleField(const std::vector<Particle_ptr>& particles_tgt){

    std::vector<Particle_ptr> particles_reorder;

    int track_min_length = 4;

    int num_track = 0;
    for(const auto& pt: particles_tgt){
        if(pt->length()>=track_min_length){
            num_track++;
            particles_reorder.push_back( std::make_unique<Particle>( *pt ) );
            particles_reorder.back()->setID(num_track);
        }
    }

    return particles_reorder;
}

std::vector<Particle_ptr>
track::rmVeryShortTracksFromParticleField(const std::vector<Particle_ptr>& particles_tgt,
                                                const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom){
    std::vector<Particle_ptr> particles_rm;

    auto len = Xc_top - Xc_bottom;
    auto minlen = std::min({len.x, len.y, len.z});

    double minimum_displacement = 0.1*minlen;

    for(const auto& pt: particles_tgt){
        if( cv::norm(pt->Xcoord().back()-pt->Xcoord().front())>minimum_displacement ){
            particles_rm.push_back( std::make_unique<Particle>( *pt ) );
        }
    }

    return particles_rm;
}

void track::readIPRDataIntoFrames(const std::vector<int>& it_bkg, const std::string& iprdir, bool is_STB,
                                std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_ipr_in_frames){

    cout<<"IPR dir: "<<iprdir<<endl;

    std::string opt_method_str = (is_STB ? "STB" : "ENS");

    for(int it_seq=it_bkg.at(0); it_seq<it_bkg.at(0)+it_bkg.size(); it_seq++){
        
        auto iprPhaseFile=str(boost::format("%1%/IPR_%3%_it%2$03d.npz") % iprdir % it_seq % opt_method_str );
        auto load_npz = cnpy::npz_load(iprPhaseFile);

        auto id_v = load_npz["ID"].as_vec<int>();
        auto X_v = load_npz["X"].as_vec<double>();
        auto Y_v = load_npz["Y"].as_vec<double>();
        auto Z_v = load_npz["Z"].as_vec<double>();
        auto E_v = load_npz["E"].as_vec<double>();

        particles_ipr_in_frames.push_back(pcl::make_shared<pcl::PointCloud<pcl::PointXYZI>>());
        for(int p=0; p<id_v.size(); p++){
            particles_ipr_in_frames.back()->push_back( pcl::PointXYZI({{ static_cast<float>(X_v[p]), static_cast<float>(Y_v[p]), static_cast<float>(Z_v[p]) },
                                                                       { static_cast<float>(E_v[p]) }}) );
        }

        cout<<"IPR particles: particles_ipr.size() at "<<it_seq<<" is "<<particles_ipr_in_frames.back()->size()<<endl;
    }
}

void track::printTrackInformation(int it_tot, const std::vector<Particle_ptr>& particles_tgt, double dtObs, const cv::Point3d& dXc_px){

    std::vector<int> track_length_distribution(it_tot, 0);

    int  num_digit=7;
    auto str_fmt = "%1$"+std::to_string(num_digit)+"d";

    cout<<"#############################################"<<endl;
    cout<<std::string(num_digit, ' ');

    for(int i=1; i<=it_tot; i++){
        cout<<str(boost::format(str_fmt) % i );
    }
    cout<<" Total"<<endl;

    for(int it_seq_deb=1; it_seq_deb<it_tot+1; it_seq_deb++){
        std::vector<int> length(it_tot-it_seq_deb+1, 0);
        for(const auto& ps: particles_tgt){
            if(ps->it_seq_deb()==it_seq_deb && ps->it_seq_fin()>=ps->it_seq_deb() && ps->Xcoord().size()!=0){
                length[ps->it_seq_fin()-ps->it_seq_deb()]++;
            }
        }

        cout<<str(boost::format(str_fmt) % it_seq_deb );
        for(int i=0; i<it_seq_deb-1; i++){
            cout<<std::string(num_digit, ' ');
        }

        for(int i=0; i<length.size(); i++){
            cout<<str(boost::format(str_fmt) % length[i] );
            track_length_distribution[i] += length[i];
        }
        cout<<str(boost::format(str_fmt) % accumulate(length.begin(), length.end(), 0) )<<endl;
    }

    cout<<"#############################################"<<endl;
    for(int i=0; i<track_length_distribution.size(); i++)
        cout<<"Track_length "<<i+1<<" has tracks: "<<track_length_distribution[i]<<endl;

    if(it_tot>2){
        cout<<"#############################################"<<endl;
        int min_length = 4;
        int num_tot = std::accumulate(track_length_distribution.begin()+min_length, track_length_distribution.end(), 0);
        cout<<"total number of tracks taken into consideration "<<num_tot<<endl;
        double avg_length = 0;
        for(int i=min_length; i<track_length_distribution.size(); i++){
            avg_length += ((double)(i)+1.)*static_cast<double>(track_length_distribution[i]);
        }
        cout<<"avg length "<<std::round(avg_length/static_cast<double>(num_tot))<<endl;

        std::vector<int> length(it_tot, 0);
        for(int it_seq=1; it_seq<it_tot+1; it_seq++){
            for(const auto& ps: particles_tgt){
                if(it_seq >= ps->it_seq_deb() && it_seq <= ps->it_seq_fin() && !ps->is_ghost() && ps->Xcoord().size()>=3){
                    length[it_seq-1]++;
                }
            }
        }

        cout<<"#############################################"<<endl;
        for(int i=0; i<length.size(); i++)
            cout<<"Snapshot "<<i+1<<" has tracks: "<<length[i]<<endl;

        cout<<"#############################################"<<endl;
        cout<<"min "<<*std::min_element(length.begin(), length.end())<<endl;
        cout<<"max "<<*std::max_element(length.begin(), length.end())<<endl;
        cout<<"avg "<<std::round(static_cast<double>(std::accumulate(length.begin(), length.end(), 0))/static_cast<double>(length.size()))<<endl;
    }
}

std::vector<Particle_ptr>
track::convertFrameToParticles(int it_seq,
                                   const std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_in_frames_cloud){

	std::vector<Particle_ptr> particles_recon;
    int p_part_index=0;
    int it = particles_in_frames_cloud.size()==1 ? 0 : it_seq-1;
    for(const auto& pc: particles_in_frames_cloud[it]->points){
        particles_recon.push_back( std::make_unique<Particle>( p_part_index, it_seq, it_seq, 
                                                                std::vector<cv::Point3d>{cv::Point3d(pc.x, pc.y, pc.z)}, std::vector<double>{pc.intensity} ) );
        p_part_index++;
    }
    return particles_recon;
}

std::vector<Particle_ptr> 
track::makeLinkUsingPredictionField(const std::vector<int>& it_bkg, const Transport& transport,
                                             const cv::Point3d& dXc_px,
                                             const std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_in_frames_cloud){

    double   threshold=cv::norm(dXc_px);

    cout<<"#####INITIALIZE NEW PARTICLES AT T=1 #########################"<<endl;

    auto particles_recon = convertFrameToParticles(it_bkg.front(), particles_in_frames_cloud);
    int  p_part_index = particles_recon.back()->ID()+1;

    for(int it_seq=it_bkg.front()+1; it_seq<it_bkg.front()+it_bkg.size(); it_seq++){

        cout<<"#####LINK BETWEEN FROM T="<<it_seq-1<<" -> T="<<it_seq<<" #########################"<<endl;

        transport.predictParticlePositionField(particles_recon, it_seq, false);

        auto particles_in_cnt_frame_cloud = particles_in_frames_cloud[it_seq-1];
        
        auto particles_in_cnt_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(1.);
        particles_in_cnt_frame_octree.setInputCloud(particles_in_cnt_frame_cloud);
        particles_in_cnt_frame_octree.addPointsFromInputCloud();

        pcl::ExtractIndices<pcl::PointXYZI>  extract;
        pcl::PointIndices::Ptr               appended_particles_indices = pcl::make_shared<pcl::PointIndices>();

        for(auto& pr : particles_recon){
            if(pr->is_ontrack() && !pr->is_deleted()){

                std::vector<int>   result_index;
                std::vector<float> sqr_distance;  
                                                                                         
                if(particles_in_cnt_frame_octree.radiusSearch(cv2pcl(pr->getCurrentXcoord(it_seq),0), threshold, result_index, sqr_distance)){
                    std::vector<std::pair<int, float>> order(result_index.size());

                    for(int i = 0; i < result_index.size(); i++)
                        order[i] = std::make_pair(result_index[i], sqr_distance[i]);

                    std::sort(order.begin(), order.end(), [](auto& lop, auto & rop) { return lop.second < rop.second; });

                    int i=0;
                    while(i<order.size())
                    {
                        auto it = std::find(std::begin(appended_particles_indices->indices),
                                            std::end(appended_particles_indices->indices), 
                                            order[i].first);

                        if(it == std::end(appended_particles_indices->indices)){
                            auto pc_pcl = particles_in_cnt_frame_cloud->points[order[i].first];

                            pr->setXcoordElement(cv::Point3d(pc_pcl.x, pc_pcl.y, pc_pcl.z), it_seq);
                            pr->push_backEvElement(pc_pcl.intensity, it_seq);
                            pr->setit_seq_fin(it_seq);

                            appended_particles_indices->indices.push_back(order[i].first);

                            break;
                        }
                        else{
                            i++;
                        }
                    }

                    if(i==order.size()){
                        pr->deleteData(false);
                        pr->pop_back_Xcoord();
                        pr->setit_seq_fin(it_seq-1);
                    }
                }
                else{
                    pr->deleteData(false);
                    pr->pop_back_Xcoord();
                    pr->setit_seq_fin(it_seq-1);
                }
            }
        }
        utils::showInfoOnDeletedParticles();
        
        cout<<"#####REMOVE APPENDED PARTICLES AT T="<<it_seq-1<<" ##################"<<endl;

        extract.setInputCloud(particles_in_cnt_frame_cloud);
        extract.setIndices(appended_particles_indices);
        extract.setNegative(true);
        extract.filter(*particles_in_cnt_frame_cloud);

        cout<<"#####ADD NEW PARTICLES AT T="<<it_seq-1<<" #########################"<<endl;

        for(const auto& pc: particles_in_cnt_frame_cloud->points){
            particles_recon.push_back( std::make_unique<Particle>( p_part_index, it_seq, it_seq, 
                                                                    std::vector<cv::Point3d>{cv::Point3d(pc.x, pc.y, pc.z)}, std::vector<double>{pc.intensity} ) );
            p_part_index++;
        }

        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_recon);
    }

    return particles_recon;
}

void track::saveInitTrackedParticles(const std::vector<int>& it_bkg, const std::string& imgdir, const std::vector<Particle_ptr>& particles_recon){

    std::string bkgdir(imgdir+"/bkg_recon");

    if(!boost::filesystem::exists(bkgdir)){
        boost::filesystem::create_directories(bkgdir);
    }
    
    for(int it=it_bkg[0]; it<it_bkg[0]+it_bkg.size(); it++){
        auto bkgPhaseFile=str(boost::format("%1%/bkg_it%2$03d.txt") % bkgdir % it );
        utils::saveFrameDataNPZ(it, bkgPhaseFile, particles_recon);
    }
}

}//namespace PTL