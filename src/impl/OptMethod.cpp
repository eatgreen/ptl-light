#include "../OptMethod.hpp"

namespace PTL{

OptMethod::OptMethod(){}

OptMethod::~OptMethod(){}

OptMethod::OptMethod(Method_Type method_type, ControlVector_Type control_vector_type, bool intensity_control_flag,
                     int n_part, int n_part_per_snapshot, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, 
                     double pixel_lost, double dtObs, 
                     const cv::Point3d& dXc_px, const std::string& res_path, int eval_window,
                     bool warm_start_flag, int warm_start_max_iter, double warm_start_pixel, double warm_start_relaxfactor)
                    : method_type_(method_type), control_vector_type_(control_vector_type), intensity_control_flag_(intensity_control_flag),
                      n_particles_(n_part), n_particles_per_snapshot_(n_part_per_snapshot), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), 
                      pixel_lost_(pixel_lost), dtObs_(dtObs), 
                      dXc_px_(dXc_px), res_path_(res_path), eval_window_fine_search_(eval_window),
                      warm_start_flag_(warm_start_flag), warm_start_max_iter_(warm_start_max_iter), warm_start_pixel_(warm_start_pixel), warm_start_relaxfactor_(warm_start_relaxfactor) {
                        avg_part_dist_ = std::cbrt( (Xc_top_.x-Xc_bottom_.x)*(Xc_top_.y-Xc_bottom_.y)*(Xc_top_.z-Xc_bottom_.z)/n_part_per_snapshot );
                        characteristic_length_=std::max(std::max(Xc_top_.x-Xc_bottom_.x, Xc_top_.y-Xc_bottom_.y), Xc_top_.z-Xc_bottom_.z);
                        cout<<"avg_part_dist_         "<<avg_part_dist_<<endl;
                        cout<<"characteristic_length_ "<<characteristic_length_<<endl;
                    }

OptMethod::OptMethod(Method_Type method_type, ControlVector_Type control_vector_type, bool intensity_control_flag,
                     int n_part, int n_part_per_snapshot, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                     const cv::Point3d& dXc_px, const std::string& res_path, int eval_window)
                    : method_type_(method_type), control_vector_type_(control_vector_type), intensity_control_flag_(intensity_control_flag),
                      n_particles_(n_part), n_particles_per_snapshot_(n_part_per_snapshot), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom),
                      dXc_px_(dXc_px), res_path_(res_path), eval_window_fine_search_(eval_window) {
                        warm_start_flag_=false;
                        avg_part_dist_ = std::cbrt( (Xc_top_.x-Xc_bottom_.x)*(Xc_top_.y-Xc_bottom_.y)*(Xc_top_.z-Xc_bottom_.z)/n_part_per_snapshot );
                        characteristic_length_=std::max(std::max(Xc_top_.x-Xc_bottom_.x, Xc_top_.y-Xc_bottom_.y), Xc_top_.z-Xc_bottom_.z);
                    }

double OptMethod::computeAverageIntensity(const std::vector<Particle_ptr>& particles_ana){

    int    num_p=0;
    double E_avg=0.;
    for(auto& pt:particles_ana){
        if(pt->is_ontrack() && this->checkIfParticleIsToBeOptimized(pt.get())){
            E_avg+=pt->Ev().back();
            num_p++;
        }
    }
    ASSERT(num_p!=0);

    return E_avg/static_cast<double>(num_p);
}

void OptMethod::setintensity_threshold(const std::vector<Particle_ptr>& particles_ana){

    intensity_average_=this->computeAverageIntensity(particles_ana);
    intensity_min_threshold_=intensity_min_threshold_ratio_*intensity_average_;
    intensity_deletion_threshold_=intensity_deletion_threshold_ratio_*intensity_average_;
    cout<<"set intensity_average_            "<<intensity_average_<<endl
        <<"set intensity_min_threshold_      "<<intensity_min_threshold_<<endl
        <<"set intensity_deletion_threshold_ "<<intensity_deletion_threshold_<<endl;
}

void OptMethod::predictIntensity(int it_seq_cnt){
    
    for(auto& pf : this->particles_field())
        if(pf->is_ontrack() && pf->is_to_be_tracked())
            pf->assignInitialIntensity(it_seq_cnt);
}

void OptMethod::predictParticlesUsingNeighboringInformation(int it_seq_cnt,
                                                            std::vector<Particle_ptr>& particles_tgt){

    bool verbal_flag=false;

    if(!tracked_particles_coord_velo_in_one_frame_cloud_->empty()) tracked_particles_coord_velo_in_one_frame_cloud_->clear();
 
    std::vector<cv::Point3d> tracked_particles_velo_in_one_frame;

    cout<<"#####Compute velocity..."<<endl<<endl;
    for(const auto& pt: particles_tgt){
        if(pt->is_ontrack() && pt->is_tracked() && !pt->is_side()){
            pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference);

            auto Xc = pt->getPreviousXcoord(it_seq_cnt);
            auto V  = pt->getPreviousOfPreviousV(it_seq_cnt);

            tracked_particles_coord_velo_in_one_frame_cloud_->push_back( cv2pcl(Xc, V) );

            tracked_particles_velo_in_one_frame.push_back(V);
        }
    }

    // if(it_seq_cnt==it_fct_deb){
    velo_max_=cv::Point3d(std::max_element(tracked_particles_velo_in_one_frame.begin(), tracked_particles_velo_in_one_frame.end(), utils::compare_x)->x,
                          std::max_element(tracked_particles_velo_in_one_frame.begin(), tracked_particles_velo_in_one_frame.end(), utils::compare_y)->y,
                          std::max_element(tracked_particles_velo_in_one_frame.begin(), tracked_particles_velo_in_one_frame.end(), utils::compare_z)->z);
    velo_min_=cv::Point3d(std::min_element(tracked_particles_velo_in_one_frame.begin(), tracked_particles_velo_in_one_frame.end(), utils::compare_x)->x,
                          std::min_element(tracked_particles_velo_in_one_frame.begin(), tracked_particles_velo_in_one_frame.end(), utils::compare_y)->y,
                          std::min_element(tracked_particles_velo_in_one_frame.begin(), tracked_particles_velo_in_one_frame.end(), utils::compare_z)->z);
    //     velo_mag_norm_=0.;
    //     for(const auto& velo: tracked_particles_velo_in_one_frame){
    //         velo_mag_norm_+=cv::norm(velo)/static_cast<double>(tracked_particles_velo_in_one_frame.size());
    //     }
    //     velo_mag_abs_variation_=cv::Point3d(100, 50, 50);
    // }
    cout<<"Velo max "<<velo_max_<<endl
        <<"Velo min "<<velo_min_<<endl;
        // <<"Velo mag norm "<<velo_mag_norm_<<endl;

    auto tracked_particles_coord_velo_in_one_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZHSV>(1.);
    tracked_particles_coord_velo_in_one_frame_octree.defineBoundingBox(Xc_bottom_.x, Xc_bottom_.y, Xc_bottom_.z, Xc_top_.x, Xc_top_.y, Xc_top_.z);
    tracked_particles_coord_velo_in_one_frame_octree.setInputCloud(tracked_particles_coord_velo_in_one_frame_cloud_);
    tracked_particles_coord_velo_in_one_frame_octree.addPointsFromInputCloud();

    cout<<"#####Find nearest neighbors and then predict using neighborhood average..."<<endl;
    if(verbal_flag) cout<<"ID"<<std::setw(8)<<"Xc"<<std::setw(40)<<"num_voisin"<<std::setw(10)<<"min_dist"<<std::setw(10)<<"velocity"<<std::setw(45)<<"weighting_flag"<<endl;

    int num_with_predictor=0, num_without_predictor=0;
    for(auto& pt: particles_tgt){
        if(pt->is_ontrack() && !pt->is_tracked() && pt->is_to_be_triangulated()){

            auto pc=pt->getPreviousXcoord(it_seq_cnt);

            auto sum_velo=cv::Point3d(0., 0., 0.);
            double sum_weight=0.;
            int num_voisin=0;

            std::vector<int>    nrst_idx;
            std::vector<float>  nrst_err_sq;      
            if(tracked_particles_coord_velo_in_one_frame_octree.radiusSearch(cv2pcl(pc, cv::Point3d(0,0,0)), num_avg_dist_*avg_part_dist_, nrst_idx, nrst_err_sq) > 0){
                for(int i=0; i<nrst_idx.size(); i++){
                    auto dist=std::sqrt( nrst_err_sq[i] );
                    auto weight=1./(avg_part_dist_*std::sqrt(2.*M_PI))*std::exp(-0.5*std::pow(dist,2)/std::pow(avg_part_dist_,2));
                    auto pt_tgt=tracked_particles_coord_velo_in_one_frame_cloud_->points[ nrst_idx[i] ];

                    sum_velo   += weight*cv::Point3d(pt_tgt.h, pt_tgt.s, pt_tgt.v);
                    sum_weight += weight;

                    num_voisin++;
                }
            }

            auto pc_cnt = pc;

            if(num_voisin>=num_min_voisin_){
                pt->setis_with_predictor_as(true);
                pc_cnt = pc + dtObs_*sum_velo/sum_weight;
                if(!checkParticleIsOutOfRange(pc_cnt, Xc_top_, Xc_bottom_))
                    pt->push_backXcoordElement(pc_cnt, it_seq_cnt);
                else{
                    utils::maintainPositionInRange(pc_cnt, Xc_top_, Xc_bottom_);
                    pt->push_backXcoordElement(pc_cnt, it_seq_cnt);
                }
                num_with_predictor++;
            }
            else{
                pt->setis_with_predictor_as(false);
                pt->push_backXcoordElement(pc_cnt, it_seq_cnt);
                num_without_predictor++;
            }

            pt->setit_seq_fin(it_seq_cnt);
        }
    }
    cout<<endl
    <<"Number of particles that are predicted using neighboring particles: "<<num_with_predictor<<endl
    <<"Number of particles whose neighbors do not have enough particles  : "<<num_without_predictor<<endl;
}


void OptMethod::appendParticles(int it_seq_cnt, std::vector<Particle_ptr>& particles_tgt){

    bool verbal_flag=false;

    cout<<"#####Append particle for undefined particles"<<endl<<endl;

    double   err_norm=0., err_norm_pre=0.;
    int n_append_particles_cnt=0;

    auto spawned_particles_in_one_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(1.);
    spawned_particles_in_one_frame_octree.defineBoundingBox(Xc_bottom_.x, Xc_bottom_.y, Xc_bottom_.z, Xc_top_.x, Xc_top_.y, Xc_top_.z); 
    spawned_particles_in_one_frame_octree.setInputCloud(spawned_particles_in_one_frame_cloud_);
    spawned_particles_in_one_frame_octree.addPointsFromInputCloud();

    pcl::ExtractIndices<pcl::PointXYZI>  extract;
    pcl::PointIndices::Ptr               appended_particles_indices = pcl::make_shared<pcl::PointIndices>();

    for(auto& pt: particles_tgt){
        if(pt->is_ontrack() && !pt->is_tracked() && pt->is_to_be_triangulated() && pt->length()==it_seq_cnt-pt->it_seq_deb()+1){

            auto pc = pt->getCurrentXcoord(it_seq_cnt);
    
            auto search_radius_in_mm = (cv::norm(pc-pt->getPreviousXcoord(it_seq_cnt))<1e-9 && !pt->is_with_predictor()) ?  num_pixels_radius_without_predictor_*cv::norm(dXc_px_) :
                                                                                                                            num_pixels_radius_with_predictor_*cv::norm(dXc_px_);
            std::vector<int>   result_index;
            std::vector<float> sqr_distance;                                                                                              

            if(spawned_particles_in_one_frame_octree.radiusSearch(cv2pcl(pc,0), search_radius_in_mm, result_index, sqr_distance) > 0){
                std::vector<std::pair<int, float>> order(result_index.size());

                for(int i = 0; i < result_index.size(); i++)
                    order[i] = std::make_pair(result_index[i], sqr_distance[i]);

                std::sort(order.begin(), order.end(), [](auto& lop, auto & rop) { return lop.second < rop.second; });

                int i=0;
                while(i<order.size())
                {
                    auto it = std::find(std::begin(appended_particles_indices->indices),
                                        std::end(appended_particles_indices->indices), 
                                        order[i].first);

                    if(it == std::end(appended_particles_indices->indices)){
                        pt->setXcoordElement(pcl2cv(spawned_particles_in_one_frame_cloud_->points[order[i].first]), it_seq_cnt);
                        pt->push_backEvElement(pt->Ev().back(), it_seq_cnt);
                        pt->markAsTriangulated();

                        pt->markAsSide(pt->part_index());

                        n_append_particles_cnt++;

                        appended_particles_indices->indices.push_back(order[i].first);

                        break;
                    }
                    else{
                        i++;
                    }
                }

                if(i==order.size()){
                    pt->pop_back_Xcoord();
                    pt->setit_seq_fin(it_seq_cnt-1);
                }
            }
            else{
                pt->pop_back_Xcoord();
                pt->setit_seq_fin(it_seq_cnt-1);
            }
        }
    }

    cout<<"spawned_particles_in_one_frame_cloud_->size() before: "<<spawned_particles_in_one_frame_cloud_->size()<<endl;

    extract.setInputCloud(spawned_particles_in_one_frame_cloud_);
    extract.setIndices(appended_particles_indices);
    extract.setNegative(true);
    extract.filter(*spawned_particles_in_one_frame_cloud_);

    cout<<"appended_particles_indices->indices.size()          : "<<appended_particles_indices->indices.size()<<endl;
    cout<<"spawned_particles_in_one_frame_cloud_->size() after : "<<spawned_particles_in_one_frame_cloud_->size()<<endl;

    n_append_particles_.push_back(n_append_particles_cnt);
    cout<<"Number of particles having been appended            : "<<n_append_particles_cnt<<endl;
                  
    bool verbal_guard=true;
    int  num_part_not_append=0;
    for(auto& pt: particles_tgt){
        if(pt->is_ontrack() && !pt->is_tracked() && pt->is_to_be_triangulated()){
            if(verbal_guard){
                verbal_guard=false;
                cout<<"NOT FOUND APPEND: ...";
            }
            num_part_not_append++;
        }
    }
    cout<<endl;
    cout<<"Number of particles still to-be-append : "<<num_part_not_append<<endl;
}

void OptMethod::addNewParticles(int it_seq_cnt, std::vector<Particle_ptr>& particles_tgt){

    cout<<"add new particles"<<endl;

    std::vector<int> id_vec;
    for(const auto& pt: particles_tgt) id_vec.push_back(pt->ID());
    auto id_max=*std::max_element(id_vec.begin(), id_vec.end());

    int n_new_particles_cnt=0, start_index=id_max+1;

    for(const auto& spc : spawned_particles_in_one_frame_cloud_->points){

        particles_tgt.push_back( std::make_unique<Particle>( start_index+n_new_particles_cnt, it_seq_cnt, it_seq_cnt, std::vector<cv::Point3d>{cv::Point3d(spc.x, spc.y, spc.z)}, std::vector<double>{spc.intensity} ) );

        particles_tgt.back()->markAsNew();
        particles_tgt.back()->markAsTriangulated();
        n_new_particles_cnt++;
    }

    n_new_particles_.push_back(n_new_particles_cnt);
}

void OptMethod::printTrackInformation(int it_seq_cnt){

    std::vector<int> length_tgt(it_seq_cnt, 0);

    for(auto& pt: this->particles_field()){
        for(int it=1; it<it_seq_cnt+1; it++){
            if(pt->it_seq_deb() == it && pt->it_seq_fin() == it_seq_cnt) length_tgt[it-1]++;
        }
    }

    for(int it=1; it<it_seq_cnt+1; it++){
        cout<<str(boost::format("Track %1$2d, %2$2d %3$5d") % it % it_seq_cnt %  length_tgt[it-1])<<endl;
    }

    total_tracks_ = std::accumulate(length_tgt.begin(), length_tgt.end(), 0.0);
    cout<<"Total tracks : "<<total_tracks_<<endl;
}

void OptMethod::removeParticlesThatAreLostOrGhost(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt){

    cout<<"remove Particles That Are Lost Or Ghost"<<endl;
    for(auto& pf: particles_tgt){
        if(pf->is_ontrack() && this->checkIfParticleIsToBeOptimized(pf.get())){
            if(pf->Ev().back()<intensity_deletion_threshold_ || 
               pf->intensity_variation_ratio()<intensity_variation_ratio_threshold_){    
                pf->setit_seq_fin(it_seq_cnt-1);
                if(pf->is_tracked()){
                    pf->markAsLost();
                    pf->deleteData(false); //DO NOT delete particle data
                    pf->pop_back_Xcoord();
                }
                else{
                    pf->markAsGhost();
                    pf->deleteData(true);  //delete particle data
                }
            }
        }
    }
    utils::showInfoOnDeletedParticles();
}

void OptMethod::processingResidualImage(const std::vector<Camera_ptr>& cam, int it, 
                                        const std::vector<Particle_ptr>& particles_tgt, int it_seq_cnt,
                                        bool first_flag, bool save_flag, int lapiv_iter){

    if(first_flag){
        if(res_glob_.size()==it)
            res_glob_.push_back(0.0);
        else if(res_glob_.size()==it+1)
            res_glob_.back()=0.0;
    }
    else{
        if(res_glob_.size()==it+lMax_)  
            res_glob_.push_back(0.0);
        else if(res_glob_.size()==it+lMax_+1)
            res_glob_.back()=0.0;
    }

    for(auto& particle_p : particles_tgt )
        if(particle_p->is_ontrack() && this->checkIfParticleIsToBeOptimized(particle_p.get()))
            particle_p->clearimgResparPos();

    for(auto& ic : cam){
        if(!ic->Iproj().empty()) ic->clearIproj();
        if(!ic->Ires().empty())  ic->clearIres();

        ic->setIprojElement( ic->sumParticleImage(particles_tgt, Camera::sumParticle_Type::Detected) );

        ic->computeImageResidual(it_seq_cnt, ipr_full_flag_);

        if(save_flag){
            ic->saveImageProjection(it_seq_cnt, "Iproj"+std::to_string(it)+std::to_string(lapiv_iter), res_path_);
            ic->saveImageResidual(it_seq_cnt, "Ires"+std::to_string(it)+std::to_string(lapiv_iter), res_path_);
        }

        if(first_flag)
            res_glob_.at(it)+=ic->computeGlobalResidual(it_seq_cnt);
        else
            res_glob_.at(it+lMax_)+=ic->computeGlobalResidual(it_seq_cnt);
    }
}

/// Process the position of lost particles

void OptMethod::processingLostParticles(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt, 
                                        const std::vector<Camera_ptr>& cam){
    cout<<"Do nothing yet!!!"<<endl;
}

void OptMethod::computeImageResidualParticle(Particle* particle_p, int it_seq_cnt, const std::vector<Camera_ptr>& cam){
    
    int it_img = it_seq_cnt - particle_p->it_seq_fin();

    assert(it_img==0);

    for(int i_cam=0; i_cam<cam.size(); i_cam++){
        
        cv::Mat imgRespar;

        if(particle_p->on_image().at(i_cam)==1){

            auto pos=particle_p->imgPos()[i_cam][it_img];

            imgRespar.create(pos.size(), CV_32FC1);

            cv::Mat imgp;
            particle_p->img()[i_cam][it_img](pos).convertTo(imgp, CV_32FC1);
            assert(cam.at(i_cam)->Ires().type()==imgp.type());

            cv::add( cam.at(i_cam)->Ires()( particle_p->imgResparPos()[i_cam][it_img] ), imgp, imgRespar );
        }
        else{
            imgRespar=cv::Mat(1, 1, CV_32FC1, cv::Scalar(0));
        }

        particle_p->push_backimgResparElement(std::vector<cv::Mat>(1, imgRespar));
    }

    assert(particle_p->imgRespar().size()==cam[0]->n_cam());
    assert(particle_p->imgResparPos().size()==cam[0]->n_cam());
    for(const auto& i: particle_p->imgRespar())    assert(i.size()==it_img+1);
    for(const auto& i: particle_p->imgResparPos()) assert(i.size()==it_img+1);
}

void OptMethod::rescaleIntensityUsingTarget(Particle* particle_p, int it_seq_cnt, const std::vector<Camera_ptr>& cam, bool only_scale_large_variation_flag){

    cv::Mat imgp, imgRespar;

    int it_img=it_seq_cnt - particle_p->it_seq_fin();
    assert(it_img==0);

    std::vector<double> Irespari_vec, Iparti_vec;

    for(int i_cam=0; i_cam<cam.size(); i_cam++){
        particle_p->img()[i_cam][it_img].convertTo(imgp, CV_32FC1);
        particle_p->imgRespar()[i_cam][it_img].convertTo(imgRespar, CV_32FC1);

        // if(scaling_intensity_type_ == OptMethod::ScalingIntensity_Type::Mild){
        // auto imgpsum = cv::sum( imgp );
        // Iparti_vec.push_back( imgpsum(0) );
        // cv::Mat   imgRespar_nonngtv = cv::max(imgRespar, 0);
        // auto imgResparsum = cv::sum( imgRespar_nonngtv );
        // Irespari_vec.push_back( imgResparsum(0) );
        // }
        // else{
        Iparti_vec.push_back( cv::norm( imgp ) );
        Irespari_vec.push_back( cv::norm( imgRespar ) );
        // }
    }

    auto Iparti_max = *std::max_element(Iparti_vec.begin(), Iparti_vec.end());
    auto Irespari_max  = *std::max_element(Irespari_vec.begin(),  Irespari_vec.end());

    auto Ipart = std::accumulate(Iparti_vec.begin(), Iparti_vec.end(), 0.) - Iparti_max;
    auto Irespart  = std::accumulate(Irespari_vec.begin(), Irespari_vec.end(), 0.) - Irespari_max;

    auto intensity_variation_ratio = std::abs(Ipart)>1e-4 ? std::sqrt(Irespart/Ipart) : 1.;

    if(only_scale_large_variation_flag){
        particle_p->setintensity_variation_ratio( intensity_variation_ratio < intensity_variation_ratio_threshold_ ? intensity_variation_ratio : 1. );
        intensity_variation_ratio = 1.;
    }
    else{
        particle_p->setintensity_variation_ratio(intensity_variation_ratio);
    }

    auto Ec=particle_p->getCurrentE(it_seq_cnt)*intensity_variation_ratio;

    if(isnan(Ec)){
        cout<<"Ipart "<<Ipart<<endl
            <<"Irespart "<<Irespart<<endl
            <<"particle_p->getCurrentE(it_seq_cnt) "<<particle_p->getCurrentE(it_seq_cnt)<<endl
            <<"intensity_variation_ratio "<<intensity_variation_ratio<<endl
            <<"Ec "<<Ec<<endl;
        std::abort();
    }

    particle_p->setEvElement(Ec, it_seq_cnt);
}

double OptMethod::correctIntensityMARTlike(Particle* particle_p, int it_seq_cnt, const std::vector<Camera_ptr>& cam){

    this->rescaleIntensityUsingTarget(particle_p, it_seq_cnt, cam);

    particle_p->clearimgAndimgPos();
    particle_p->projectToImageSpace(it_seq_cnt, cam);
    
    return particle_p->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_fine_search_);
}

double OptMethod::jumpToMinimumResidual(Particle* particle_p, const cv::Point3d& dXc_radius, int it_seq_cnt, const std::vector<Camera_ptr>& cam){

    double ref_coord_X, ref_coord_Xm, ref_coord_Xp, ref_coord_Y, ref_coord_Ym, ref_coord_Yp, ref_coord_Z, ref_coord_Zm, ref_coord_Zp;
    cv::Point3d Xcopt;

    ref_coord_X=particle_p->getCurrentXcoord(it_seq_cnt).x;
    ref_coord_Xm=ref_coord_X-dXc_radius.x;
    ref_coord_Xp=ref_coord_X+dXc_radius.x;
    ref_coord_Y=particle_p->getCurrentXcoord(it_seq_cnt).y;
    ref_coord_Ym=ref_coord_Y-dXc_radius.y;
    ref_coord_Yp=ref_coord_Y+dXc_radius.y;
    ref_coord_Z=particle_p->getCurrentXcoord(it_seq_cnt).z;
    ref_coord_Zm=ref_coord_Z-dXc_radius.z;
    ref_coord_Zp=ref_coord_Z+dXc_radius.z;

    cv::Point3d ref_coord_Xmc=cv::Point3d(ref_coord_Xm, ref_coord_Ym, ref_coord_Zm);
    cv::Point3d ref_coord_Xpc=cv::Point3d(ref_coord_Xp, ref_coord_Yp, ref_coord_Zp);

    utils::maintainPositionInRange(ref_coord_Xmc, Xc_top_, Xc_bottom_);
    utils::maintainPositionInRange(ref_coord_Xpc, Xc_top_, Xc_bottom_);

    Eigen::Vector3d Xvec,Yvec,Zvec;
    Xvec<< ref_coord_Xmc.x, ref_coord_X, ref_coord_Xpc.x;
    Yvec<< ref_coord_Xmc.y, ref_coord_Y, ref_coord_Xpc.y;
    Zvec<< ref_coord_Xmc.z, ref_coord_Z, ref_coord_Xpc.z;

    Eigen::Matrix3d XX,YY,ZZ;
    XX=Xvec.transpose().replicate(3,1);
    YY=Yvec.replicate(1,3);
    
    std::vector<Eigen::Matrix3d> XXv(3, XX);
    std::vector<Eigen::Matrix3d> YYv(3, YY);
    std::vector<Eigen::Matrix3d> ZZv;

    ZZ.setOnes();
    for(int i=0; i<3; i++)
        ZZv.push_back(Zvec(i)*ZZ);

    Eigen::Matrix3d resij;
    double resmin=0.,resopt;
    int opti=0,optj=0,optk=0;

    resij.setZero();
    std::vector<Eigen::Matrix3d> res(3, resij);         

    for(int k=0; k<3; k++){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){

                particle_p->setXcoordElement( cv::Point3d(XXv.at(k)(i,j), YYv.at(k)(i,j), ZZv.at(k)(i,j)), it_seq_cnt );

                particle_p->clearimgAndimgPos();
                particle_p->projectToImageSpace(it_seq_cnt, cam);
                res.at(k)(i,j)=particle_p->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_warm_start_);

                if(i==0 && j==0 && k==0){
                    resmin=res.at(k)(i,j);
                    opti=i;
                    optj=j;
                    optk=k;
                }
                else{
                    if(res.at(k)(i,j)<resmin){
                        resmin=res.at(k)(i,j);
                        opti=i;
                        optj=j;
                        optk=k;
                    }
                }
            }
        }
    }

    Xcopt=cv::Point3d(XXv.at(optk)(opti,optj), YYv.at(optk)(opti,optj), ZZv.at(optk)(opti,optj));

    utils::maintainPositionInRange(Xcopt, Xc_top_, Xc_bottom_);

    particle_p->setXcoordElement(Xcopt, it_seq_cnt);

    particle_p->clearimgAndimgPos();
    particle_p->projectToImageSpace(it_seq_cnt, cam);
    resopt=particle_p->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_warm_start_);

    return resopt;
}

void OptMethod::setres_parti(int warm_start_max_iter, int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt,
                            const std::vector<Camera_ptr>& cam){

    res_parti_.setZero(warm_start_max_iter+1, particles_tgt.size());
    for(int p=0; p<particles_tgt.size(); p++){
        if(particles_tgt[p]->is_ontrack() && particles_tgt[p]->is_to_be_tracked())
            res_parti_(0,p)=particles_tgt[p]->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_warm_start_);
        else
            res_parti_(0,p)=0.0;
    }
}

bool OptMethod::checkConvergence(int iter, int max_iter, double delta_res_threshold){

    bool exit_flag=false;

    if( std::abs(res_glob_.back() - res_glob_.rbegin()[1])/res_glob_.back() < delta_res_threshold ){
        cout<<"The cost function changes too small!"<<endl;
        exit_flag=true;
    }

    if( iter >= 3 ){
        auto min_var = res_glob_.rbegin()[2]*0.01;
        if( res_glob_.back() - res_glob_.rbegin()[1] > min_var && res_glob_.rbegin()[1] - res_glob_.rbegin()[2] > min_var ){
            cout<<"The cost function failed to be minimized, early stop!"<<endl;
            exit_flag=true;
        }
    }

    if( iter==max_iter ){
        cout<<"Reach maximum iteration "<<iter<<endl;
        exit_flag=true;
    }

    return exit_flag;
}

void OptMethod::saveGlobalResidual(int it_seq_cnt, bool overwrite_flag){

    std::vector<cv::Point3d> mean_error;
    std::vector<cv::Point2d> mean_error_inpixel;
    std::vector<double> mean_error_E;
    int i=0;

    std::ofstream resfile;
    std::string   resfilename=res_path_+"/result.txt", line;

    if(overwrite_flag)
        resfile.open(resfilename, std::ios::out);
    else
        resfile.open(resfilename, std::ios::out | std::ios::app);

    total_tracks_-=n_new_particles_.back();

    std::string cycle=str(boost::format("#Cycle %1%") % it_seq_cnt);
    std::string ghost=str(boost::format("#Total tracks reconstructed without new ones %1$6d") % total_tracks_);
    std::string title=str(boost::format("#ite res_glob Append New"));
    
    resfile<<cycle<<endl;
    resfile<<ghost<<endl;
    resfile<<title<<endl;

    for(int i=0; i<res_glob_.size(); i++){
        line=str(boost::format("%1$02d  %2$10.6e %3$5d %4$5d") 
                                        % i % res_glob_[i] % n_append_particles_[i] % n_new_particles_[i]);

        resfile<<line<<endl;
    }
    resfile.close();
}

void OptMethod::processingImageResidualParticle(const std::vector<Particle_ptr>& particles_tgt, 
                                                int it_seq_cnt, const std::vector<Camera_ptr>& cam){
#ifdef INTELTBB
    tbb::parallel_for( tbb::blocked_range<int>(0, particles_tgt.size()),
                        [&](const tbb::blocked_range<int>& r){ for(int p=r.begin(); p!=r.end(); ++p){
#else
    for(int p=0; p<particles_tgt.size(); p++){            
#endif    
        if(particles_tgt[p]->is_ontrack() && this->checkIfParticleIsToBeOptimized(particles_tgt[p].get())){
            particles_tgt[p]->clearimgRespar();
            this->computeImageResidualParticle(particles_tgt[p].get(), it_seq_cnt, cam);
        }
    }
#ifdef INTELTBB
    });
#endif
}

void OptMethod::doWarmStart(int it_seq_cnt, const std::vector<Camera_ptr>& cam,
                            const std::vector<Particle_ptr>& particles_tgt){

    warm_start_do_flag_.assign(n_particles_, 1);
    warm_start_ratio_ = cv::Point3d(warm_start_pixel_, warm_start_pixel_, warm_start_pixel_);
    res_prev_.assign(n_particles_, res_glob_[0]);

    for(int l=1; l<warm_start_max_iter_+1; l++){

        cout<<"-----------------------------------------------------"<<endl;
        cout<<"Warm start iteration : "<<l<<endl;
        cout<<"residual begin ite "<<l<<"    : "<<res_glob_.at(l-1)<<endl;

#ifdef INTELTBB
        tbb::parallel_for( tbb::blocked_range<int>(0, particles_tgt.size()),
                    [&](const tbb::blocked_range<int>& r){ for(int p=r.begin(); p!=r.end(); ++p){
#else
        for(int p=0; p<particles_tgt.size(); p++){
#endif
            this->doWarmStartInnerLoop(particles_tgt[p].get(), p, l, it_seq_cnt, cam);
        }
#ifdef INTELTBB
        });
#endif
        this->processingResidualImage(cam, l, particles_tgt, it_seq_cnt, true);
        
        this->processingImageResidualParticle(particles_tgt, it_seq_cnt, cam);

        cout<<"residual after position correction ite "<<l<<"    : "<<res_glob_.at(l)<<endl;

        n_new_particles_.push_back(0);
        n_append_particles_.push_back(0);

        if(this->checkConvergence(l, warm_start_max_iter_))   break;
    }
}

void OptMethod::doWarmStartInnerLoop(Particle* particle_tgt, int idx, int iter, int it_seq_cnt, const std::vector<Camera_ptr>& cam){

    if(particle_tgt->is_ontrack()){
        if(particle_tgt->is_tracked() || (iter==1 && particle_tgt->is_to_be_tracked()))
        {
            if( warm_start_ratio_.x<warm_start_ratio_valve_ ||
                warm_start_ratio_.y<warm_start_ratio_valve_ || 
                warm_start_ratio_.z<warm_start_ratio_valve_ ){
                warm_start_do_flag_[idx]=0;
            }

            if(warm_start_do_flag_[idx]!=0){

                if(warm_start_ratio_.x < 0.1)
                    warm_start_ratio_ = cv::Point3d(0.1,0.1,0.1);

                auto dXc_radius=cv::Point3d(dXc_px_.x*warm_start_ratio_.x, 
                                            dXc_px_.y*warm_start_ratio_.y, 
                                            dXc_px_.z*warm_start_ratio_.z);

                res_parti_(iter,idx)=this->jumpToMinimumResidual(particle_tgt, dXc_radius, it_seq_cnt, cam);

                if(res_parti_(iter,idx)>=res_prev_[idx])
                    warm_start_ratio_ = warm_start_ratio_*warm_start_relaxfactor_;
                else
                    res_prev_[idx] = res_parti_(iter,idx);
            }
            else
                res_parti_(iter,idx) = res_parti_(iter-1,idx);

            if(particle_tgt->is_to_be_tracked())
                particle_tgt->markAsTracked();
        }
    }
}

void OptMethod::removeOutlier(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt, int k){

    if(!tracked_particles_coord_velo_in_one_frame_cloud_->empty()) tracked_particles_coord_velo_in_one_frame_cloud_->clear();
 
    cout<<"#####Compute velocity..."<<endl<<endl;
    for(const auto& pt: particles_tgt){
        if(pt->is_ontrack() && pt->is_tracked() && !pt->is_side()){
            pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference);

            auto Xc = pt->getPreviousXcoord(it_seq_cnt);
            auto V  = pt->getPreviousOfPreviousV(it_seq_cnt);

            tracked_particles_coord_velo_in_one_frame_cloud_->push_back( cv2pcl(Xc, V) );
        }
    }   

    auto tracked_particles_coord_velo_in_one_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZHSV>(1.);
    tracked_particles_coord_velo_in_one_frame_octree.defineBoundingBox(Xc_bottom_.x, Xc_bottom_.y, Xc_bottom_.z, Xc_top_.x, Xc_top_.y, Xc_top_.z); 
    tracked_particles_coord_velo_in_one_frame_octree.setInputCloud(tracked_particles_coord_velo_in_one_frame_cloud_);
    tracked_particles_coord_velo_in_one_frame_octree.addPointsFromInputCloud();
    
    bool verbal_flag = false;

    for(const auto& pt: particles_tgt){
        if(pt->is_ontrack() && pt->is_tracked() && pt->length()>=3){
            // spatial coherence check
            this->checkSpatialCoherence(pt.get(), tracked_particles_coord_velo_in_one_frame_octree, it_seq_cnt, k, verbal_flag);
            // temporal coherence check
            if(pt->is_ontrack() && pt->length()>=4){
                this->checkTemporalCoherence(pt.get(), it_seq_cnt, k, verbal_flag);
            }
        }
    }
    utils::showInfoOnDeletedParticles();
}

void OptMethod::checkSpatialCoherence(Particle* pt, const pcl::octree::OctreePointCloudSearch<pcl::PointXYZHSV>& tracked_particles_coord_velo_in_one_frame_octree,
                                     int it_seq_cnt, int k, bool verbal_flag){

    auto pc_cnt = pt->getCurrentXcoord(it_seq_cnt);

    auto search_radius_in_mm = num_avg_dist_*avg_part_dist_;

    std::vector<int>         nrst_idx;
    std::vector<float>       nrst_err_sq;
    std::vector<cv::Point3d> velo_near_vec;
    cv::Point3d              velo_avg=cv::Point3d(0,0,0);

    if(tracked_particles_coord_velo_in_one_frame_octree.radiusSearch(cv2pcl(pc_cnt, cv::Point3d(0,0,0)), search_radius_in_mm, nrst_idx, nrst_err_sq) > 0){
        std::vector<double>  nrst_err;
        for(auto& err_sq : nrst_err_sq) nrst_err.push_back( static_cast<double>(std::sqrt(err_sq)) );

        if(this->checkIfParticleVelocityIsCoherentWithItsNeighbors(pt, it_seq_cnt, nrst_idx, nrst_err, search_radius_in_mm,
                                                                    velo_near_vec, velo_avg, verbal_flag)){

            /// for very slow tracks that displace no more than 1 pixel between every two snapshots, 
            if(all_of( pt->V().cbegin(), std::prev(pt->V().end(), 2), [=](const auto& velo){ return cv::norm(velo*dtObs_)<cv::norm(dXc_px_); } )){
                if(k==kMax_){
                    pt->markAsLost();
                    pt->deleteData(false); //DO NOT delete particle data
                    pt->pop_back_Xcoord();
                    pt->setit_seq_fin(it_seq_cnt-1);
                }
                else{
                    this->reestimateParticlePositionUsingSmoothSpline(pt, it_seq_cnt, velo_near_vec, verbal_flag);
                }
            }
            /// for fast track that displace more than 1 pixel between every two snapshots,
            else if(all_of( pt->V().cbegin(), pt->V().cbegin()+std::min(pt->length()-2, stable_long_track_length_spatial_), [=](const auto& velo){ return cv::norm(velo*dtObs_)>cv::norm(dXc_px_); } )){
                if(k==kMax_){
                    if(pt->length() < stable_long_track_length_spatial_){
                        pt->markAsLost();
                        pt->deleteData(false); //DO NOT delete particle data
                        pt->pop_back_Xcoord();
                        pt->setit_seq_fin(it_seq_cnt-1);
                    }
                    else{
                        if(velo_near_vec.size() > min_num_near_velo_){
                            pt->markAsLost();
                            pt->deleteData(false); //DO NOT delete particle data
                            pt->pop_back_Xcoord();
                            pt->setit_seq_fin(it_seq_cnt-1);
                        }
                        else{
                            // pt->markAsLost();
                            // pt->deleteData(false); //DO NOT delete particle data
                            // pt->pop_back_Xcoord();
                            // pt->setit_seq_fin(it_seq_cnt-1);  
                        }
                    }
                }
                else{
                    this->reestimateParticlePositionUsingSmoothSpline(pt, it_seq_cnt, velo_near_vec, verbal_flag);
                }
            }
            /// for other tracks
            else{
                if(k==kMax_){
                    pt->markAsLost();
                    pt->deleteData(false); //DO NOT delete particle data
                    pt->pop_back_Xcoord();
                    pt->setit_seq_fin(it_seq_cnt-1);
                }
                else{
                    this->reestimateParticlePositionUsingSmoothSpline(pt, it_seq_cnt, velo_near_vec, verbal_flag);
                }
            }
        }
    }
}

void OptMethod::checkTemporalCoherence(Particle* pt, int it_seq_cnt, int k, bool verbal_flag){

    auto sharp_falg      = this->qualifyLinearFitErrorsOfLastFourSnapshots(pt, it_seq_cnt);
    auto restablize_flag = false;

    /// for very slow tracks that displace no more than 1 pixel between every two snapshots, 
    if(all_of( pt->V().cbegin(), std::prev(pt->V().end(), 2), [=](const auto& velo){ return cv::norm(velo*dtObs_)<cv::norm(dXc_px_); } ))
    {
        restablize_flag = pt->length()<=5;
        if(restablize_flag){
            if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt))){
                this->reestimateParticlePositionUsingPolynomial(pt, it_seq_cnt, verbal_flag);
                if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt))){
                    this->reestimateParticlePositionUsingLinearPredictor(pt, it_seq_cnt, k ,verbal_flag);
                }
            }
        }
        else{
            if(sharp_falg){
                if(k==kMax_){
                    pt->markAsLost();
                    pt->deleteData(false); //DO NOT delete particle data
                    pt->pop_back_Xcoord();
                    pt->setit_seq_fin(it_seq_cnt-1);  
                }
                else{
                    // do nothing
                }
            }
        }
    }
    /// for fast track that displace more than 1 pixel between every two snapshots,
    else if(all_of( pt->V().cbegin(), pt->V().cbegin()+std::min(pt->length()-2, stable_long_track_length_temporal_), [=](const auto& velo){ return cv::norm(velo*dtObs_)>cv::norm(dXc_px_); } ))
    {
        restablize_flag = pt->length()<=stable_long_track_length_temporal_;
        if(restablize_flag){
            if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt))){
                this->reestimateParticlePositionUsingPolynomial(pt, it_seq_cnt, verbal_flag);
                if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt))){
                    this->reestimateParticlePositionUsingLinearPredictor(pt, it_seq_cnt, k ,verbal_flag);
                }
            }
        }
        else{            
            if(sharp_falg){
                if(k==kMax_){
                    pt->markAsLost();
                    pt->deleteData(false); //DO NOT delete particle data
                    pt->pop_back_Xcoord();
                    pt->setit_seq_fin(it_seq_cnt-1);  
                }
                else{
                    // do nothing
                }
            }
        }
    }
    else{
        restablize_flag = pt->length()<=5;
        if(restablize_flag){
            if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt))){
                this->reestimateParticlePositionUsingPolynomial(pt, it_seq_cnt, verbal_flag);
                if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt))){
                    this->reestimateParticlePositionUsingLinearPredictor(pt, it_seq_cnt, k ,verbal_flag);
                }
            }
        }
        else{
            if(sharp_falg){
                if(k==kMax_){
                    pt->markAsLost();
                    pt->deleteData(false); //DO NOT delete particle data
                    pt->pop_back_Xcoord();
                    pt->setit_seq_fin(it_seq_cnt-1);  
                }
                else{
                    // do nothing
                }
            }
        }
    }
}

bool OptMethod::qualifyLinearFitErrorsOfLastFourSnapshots(Particle* pt, int it_seq_cnt, bool verbal_flag){

    VectorXd X(pt->length()), Y(pt->length()), Z(pt->length());
    for(int i=0; i<pt->length(); i++){
        X(i)=pt->Xcoord().at(i).x;
        Y(i)=pt->Xcoord().at(i).y;
        Z(i)=pt->Xcoord().at(i).z;
    }

    VectorXd Xfit = polynomialLinearFit1D(X);
    VectorXd Yfit = polynomialLinearFit1D(Y);
    VectorXd Zfit = polynomialLinearFit1D(Z);

    VectorXd X_err, Y_err, Z_err;

    X_err = Xfit - X.tail(4);
    Y_err = Yfit - Y.tail(4);
    Z_err = Zfit - Z.tail(4);

    auto err = std::sqrt(std::pow(X_err.norm(),2)+std::pow(Y_err.norm(),2)+std::pow(Z_err.norm(),2));

    if(verbal_flag){
        if(err>linear_fit_error_threshold_){
            pt->printID();
            cout<<"Err  "<<err<<endl;
        }
    }

    return err>linear_fit_error_threshold_;
}

void OptMethod::assertVelocityQualityCheck(int it_seq_cnt, const std::vector<Particle_ptr>& particles_tgt, bool verbal_flag){

    cout<<"#####assert VelocityQualityCheck..."<<endl<<endl;
    for(const auto& pt: particles_tgt){
        if(pt->is_ontrack() && pt->is_tracked() && pt->length()>=3){
            pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference);
            /// for very slow tracks
            if(all_of( pt->V().cbegin(), std::prev(pt->V().end(), 2), [=](const auto& velo){ return cv::norm(velo*dtObs_)<cv::norm(dXc_px_); } )){
                if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt))){
                    auto pc_cnt = pt->getPreviousXcoord(it_seq_cnt) + dtObs_*pt->getPreviousOfPreviousV(it_seq_cnt);
                    pt->setXcoordElement(pc_cnt, it_seq_cnt);
                    pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference, false);
                    if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt), true)){
                        cout<<"slow track"<<endl;
                        std::abort();
                    }
                }
            }
            /// for fast track
            else if(all_of( pt->V().cbegin(), std::prev(pt->V().end(), 2), [=](const auto& velo){ return cv::norm(velo*dtObs_)>cv::norm(dXc_px_); } )){
                // do nothing
            }
        }
    }
}

bool OptMethod::checkIfParticleVelocityIsCoherentWithItsNeighbors(Particle* pt, int it_seq_cnt, const std::vector<int>& nrst_idx, const std::vector<double> nrst_err, double search_radius_in_mm,
                                                                    std::vector<cv::Point3d>& velo_near_vec, cv::Point3d& velo_avg, bool verbal_flag){

    bool reestimate_flag = false;
    bool check_angle_flag = false;

    if(verbal_flag){
        pt->printID();
        cout<<"Estimated particle velocity "<<pt->getCurrentV(it_seq_cnt)<<endl;
    }

    double err_ratio = 0.;
    double err_angle = 0;

    std::vector<double> angle_vec;
    for(int i=0; i<pt->length()-3; i++){
        angle_vec.push_back( utils::angle(pt->V()[i+1], pt->V()[i]) );
    }

    /// for very slow tracks that displace no more than 1 pixel between every two snapshots,
    if(all_of( pt->V().cbegin(), std::prev(pt->V().end(), 2), [=](const auto& velo){ return cv::norm(velo*dtObs_)<cv::norm(dXc_px_); } )){
        err_ratio = max_err_v_ratio_;
        if(all_of(angle_vec.begin(), angle_vec.end(), [=](double angle){ return angle<CV_PI/20; })){
            check_angle_flag = true;
            err_angle = min_err_V_angle_;
        }
    }
    /// for fast track that displace more than 1 pixel between every two snapshots,
    else if(all_of( pt->V().cbegin(), pt->V().cbegin()+std::min(pt->length()-2, stable_long_track_length_spatial_), [=](const auto& velo){ return cv::norm(velo*dtObs_)>cv::norm(dXc_px_); } )){
        err_ratio = (pt->length()<stable_long_track_length_spatial_ ? min_err_v_ratio_ : max_err_v_ratio_);
        if(all_of(angle_vec.begin(), angle_vec.end(), [=](double angle){ return angle<CV_PI/20; })){
            check_angle_flag = true;
            err_angle = min_err_V_angle_;
        }
    }
    /// for other tracks
    else{
        err_ratio = max_err_v_ratio_;
    }

    auto sum_velo=cv::Point3d(0., 0., 0.);
    double sum_weight=0.;

    if(nrst_idx.size() >= 3){
        for(int i=0; i<nrst_idx.size(); i++){
            auto pt_tgt = tracked_particles_coord_velo_in_one_frame_cloud_->points[ nrst_idx[i] ];
            velo_near_vec.push_back( cv::Point3d(pt_tgt.h, pt_tgt.s, pt_tgt.v) );

            auto weight=1./(search_radius_in_mm*std::sqrt(2.*M_PI))*std::exp(-0.5*std::pow(nrst_err[i],2)/std::pow(search_radius_in_mm,2));
            sum_velo   += weight*velo_near_vec.back();
            sum_weight += weight;
        }
        velo_avg = sum_velo/sum_weight;

        std::vector<double> err_v;
        for(auto& velo_near: velo_near_vec){
            err_v.push_back(cv::norm(velo_near-velo_avg));
        }
        auto err_v_avg = std::accumulate(err_v.begin(), err_v.end(), 0.0)/static_cast<double>(err_v.size());

        reestimate_flag = cv::norm(pt->getCurrentV(it_seq_cnt) - velo_avg) > err_ratio*err_v_avg;

        if(!reestimate_flag && check_angle_flag) reestimate_flag = utils::angle(pt->getCurrentV(it_seq_cnt), velo_avg) > err_angle;

        if(verbal_flag){
            cout<<"Mean velocity               "<<velo_avg<<endl;
            for(auto& velo_near: velo_near_vec){
                cout<<"Near velocity               "<<velo_near<<endl;
            }
            if(reestimate_flag) cout<<"Incoherent with mean velocity!!!"<<endl;
        }
    }
    else{
        for(int i=0; i<nrst_idx.size(); i++){
            auto pt_tgt = tracked_particles_coord_velo_in_one_frame_cloud_->points[ nrst_idx[i] ];
            velo_near_vec.push_back( cv::Point3d(pt_tgt.h, pt_tgt.s, pt_tgt.v) );
            if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), velo_near_vec.back())){
                reestimate_flag = true;
                if(verbal_flag) cout<<"Incoherent velocity         "<<velo_near_vec.back()<<endl;
            }
        }
    }

    return reestimate_flag;
}

void OptMethod::reestimateParticlePositionUsingNeighborAverage(Particle* pt, int it_seq_cnt, const cv::Point3d& velo_avg, double relax_factor){

    auto pc_cnt = pt->getPreviousXcoord(it_seq_cnt) + dtObs_*velo_avg*relax_factor;
    pt->setXcoordElement(pc_cnt, it_seq_cnt);
    pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference, false);
}

void OptMethod::reestimateParticlePositionUsingSmoothSpline(Particle* pt, int it_seq_cnt, const std::vector<cv::Point3d>& velo_near_vec, bool verbal_flag){

    if(verbal_flag){
        pt->printID();
        pt->printLastNV(pt->length());
        pt->printLastNXcoord(pt->length());
    }

    VectorXd X(pt->length()), Y(pt->length()), Z(pt->length());
    for(int i=0; i<pt->length(); i++){
        X(i)=pt->Xcoord().at(i).x;
        Y(i)=pt->Xcoord().at(i).y;
        Z(i)=pt->Xcoord().at(i).z;
    }

    int smth_count=0, max_smth=3;
    auto velo_p = pt->getCurrentV(it_seq_cnt);
    while(any_of( velo_near_vec.cbegin(), velo_near_vec.cend(), [=](const auto& velo_anchor){ return !this->qualifyDiscrepancyComparedToAnchorVelocity(velo_p, velo_anchor); } )){
        double lambda=1e3;
        VectorXd Xfit = splineFit1D(X, lambda);
        VectorXd Yfit = splineFit1D(Y, lambda);
        VectorXd Zfit = splineFit1D(Z, lambda);

        pt->setXcoordElement(cv::Point3d(*(Xfit.tail(1).data()), *(Yfit.tail(1).data()), *(Zfit.tail(1).data())), it_seq_cnt);

        X[pt->length()-1]=pt->getCurrentXcoord(it_seq_cnt).x;
        Y[pt->length()-1]=pt->getCurrentXcoord(it_seq_cnt).y;
        Z[pt->length()-1]=pt->getCurrentXcoord(it_seq_cnt).z;

        pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference, false);
        velo_p = pt->getCurrentV(it_seq_cnt);

        if(verbal_flag){
            pt->printLastNV(pt->length());
            pt->printLastNXcoord(pt->length());
        }

        smth_count++;
        if(smth_count>max_smth) break;
    }
}

void OptMethod::reestimateParticlePositionUsingPolynomial(Particle* pt, int it_seq_cnt, bool verbal_flag){

    VectorXd X(pt->length()), Y(pt->length()), Z(pt->length());
    for(int i=0; i<pt->length(); i++){
        X(i)=pt->Xcoord().at(i).x;
        Y(i)=pt->Xcoord().at(i).y;
        Z(i)=pt->Xcoord().at(i).z;
    }

    double Xp, Yp, Zp;
    if(pt->length()==3){
        Xp = polynomialFit1D(X, true, 3, 3);
        Yp = polynomialFit1D(Y, true, 3, 3);
        Zp = polynomialFit1D(Z, true, 3, 3);
    }
    else{
        Xp = polynomialFit1D(X.head(pt->length()-1), false, 4, 3);
        Yp = polynomialFit1D(Y.head(pt->length()-1), false, 4, 3);
        Zp = polynomialFit1D(Z.head(pt->length()-1), false, 4, 3);
    }

    pt->setXcoordElement(cv::Point3d(Xp, Yp, Zp), it_seq_cnt);
    pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference, false);
}

void OptMethod::reestimateParticlePositionUsingLinearPredictor(Particle* pt, int it_seq_cnt, int k, bool verbal_flag){

    if(k==kMax_){
        pt->markAsLost();
        pt->deleteData(false); //DO NOT delete particle data
        pt->pop_back_Xcoord();
        pt->setit_seq_fin(it_seq_cnt-1);
    }
    else{
        //do linear fit
        auto pc_new = 2*pt->getPreviousXcoord(it_seq_cnt) - pt->getPreviousOfPreviousXcoord(it_seq_cnt);
        pt->setXcoordElement(pc_new, it_seq_cnt);
        pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference, false);

        if(verbal_flag){
            cout<<"do linear fit"<<endl;
            pt->printID();
            pt->printLastNV(pt->length());
            pt->printLastNXcoord(pt->length());
        }

        if(!this->qualifyDiscrepancyComparedToAnchorVelocity(pt->getCurrentV(it_seq_cnt), pt->getPreviousOfPreviousV(it_seq_cnt), true)){
            pt->markAsLost();
            pt->deleteData(false); //DO NOT delete particle data
            pt->pop_back_Xcoord();
            pt->setit_seq_fin(it_seq_cnt-1);
        }
    }
}

void OptMethod::computeParticleVelocity(int it_seq_cnt){

    for(auto& pa: this->particles_field()){
        if(pa->is_ontrack()){
            pa->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference);
        }
    }
}

VectorXd OptMethod::solve(const MatrixXd& hessian, const MatrixXd& rhs){

    VectorXd gamma;

    Eigen::LLT<MatrixXd> lltOfHessian(hessian);

    gamma = lltOfHessian.solve(rhs);

    return gamma;
}

void OptMethod::predictParticleUsingLinearPredictor(int it_seq_cnt,
                                                    std::vector<Particle_ptr>& particles_tgt){

    cout<<"#####Using Linear Predictor for track that has two positions..."<<endl<<endl;

    int num=0, delete_num=0;
    for(auto& pt: particles_tgt){
        if(pt->is_ontrack() && !pt->is_tracked() && pt->is_to_be_triangulated() && pt->length()==2){

            auto pc_kminus1 = pt->getPreviousXcoord(it_seq_cnt);
            auto pc_kminus2 = pt->getPreviousOfPreviousXcoord(it_seq_cnt);
            auto pc_cnt     = 2*pc_kminus1 - pc_kminus2;

            if(!checkParticleIsOutOfRange(pc_cnt, Xc_top_, Xc_bottom_))
                pt->push_backXcoordElement(pc_cnt, it_seq_cnt);
            else{
                if(pt->is_side()){
                    pt->markAsGhost();
                    pt->deleteData(true);
                    delete_num++;
                }
                else{
                    utils::maintainPositionInRange(pc_cnt, Xc_top_, Xc_bottom_);
                    pt->push_backXcoordElement(pc_cnt, it_seq_cnt);
                }
            }

            if(pt->is_ontrack()){
                pt->push_backEvElement(pt->Ev().back(), it_seq_cnt);
                pt->setit_seq_fin(it_seq_cnt);
                // pt->markAsSide(pt->ID());
                pt->markAsTriangulated();
                pt->markAsToBeTracked();
                num++;
            }
        }
    }
    cout<<endl
    <<"Number of particles that are predicted using linear predictor: "<<num<<endl
    <<"Delete: "<<delete_num<<" tracks that start from "<<it_seq_cnt-2<<endl;
}

bool OptMethod::qualifyDiscrepancyComparedToAnchorVelocity(const cv::Point3d& velo_tgt, const cv::Point3d& velo_anchor, bool verbal_flag){

    double large_velocity_norm = 2.*one_pixel_velo_norm_;
    double small_velocity_norm = 0.5*one_pixel_velo_norm_;

    bool in_range_flag = true;

    if(cv::norm(velo_anchor)<small_velocity_norm)
        in_range_flag = cv::norm(velo_tgt - velo_anchor)<small_velocity_norm;
    else if(cv::norm(velo_anchor)>=small_velocity_norm && cv::norm(velo_anchor)<one_pixel_velo_norm_)
        in_range_flag = cv::norm(velo_tgt - velo_anchor)<one_pixel_velo_norm_ && utils::angle(velo_tgt, velo_anchor) < CV_PI/2.;
    else if(cv::norm(velo_anchor)>=one_pixel_velo_norm_ && cv::norm(velo_anchor)<large_velocity_norm)
        in_range_flag = cv::norm(velo_tgt - velo_anchor)<one_pixel_velo_norm_ && utils::angle(velo_tgt, velo_anchor) < CV_PI/3.;
    else{
        auto vx_tgt_norm = velo_tgt.x;
        auto vx_anchor_norm = velo_tgt.x;

        auto vyz_tgt_norm = std::sqrt(velo_tgt.y*velo_tgt.y + velo_tgt.z*velo_tgt.z);
        auto vyz_anchor_norm = std::sqrt(velo_anchor.y*velo_anchor.y + velo_anchor.z*velo_anchor.z);

        if(velo_anchor.x > large_velocity_norm && vyz_anchor_norm < large_velocity_norm)
            in_range_flag = cv::norm(velo_tgt - velo_anchor)<large_velocity_norm && utils::angle(velo_tgt, velo_anchor) < CV_PI/6.;
        else if(velo_anchor.x < large_velocity_norm && vyz_anchor_norm > large_velocity_norm)
            in_range_flag = cv::norm(velo_tgt - velo_anchor)<large_velocity_norm && utils::angle(velo_tgt, velo_anchor) < CV_PI/2.;
        else if(velo_anchor.x < large_velocity_norm && vyz_anchor_norm < large_velocity_norm)
            in_range_flag = cv::norm(velo_tgt - velo_anchor)<large_velocity_norm && utils::angle(velo_tgt, velo_anchor) < CV_PI/2.;
        else
            in_range_flag = cv::norm(velo_tgt - velo_anchor)<large_velocity_norm && utils::angle(velo_tgt, velo_anchor) < CV_PI/6.;
    }

    if(!in_range_flag && verbal_flag)
        cout<<"in_range_flag "<<in_range_flag<<endl
            <<"velo_tgt      "<<velo_tgt<<endl
            <<"velo_anchor   "<<velo_anchor<<endl
            <<"norm_err  "<<cv::norm(velo_tgt - velo_anchor)
            <<" angle    "<<utils::angle(velo_tgt, velo_anchor)<<endl;

    return in_range_flag;
}

void OptMethod::projectParticlesToImageSpace(int it_seq_cnt, const std::vector<Particle_ptr>& particle_tgt, const std::vector<Camera_ptr>& cam){
#ifdef INTELTBB
    tbb::parallel_for( tbb::blocked_range<int>(0, particle_tgt.size()),
                        [&](const tbb::blocked_range<int>& r){ for(int p=r.begin(); p!=r.end(); ++p){
#else
    for(int p=0; p<particle_tgt.size(); ++p){
#endif
        if(particle_tgt[p]->is_ontrack() && this->checkIfParticleIsToBeOptimized(particle_tgt[p].get())){
            particle_tgt[p]->clearimgAndimgPos();
            particle_tgt[p]->projectToImageSpace(it_seq_cnt, cam);
        }
    }
#ifdef INTELTBB
    });    
#endif
}

void OptMethod::updateParticles(int it_seq_cnt, const std::vector<Particle_ptr>& particle_tgt, const std::vector<Camera_ptr>& cam, std::vector<double>& dummy){
#ifdef INTELTBB
    tbb::parallel_for( tbb::blocked_range<int>(0, particle_tgt.size()),
                [&](const tbb::blocked_range<int>& r){ for(int p=r.begin(); p!=r.end(); ++p){
#else
    for(int p=0; p<particle_tgt.size(); ++p){
#endif
        this->update(particle_tgt[p].get(), p, it_seq_cnt, cam, dummy[p]);
    }
#ifdef INTELTBB 
    });
#endif
}

}//namespace PTL