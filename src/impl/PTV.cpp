#include "../PTV.hpp"

namespace PTL{

PTV::PTV(){}

PTV::PTV(SubPixelMethod_Type subPixel_type, double initial_min_intensity_threshold, double filter_threshold, 
        double dilate_threshold, double dilate_surrounding_checker_threshold, double dilate_surrounding_difference_threshold,
        double search_threshold, double Wieneke_threshold, bool verbal_flag, double min_intensity_ratio, 
        const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, const cv::Point3d& dXc_px,
        const std::vector<int>& cam_seq,
        bool iterative_triangulation_flag, bool lsq_triangulation_flag):
        subPixel_type_(subPixel_type), initial_min_intensity_threshold_(initial_min_intensity_threshold), filter_threshold_(filter_threshold), 
        dilate_threshold_(dilate_threshold), dilate_surrounding_checker_threshold_(dilate_surrounding_checker_threshold), dilate_surrounding_difference_threshold_(dilate_surrounding_difference_threshold),
        search_threshold_(search_threshold), Wieneke_threshold_(Wieneke_threshold), verbal_flag_(verbal_flag), min_intensity_ratio_(min_intensity_ratio),
        Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), 
        dXc_px_(dXc_px), 
        idx1_(cam_seq[0]), idx2_(cam_seq[1]), idx3_(cam_seq[2]), idx4_(cam_seq[3]),
        iterative_triangulation_flag_(iterative_triangulation_flag), lsq_triangulation_flag_(lsq_triangulation_flag){
            min_intensity_threshold_=initial_min_intensity_threshold_;
            triangulation_tolerance_at_bounadary_=triangulation_tolerance_ratio_*std::min(std::min(Xc_top_.x, Xc_top_.y), Xc_top_.z);
            bad_point_=Xc_bottom_-Xc_top_;
        }

PTV::~PTV(){}

void PTV::readRecordsFromCamera(const std::vector<Camera_ptr>& cam){

    ImageHeight_ = cam[0]->Irec().size().height;
    ImageWidth_  = cam[0]->Irec().size().width;

    for(auto& ic : cam){
        auto imgRec=ic->Irec().clone();
        Irec_.push_back(imgRec);

        cv::Mat_<double> image_float;
        imgRec.convertTo(image_float, CV_64FC1); 
        Irec_float_.push_back(image_float);
    }
}

void PTV::getImagePyramids(void){

    int i=0;
    for(const auto& img : Irec_){
        cv::Mat out_img;
        cv::pyrDown(img, out_img);
        cv::imwrite("cam"+std::to_string(i)+"_pyrDown.png", out_img, utils::compression_params);
        Irec_pyrDown_.push_back(out_img);
        i++;
    }
}

void PTV::getMaxPixelCoords(void){
    this->useDilate();
    this->useFilter();
}

void PTV::getSubPixelCoords(double duplicate_threshold_px){

    int N = 1;// N : number of surrounding levels to be used for weighted average
    for(int k = 0; k < Irec_.size(); k++){          
        std::vector<cv::Point2d> CenterOfMass;
        for(auto& pd : PeakValues_.at(k)->points){
            cv::Point2d Wxc;
            if(subPixel_type_==SubPixelMethod_Type::WeightedAverage)
                Wxc = utils::applyWeightedAverage(pcl2cv(pd), Irec_float_[k], N);
            else if(subPixel_type_==SubPixelMethod_Type::Gaussian)
                Wxc = utils::applyGaussianFit(pcl2cv(pd), Irec_float_[k]);

            //duplicate check
            auto it=std::find_if(std::begin(CenterOfMass), std::end(CenterOfMass), [=](const cv::Point2d& Ct) { return cv::norm(Wxc-Ct)<duplicate_threshold_px; } );
            if(it == std::end(CenterOfMass)){
                CenterOfMass.push_back(Wxc);
            }
        }   
        Points_xy_.push_back(CenterOfMass);
    }   

    Points_xy_kdtree_.assign(Points_xy_.size(), pcl::KdTreeFLANN<pcl::PointXY>());

    for(int k = 0; k<Points_xy_.size(); k++){
        Points_xy_cloud_.push_back( pcl::make_shared<pcl::PointCloud<pcl::PointXY>>() );
        for(const auto& pt: Points_xy_[k]){ Points_xy_cloud_.back()->push_back(cv2pcl(pt)); }
        Points_xy_kdtree_[k].setInputCloud(Points_xy_cloud_.back());
    }
}

// dilate_surrounding_checker_threshold_ : checking surrounding is only occur if Pixel's intensity (obtained from dilation) is higher than dilate_surrounding_checker_threshold_ (decreasing this number will increase number of particles)
// SurroundingLevel : the number of surrounding pixel levels to be checked
// dilate_surrounding_difference_threshold_ : difference limit between the found pixel and the surrounding pixels (Decreasing this number will decrease the number of particles obtained from the code)

/* Checking surrounding for close overlapped particles by evaluating the difference 
    between the found Pixel and the surrounding if the difference is within a limit
    close pixel is considered as a maximum for another particle*/

void PTV::useDilate(void){    
    cv::Mat DilateKernel = (cv::Mat_<uchar>(3,3)<<1,1,1,1,0,1,1,1,1);
    int SurroundingLevel = 1;
    int np = 2*SurroundingLevel+1;
    Eigen::VectorXi R(np);
    for(int i = 0; i < np; i++)
        R(i) = -SurroundingLevel+i; 

    for(int k = 0; k < Irec_.size(); k++){

        PeakValues_.push_back( pcl::make_shared<pcl::PointCloud<pcl::PointXY>>() );
        cv::Mat                  image_dilate, Comp_image_dilate, Substracted;
        std::vector<cv::Point>   pixels_all, pixel_valid;

        cv::dilate(Irec_float_.at(k), image_dilate, DilateKernel);
        cv::subtract(Irec_float_.at(k), image_dilate, Substracted);
        cv::compare(Substracted, dilate_threshold_, Comp_image_dilate, cv::CMP_GT);
        cv::findNonZero(Comp_image_dilate, pixels_all); 

        if(!pixels_all.empty()){
            for(auto pt: pixels_all){
                if(Irec_float_.at(k)(pt) > min_intensity_threshold_){
                    pixel_valid.push_back(pt);
                    PeakValues_.back()->push_back( cv2pcl(pt) );
                }
            }
            cout<<"Using dilate Found "<<PeakValues_.back()->size()<<" particles for image "<<k<<" with intensity > "<<min_intensity_threshold_<<endl;

            for(auto& pt: pixel_valid){
                if(Irec_float_.at(k)(pt) > dilate_surrounding_checker_threshold_){
                    for(int i = 0; i < np; i++){
                        for(int j = 0; j < np; j++){
                            auto s_pt = pt + cv::Point(R(i), R(j));
                            double diff = abs( Irec_float_.at(k)(pt) - Irec_float_.at(k)(s_pt) );
                            if(diff < dilate_surrounding_difference_threshold_ && diff > 0){
                                PeakValues_.back()->push_back( cv2pcl(s_pt) );
                            }
                        }
                    }
                }
            }
            cout<<"Append "<<PeakValues_.back()->size()-pixel_valid.size()<<" particles by searching surroundings for image "<<k<<endl;
        }
    }
}
    
void PTV::useFilter(void){
            
    cv::Mat FilterKernel = (cv::Mat_<double>(3,3)<<0,-1,0,-1,5,-1,0,-1,0);

    for(int k = 0; k < Irec_.size(); k++){

        cv::Mat Comp_image_filter, image_filter;
        std::vector<cv::Point>  pixels_all;
        
        cv::filter2D(Irec_float_.at(k), image_filter, -1, FilterKernel);
        cv::compare(image_filter, filter_threshold_, Comp_image_filter, cv::CMP_GT);
        cv::findNonZero(Comp_image_filter, pixels_all);

        auto   PeakValues_kdtree = pcl::KdTreeFLANN<pcl::PointXY>();
        PeakValues_kdtree.setInputCloud(PeakValues_.at(k));

        if(!pixels_all.empty()){
            for(auto pt: pixels_all){
                auto pc = cv2pcl(pt);
                std::vector<int>   pointIdxRadiusSearch;
                std::vector<float> pointRadiusSquaredDistance;
                if( PeakValues_kdtree.radiusSearch(pc, 0.5, pointIdxRadiusSearch, pointRadiusSquaredDistance) == 0 ){
                    PeakValues_.at(k)->push_back(pc);
                }
            }         
        }
        cout<<"Found TOTAL "<<PeakValues_[k]->size()<<" particles for image "<<k<<" with intensity > "<<min_intensity_threshold_<<endl;
    }
}

void PTV::findParticleImagePeak(double duplicate_threshold_px){
    this->getMaxPixelCoords();
    this->getSubPixelCoords(duplicate_threshold_px);
}

bool PTV::findPointsOnEpipolarWithoutDrawingLine(Camera* cam1, Camera* cam2, 
                                                const cv::Point2d& Point_idx1_u,
                                                const std::vector<cv::Point2d>& Points_idx2_u,
                                                std::vector<cv::Point2d>& Points_idx2_u_pool,
                                                const std::vector<cv::Point2d>& Points_idx2_d,
                                                std::vector<cv::Point2d>& Points_idx2_d_pool){

    double search_threshold_cnt=search_threshold_;

    cv::Mat F12_f;
    cam1->F_betw_cams().at(cam2->i_cam()).convertTo(F12_f, CV_32F);

    std::vector<cv::Point2f> Point_idx1_f_u{cv::Point2f(float(Point_idx1_u.x), float(Point_idx1_u.y))};

    cv::Mat epiline12_vec;
    cv::computeCorrespondEpilines(Point_idx1_f_u, 1, F12_f, epiline12_vec);
    auto vec3f = epiline12_vec.at<cv::Vec3f>(0,0);
    cv::Mat_<double> epiline12_u = (cv::Mat_<double>(3,1) << double(vec3f(0)), double(vec3f(1)), double(vec3f(2))); 

    auto it=std::find_if(std::begin(Points_idx2_u), std::end(Points_idx2_u), [=](const cv::Point2d& P) {  return  abs(epiline12_u(0)*P.x + epiline12_u(1)*P.y + epiline12_u(2)) < search_threshold_cnt; } );
    while(it != std::end(Points_idx2_u)){
        size_t n = std::distance(std::begin(Points_idx2_u), it);
        Points_idx2_u_pool.push_back(Points_idx2_u[n]);
        if(nonlinear_triangulation_flag_) Points_idx2_d_pool.push_back(Points_idx2_d[n]);
        it=std::find_if(++it, std::end(Points_idx2_u), [=](const cv::Point2d& P) { return  abs(epiline12_u(0)*P.x + epiline12_u(1)*P.y + epiline12_u(2)) < search_threshold_cnt; } );
    }

    return (!Points_idx2_u_pool.empty());
}

bool PTV::doTwoPointsTriangulation(const std::vector<Camera_ptr>& cam, 
                                 const cv::Point2d& TargetPoint_idx1, const std::vector<cv::Point2d>& PointsNearEpipolarLine_idx2,
                                 int idx1, int idx2,
                                 std::vector<cv::Point3d>& TargetXworldCandidates, std::vector<cv::Point2d>& TriangulatePoints_idx2,
                                 const cv::Point2d& TargetPoint_idx1_distort, const std::vector<cv::Point2d>& PointsNearEpipolarLine_idx2_distort,
                                 std::vector<cv::Point2d>& TriangulatePoints_idx2_distort){
    
    for(int w = 0; w < PointsNearEpipolarLine_idx2.size(); w++){

        auto Xc = triang::triangulateTwoPoints(cam, idx1, idx2, TargetPoint_idx1, PointsNearEpipolarLine_idx2.at(w), iterative_triangulation_flag_);
        
        if(nonlinear_triangulation_flag_){
            Xc = triang::triangulateTwoPointsNonlinear(cam, idx1, idx2, Xc_top_, Xc_bottom_, TargetPoint_idx1_distort, PointsNearEpipolarLine_idx2_distort.at(w), Xc);
        }

        if( (Xc.x > Xc_bottom_.x - triangulation_tolerance_at_bounadary_ && Xc.x < Xc_top_.x + triangulation_tolerance_at_bounadary_) && 
            (Xc.y > Xc_bottom_.y - triangulation_tolerance_at_bounadary_ && Xc.y < Xc_top_.y + triangulation_tolerance_at_bounadary_) &&
            (Xc.z > Xc_bottom_.z - triangulation_tolerance_at_bounadary_ && Xc.z < Xc_top_.z + triangulation_tolerance_at_bounadary_) ){
            TargetXworldCandidates.push_back(Xc);
            TriangulatePoints_idx2.push_back( PointsNearEpipolarLine_idx2.at(w) );
            if(nonlinear_triangulation_flag_) TriangulatePoints_idx2_distort.push_back( PointsNearEpipolarLine_idx2_distort.at(w) );
        }
    }

    return (!TargetXworldCandidates.empty() && !TriangulatePoints_idx2.empty());
}

bool PTV::doLeastSquareTriangulation(const std::vector<Camera_ptr>& cam, int idx1, int idx2, int idx3, int idx4,
                                    const cv::Point2d& TargetPoint_idx1_u, 
                                    const std::vector<cv::Point2d>& TargetCorrespondent_idx2,
                                    const std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx3,
                                    const std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx4,
                                    const cv::Point2d& TargetPoint_idx1_d,
                                    const std::vector<cv::Point3d>& TargetWorld,
                                    std::vector<std::vector<cv::Point3d>>& TargetWorld_lsq){
    cv::Point3d Xc;

    for(int i2=0; i2<TargetCorrespondent_idx2.size(); i2++){
        std::vector<cv::Point3d> TargetWorld_i2;

        if(!TargetCorrespondent_idx3.at(i2).empty() && !TargetCorrespondent_idx4.at(i2).empty()){
            for(int i3 = 0; i3 < TargetCorrespondent_idx3.at(i2).size(); i3++){
                for(int i4 = 0; i4 < TargetCorrespondent_idx4.at(i2).size(); i4++){
                    if(nonlinear_lsq_triangulation_flag_)
                        Xc=triang::triangulateThreeOrFourPointsNonlinear(cam, idx1, idx2, idx3, idx4,
                                                                Xc_top_, Xc_bottom_,
                                                                TargetPoint_idx1_d,
                                                                TargetCorrespondent_idx2[i2],
                                                                TargetCorrespondent_idx3[i2][i3],
                                                                TargetWorld[i2],
                                                                TargetCorrespondent_idx4[i2][i4] );
                    else
                        Xc=triang::triangulateThreeOrFourPoints(cam, idx1, idx2, idx3, idx4,
                                                                TargetPoint_idx1_u,
                                                                TargetCorrespondent_idx2[i2],
                                                                TargetCorrespondent_idx3[i2][i3],
                                                                TargetCorrespondent_idx4[i2][i4] );
                    TargetWorld_i2.push_back(Xc);
                }
            }
        }
        else if(!TargetCorrespondent_idx3.at(i2).empty()){
            for(int i3 = 0; i3 < TargetCorrespondent_idx3.at(i2).size(); i3++){
                if(nonlinear_lsq_triangulation_flag_)
                    Xc=triang::triangulateThreeOrFourPointsNonlinear(cam, idx1, idx2, idx3, -1, 
                                                            Xc_top_, Xc_bottom_,
                                                            TargetPoint_idx1_d,
                                                            TargetCorrespondent_idx2[i2],
                                                            TargetCorrespondent_idx3[i2][i3],
                                                            TargetWorld[i2]);
                else
                    Xc=triang::triangulateThreeOrFourPoints(cam, idx1, idx2, idx3, -1, 
                                                            TargetPoint_idx1_u,
                                                            TargetCorrespondent_idx2[i2],
                                                            TargetCorrespondent_idx3[i2][i3]);
                TargetWorld_i2.push_back(Xc);
            }
        }
        else if(!TargetCorrespondent_idx4.at(i2).empty()){
            for(int i4 = 0; i4 < TargetCorrespondent_idx4.at(i2).size(); i4++){
                if(nonlinear_lsq_triangulation_flag_)
                    Xc=triang::triangulateThreeOrFourPointsNonlinear(cam, idx1, idx2, idx4, -1,
                                                            Xc_top_, Xc_bottom_,
                                                            TargetPoint_idx1_d,
                                                            TargetCorrespondent_idx2[i2],
                                                            TargetCorrespondent_idx3[i2][i4],
                                                            TargetWorld[i2]);
                else
                    Xc=triang::triangulateThreeOrFourPoints(cam, idx1, idx2, idx4, -1, 
                                                            TargetPoint_idx1_u,
                                                            TargetCorrespondent_idx2[i2],
                                                            TargetCorrespondent_idx4[i2][i4] );
                TargetWorld_i2.push_back(Xc);
            }
        }

        //duplicate check
        for(auto it=TargetWorld_i2.begin(); it!=TargetWorld_i2.end(); ++it){
            if(cv::norm(*it-bad_point_)>1e-6){
                std::replace_if(it+1, TargetWorld_i2.end(), 
                                [=](const cv::Point3d& P) { return (cv::norm(*it-P)<duplicate_ratio_doLeastSquareTriangulation_*cv::norm(dXc_px_)); }, 
                                bad_point_);
            }
        }
        //duplicate check

        TargetWorld_lsq.push_back(TargetWorld_i2);
    }

    return (!TargetWorld_lsq.at(0).empty());
}

bool PTV::WienekeMethod(const std::vector<cv::Point3d>& TargetXWorldCandidates, const std::vector<cv::Point2d>& TriangulatePoints_idx2, 
                        const std::vector<Camera_ptr>& cam, int idx3, int idx4, bool reduced_IPR_flag, 
                        std::vector<cv::Point2d>& TargetCorrespondent_idx2, 
                        std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx3, 
                        std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx4,
                        std::vector<cv::Point3d>& TargetXw, 
                        const std::vector<cv::Point2d>& TriangulatePoints_idx2_distort){ 

    std::vector<cv::Point2d>  Candidate_idx3, Candidate_idx4;

    int r=0;
    for(auto& Xw: TargetXWorldCandidates){
        Candidate_idx3=this->CorrespondentSearcher(idx3, cam[idx3]->mappingFunction(Xw));
        Candidate_idx4=this->CorrespondentSearcher(idx4, cam[idx4]->mappingFunction(Xw));

        auto Xw_Found_flag = (reduced_IPR_flag ? (!Candidate_idx3.empty() || !Candidate_idx4.empty()) :
                                                 (!Candidate_idx3.empty() && !Candidate_idx4.empty()) );
        
        if(Xw_Found_flag){
            TargetCorrespondent_idx2.push_back(nonlinear_triangulation_flag_ ? TriangulatePoints_idx2_distort[r] : TriangulatePoints_idx2[r]);
            TargetCorrespondent_idx3.push_back(Candidate_idx3);
            TargetCorrespondent_idx4.push_back(Candidate_idx4);
            TargetXw.push_back(Xw);
        }
        r++;
    }

    bool CorrespondentFound_flag = (reduced_IPR_flag ? (!TargetCorrespondent_idx3.empty() || !TargetCorrespondent_idx4.empty()) :
                                                       (!TargetCorrespondent_idx3.empty() && !TargetCorrespondent_idx4.empty()) );

    return CorrespondentFound_flag;
}

std::vector<cv::Point2d> PTV::CorrespondentSearcher(int idx3, const cv::Point2d& TargetPoint_idx3){

    std::vector<cv::Point2d> Candidate_idx3;

    std::vector<int>   pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;

    Points_xy_kdtree_[idx3].radiusSearch(cv2pcl(TargetPoint_idx3), Wieneke_threshold_, pointIdxRadiusSearch, pointRadiusSquaredDistance);
    for(std::size_t i = 0; i < pointIdxRadiusSearch.size(); ++i){
        Candidate_idx3.push_back( pcl2cv(Points_xy_cloud_[idx3]->points[ pointIdxRadiusSearch[i] ]) );
    }

    return Candidate_idx3;
}

void PTV::packResultsForCurrentParticle(const cv::Point2d& TargetPoint_idx1,
                                        const std::vector<cv::Point2d>& TargetCorrespondent_idx2,
                                        const std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx3,
                                        const std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx4,
                                        const std::vector<cv::Point3d>& TargetWorld){

    for(int i2 = 0; i2 < TargetCorrespondent_idx2.size(); i2++){
        pcl::PointXYZI worldPoint_i2 = cv2pcl(TargetWorld.at(i2), 0);
        
        if(!TargetCorrespondent_idx3.at(i2).empty() && !TargetCorrespondent_idx4.at(i2).empty()){
            for(int i3 = 0; i3 < TargetCorrespondent_idx3.at(i2).size(); i3++){
                for(int i4 = 0; i4 < TargetCorrespondent_idx4.at(i2).size(); i4++){
                    if(cv::norm(TargetWorld.at(i2)-bad_point_)>1e-6){
                        CorrespondentImagePoints_[0].push_back(TargetPoint_idx1);
                        CorrespondentImagePoints_[1].push_back(TargetCorrespondent_idx2.at(i2));
                        CorrespondentImagePoints_[2].push_back(TargetCorrespondent_idx3.at(i2).at(i3));
                        CorrespondentImagePoints_[3].push_back(TargetCorrespondent_idx4.at(i2).at(i4));
                        CorrespondentWorldPoints_.push_back(worldPoint_i2);
                    }
                }   
            }   
        }
        else{
            if(!TargetCorrespondent_idx3.at(i2).empty()){
                for(int i3 = 0; i3 < TargetCorrespondent_idx3.at(i2).size(); i3++){
                    if(cv::norm(TargetWorld.at(i2)-bad_point_)>1e-6){
                        CorrespondentImagePoints_[0].push_back(TargetPoint_idx1);
                        CorrespondentImagePoints_[1].push_back(TargetCorrespondent_idx2.at(i2));
                        CorrespondentImagePoints_[2].push_back(TargetCorrespondent_idx3.at(i2).at(i3));
                        CorrespondentImagePoints_[3].push_back(cv::Point2d(-1, -1));
                        CorrespondentWorldPoints_.push_back(worldPoint_i2);
                    }
                }   
            }
            else if(!TargetCorrespondent_idx4.at(i2).empty()){
                for(int i4 = 0; i4 < TargetCorrespondent_idx4.at(i2).size(); i4++){
                    if(cv::norm(TargetWorld.at(i2)-bad_point_)>1e-6){
                        CorrespondentImagePoints_[0].push_back(TargetPoint_idx1);
                        CorrespondentImagePoints_[1].push_back(TargetCorrespondent_idx2.at(i2));
                        CorrespondentImagePoints_[2].push_back(cv::Point2d(-1, -1));
                        CorrespondentImagePoints_[3].push_back(TargetCorrespondent_idx4.at(i2).at(i4));
                        CorrespondentWorldPoints_.push_back(worldPoint_i2);
                    }
                }   
            }
        }
    }
}

void PTV::packResultsForCurrentParticle(const cv::Point2d& TargetPoint_idx1,
                                        const std::vector<cv::Point2d>& TargetCorrespondent_idx2,
                                        const std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx3,
                                        const std::vector<std::vector<cv::Point2d>>& TargetCorrespondent_idx4,
                                        const std::vector<std::vector<cv::Point3d>>& TargetWorld_lsq){

    for(int i2 = 0; i2 < TargetCorrespondent_idx2.size(); i2++){
        if(!TargetCorrespondent_idx3.at(i2).empty() && !TargetCorrespondent_idx4.at(i2).empty()){
            for(int i3 = 0; i3 < TargetCorrespondent_idx3.at(i2).size(); i3++){
                for(int i4 = 0; i4 < TargetCorrespondent_idx4.at(i2).size(); i4++){
                    if(cv::norm(TargetWorld_lsq.at(i2).at(i3*TargetCorrespondent_idx4.at(i2).size()+i4)-bad_point_)>1e-6){
                        CorrespondentImagePoints_[0].push_back(TargetPoint_idx1);
                        CorrespondentImagePoints_[1].push_back(TargetCorrespondent_idx2.at(i2));
                        CorrespondentImagePoints_[2].push_back(TargetCorrespondent_idx3.at(i2).at(i3));
                        CorrespondentImagePoints_[3].push_back(TargetCorrespondent_idx4.at(i2).at(i4));
                        CorrespondentWorldPoints_.push_back( cv2pcl(TargetWorld_lsq.at(i2).at(i3*TargetCorrespondent_idx4.at(i2).size()+i4), 0) );
                    }
                }   
            }   
        }
        else{
            if(!TargetCorrespondent_idx3.at(i2).empty()){
                for(int i3 = 0; i3 < TargetCorrespondent_idx3.at(i2).size(); i3++){
                    if(cv::norm(TargetWorld_lsq.at(i2).at(i3)-bad_point_)>1e-6){
                        CorrespondentImagePoints_[0].push_back(TargetPoint_idx1);
                        CorrespondentImagePoints_[1].push_back(TargetCorrespondent_idx2.at(i2));
                        CorrespondentImagePoints_[2].push_back(TargetCorrespondent_idx3.at(i2).at(i3));
                        CorrespondentImagePoints_[3].push_back(cv::Point2d(NAN, NAN));
                        CorrespondentWorldPoints_.push_back( cv2pcl(TargetWorld_lsq.at(i2).at(i3), 0) );
                    }
                }   
            }
            else if(!TargetCorrespondent_idx4.at(i2).empty()){
                for(int i4 = 0; i4 < TargetCorrespondent_idx4.at(i2).size(); i4++){
                    if(cv::norm(TargetWorld_lsq.at(i2).at(i4)-bad_point_)>1e-6){
                        CorrespondentImagePoints_[0].push_back(TargetPoint_idx1);
                        CorrespondentImagePoints_[1].push_back(TargetCorrespondent_idx2.at(i2));
                        CorrespondentImagePoints_[2].push_back(cv::Point2d(NAN, NAN));
                        CorrespondentImagePoints_[3].push_back(TargetCorrespondent_idx4.at(i2).at(i4));
                        CorrespondentWorldPoints_.push_back( cv2pcl(TargetWorld_lsq.at(i2).at(i4), 0) );
                    }
                }   
            }
        }
    }
}

void PTV::writeIPRParticlesToFile(int it_seq, const std::string& resdir, 
                                    const std::string& opt_method_flag){

    std::string name=str(boost::format("%1%/IPR_%3%_it%2$03d.npz") % resdir % it_seq % opt_method_flag );
    cout<<"IPR file saved to"<<endl<<name<<endl;
    utils::saveFrameDataNPZ(it_seq, name, particles_bkg_);
}

void PTV::writeTriangulatedParticlesToFile(int it_seq, const std::string& resdir){
    
    std::string name=str(boost::format("%1%/Triangulation_it%2$03d.npz") % resdir % it_seq );
    cout<<"PTV file saved to"<<endl<<name<<endl;

    std::vector<double> xc1_v, yc1_v, xc2_v, yc2_v, xc3_v, yc3_v, xc4_v, yc4_v, X_v, Y_v, Z_v;

    for(int p = 0; p < CorrespondentWorldPoints_.size(); p++){   
        xc1_v.push_back(CorrespondentImagePoints_[idx1_].at(p).x);
        yc1_v.push_back(CorrespondentImagePoints_[idx1_].at(p).y);
        xc2_v.push_back(CorrespondentImagePoints_[idx2_].at(p).x);
        yc2_v.push_back(CorrespondentImagePoints_[idx2_].at(p).y);
        xc3_v.push_back(CorrespondentImagePoints_[idx3_].at(p).x);
        yc3_v.push_back(CorrespondentImagePoints_[idx3_].at(p).y);
        xc4_v.push_back(CorrespondentImagePoints_[idx4_].at(p).x);
        yc4_v.push_back(CorrespondentImagePoints_[idx4_].at(p).y);
        X_v.push_back(CorrespondentWorldPoints_.at(p).x);
        Y_v.push_back(CorrespondentWorldPoints_.at(p).y);
        Z_v.push_back(CorrespondentWorldPoints_.at(p).z);
    }

    auto NX = static_cast<long unsigned int>(CorrespondentWorldPoints_.size());
    cnpy::npz_save(name, "xc1", &xc1_v[0], {NX,1}, "w");
    cnpy::npz_save(name, "yc1", &yc1_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "xc2", &xc2_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "yc2", &yc2_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "xc3", &xc3_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "yc3", &yc3_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "xc4", &xc4_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "yc4", &yc4_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "X", &X_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "Y", &Y_v[0], {NX,1}, "a");
    cnpy::npz_save(name, "Z", &Z_v[0], {NX,1}, "a");
}

std::vector<pcl::PointXYZI> PTV::removeDuplicates(void){

    std::vector<pcl::PointXYZI> CorrespondentWorldPoints_l;

    for(int i=0; i<CorrespondentWorldPoints_.size(); ++i){
        CorrespondentWorldPoints_l.push_back(CorrespondentWorldPoints_[i]);
    }

    int nb_removed=0;
    for(int i=0; i<CorrespondentWorldPoints_l.size()-nb_removed; ++i){
        for(int j=i+1; j<CorrespondentWorldPoints_l.size()-nb_removed; ++j){
            if(cv::norm(pcl2cv(CorrespondentWorldPoints_l[i])-pcl2cv(CorrespondentWorldPoints_l[j]))<cv::norm(dXc_px_)){
                std::iter_swap(CorrespondentWorldPoints_l.begin()+j, CorrespondentWorldPoints_l.end()-nb_removed-1);
                nb_removed+=1;
                --j;
                break;
            }
        }
    }
    CorrespondentWorldPoints_l.erase(CorrespondentWorldPoints_l.end()-nb_removed, CorrespondentWorldPoints_l.end());

    return CorrespondentWorldPoints_l;
}

void PTV::setFrameField(pcl::PointCloud<pcl::PointXYZI>* particles_in_one_frame_cloud){

    auto particles_frame_app_cloud = this->addParticlesFromPoolOctree(*particles_in_one_frame_cloud);
    int num_dup=particles_bkg_.size()-particles_frame_app_cloud.size();
    cout<<"Found duplicates "<<num_dup<<" discard!!!"<<endl;

    particles_in_one_frame_cloud->points.insert(particles_in_one_frame_cloud->points.end(),
                                                particles_frame_app_cloud.points.begin(),
                                                particles_frame_app_cloud.points.end());

    cout<<"particles_frame_app_cloud.size()  "<<particles_frame_app_cloud.size()<<endl;
    cout<<"particles_in_one_frame->size()    "<<particles_in_one_frame_cloud->size()<<endl;
}

void PTV::setTriangulatedParticleField(int it_seq, bool duplicate_check_flag, bool reduced_flag){

    int append_index=0;
    if(!particles_bkg_.empty()){
        std::vector<int> id_vec;
        for(const auto& pt: particles_bkg_) id_vec.push_back(pt->ID());
        auto id_max=*std::max_element(id_vec.begin(), id_vec.end());
        append_index = id_max + 1;
    } 

    int num_dup=0;
    if(duplicate_check_flag){
        auto CorrespondentWorldPoints_l = this->removeDuplicates();
        auto particles_app = this->addParticlesFromPoolOctree(it_seq, CorrespondentWorldPoints_l);
        num_dup = CorrespondentWorldPoints_.size()-particles_app.size();

        int counter=0;
        for(const auto& pa: particles_app){
            particles_bkg_.push_back( std::make_unique<Particle>( *pa ) );
            particles_bkg_.back()->markAsTriangulated();
            particles_bkg_.back()->setID(counter+append_index);
            if(reduced_flag) particles_bkg_.back()->markAsNew();
            counter++;
        }
        particles_app.clear();
    }
    else{
        int p=0;
        for(const auto& pt: CorrespondentWorldPoints_){
            particles_bkg_.push_back( std::make_unique<Particle>( p+append_index, it_seq, it_seq, std::vector<cv::Point3d>{ pcl2cv(pt) }, std::vector<double>{pt.intensity} ) );
            particles_bkg_.back()->markAsTriangulated();
            if(reduced_flag) particles_bkg_.back()->markAsNew();
            p++;
        }
    }
    cout<<"Found duplicates "<<num_dup<<" discard!!!"<<endl;
}

std::vector<Particle_ptr>
PTV::addParticlesFromPoolOctree(int it_seq, const std::vector<pcl::PointXYZI>& CorrespondentWorldPoints_l,
                                double resolution){

    auto particles_cloud_4d  = pcl::make_shared<pcl::PointCloud<pcl::PointXYZI>>();
    for(const auto& pc: particles_bkg_)
        if(pc->is_ontrack())
            particles_cloud_4d->push_back( cv2pcl(pc->Xcoord()[0], 0) );

    auto particles_octree_4d = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(resolution);
    particles_octree_4d.setInputCloud(particles_cloud_4d);
    particles_octree_4d.addPointsFromInputCloud();

    std::vector<Particle_ptr> particles_app;

    for(int p=0; p<CorrespondentWorldPoints_l.size(); p++){
        auto searchPoint = CorrespondentWorldPoints_l.at(p);

        std::vector<int>   pointIdxRadiusSearch;
        std::vector<float> pointRadiusSquaredDistance;
        float threshold = static_cast<float>(duplicate_ratio_setTriangulatedParticleField_*cv::norm(dXc_px_));

        if( particles_octree_4d.radiusSearch(searchPoint, threshold, pointIdxRadiusSearch, pointRadiusSquaredDistance) == 0 ){
            particles_app.push_back( std::make_unique<Particle>( p, it_seq, it_seq, std::vector<cv::Point3d>{pcl2cv(searchPoint)}, std::vector<double>{searchPoint.intensity} ) );
        }
    }

    return particles_app;
}

pcl::PointCloud<pcl::PointXYZI>
PTV::addParticlesFromPoolOctree(const pcl::PointCloud<pcl::PointXYZI>& particles_in_one_frame_cloud,
                                double resolution){

    auto particles_octree_4d = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(resolution);
    particles_octree_4d.setInputCloud( pcl::make_shared<pcl::PointCloud<pcl::PointXYZI>>(particles_in_one_frame_cloud) );
    particles_octree_4d.addPointsFromInputCloud();    

    auto particles_frame_app_cloud = pcl::PointCloud<pcl::PointXYZI>();

    for(const auto& pb: particles_bkg_){
        if(pb->is_ontrack()){
            auto searchPoint = cv2pcl(pb->Xcoord()[0], pb->Ev()[0]);

            std::vector<int>   pointIdxRadiusSearch;
            std::vector<float> pointRadiusSquaredDistance;

            if( particles_octree_4d.radiusSearch(searchPoint, duplicate_ratio_setTriangulatedParticleField_*cv::norm(dXc_px_), pointIdxRadiusSearch, pointRadiusSquaredDistance) == 0 ){
                particles_frame_app_cloud.push_back( searchPoint );
            }
        }
    }

    return particles_frame_app_cloud;
}

bool PTV::stereoMatching(const std::vector<Camera_ptr>& cam, bool reduced_IPR_flag, bool pos_only_flag){      

    std::vector<cv::Point2d> Points_idx1_u,  Points_idx2_u;
    if(cv::norm(cam[idx1_]->distCoeffs())<1e-3 && cv::norm(cam[idx2_]->distCoeffs())<1e-3){
        nonlinear_triangulation_flag_=false;
        Points_idx1_u = Points_xy_[idx1_];
        Points_idx2_u = Points_xy_[idx2_];
    }
    else{
        nonlinear_triangulation_flag_=true;
        if(lsq_triangulation_flag_ && nonlinear_triangulation_flag_) nonlinear_lsq_triangulation_flag_=true;
        cv::undistortPoints(Points_xy_[idx1_], Points_idx1_u, cam[idx1_]->cameraMatrix(), cam[idx1_]->distCoeffs(), cv::noArray(), cam[idx1_]->cameraMatrix()); 
        cv::undistortPoints(Points_xy_[idx2_], Points_idx2_u, cam[idx2_]->cameraMatrix(), cam[idx2_]->distCoeffs(), cv::noArray(), cam[idx2_]->cameraMatrix()); 
    }

    if(!CorrespondentImagePoints_.empty()) CorrespondentImagePoints_.clear();
    CorrespondentImagePoints_.assign(cam.size(), 
#ifdef INTELTBB
        tbb::concurrent_vector<cv::Point2d>()
#else
        std::vector<cv::Point2d>()
#endif
    );

#ifdef INTELTBB 
    tbb::parallel_for( tbb::blocked_range<int>(0, Points_xy_[idx1_].size()),
                [&](const tbb::blocked_range<int>& r)
                {
                    for(int j=r.begin(); j!=r.end(); ++j){
#else
                    for(int j=0; j<Points_xy_[idx1_].size(); j++){
#endif
                        std::vector<cv::Point2d>                EpiPoints_idx2, EpiPoints_d_idx2, TriangulatePoints_idx2, TriangulatePoints_d_idx2, TargetCorrespondent_idx2;
                        std::vector<std::vector<cv::Point2d>>   TargetCorrespondent_idx3, TargetCorrespondent_idx4;
                        std::vector<cv::Point3d>                TargetXworldCandidates, TargetWorld;
                        std::vector<std::vector<cv::Point3d>>   TargetWorld_lsq;
// #ifndef INTELTBB 
//                         if(verbal_flag_)    cout<<"#########################################"<<endl
//                                                 <<"For "<<j<<"th particle in image idx1 with coords "<<Points_idx1_u[j]<<endl;
// #endif

                        if(this->findPointsOnEpipolarWithoutDrawingLine(cam.at(idx1_).get(), cam.at(idx2_).get(), 
                                                                                                Points_idx1_u[j], Points_idx2_u, EpiPoints_idx2, 
                                                                                                Points_xy_[idx2_], EpiPoints_d_idx2)){

// #ifndef INTELTBB 
//                             if(verbal_flag_)   cout<<"Found "<<EpiPoints_idx2.size()<<" number of points on epipolar line of image idx2"<<endl;
// #endif              

                            if(this->doTwoPointsTriangulation(cam, Points_idx1_u[j], EpiPoints_idx2, idx1_, idx2_,
                                                                                    TargetXworldCandidates, TriangulatePoints_idx2,
                                                                                    Points_xy_[idx1_][j], EpiPoints_d_idx2, TriangulatePoints_d_idx2)){
// #ifndef INTELTBB 
//                                 if(verbal_flag_)  cout<<"After triangulation, found "<<TargetXworldCandidates.size()<<" points of world target"<<endl;
// #endif

                                if(this->WienekeMethod(TargetXworldCandidates, TriangulatePoints_idx2, cam, idx3_, idx4_, reduced_IPR_flag,
                                                                            TargetCorrespondent_idx2, TargetCorrespondent_idx3, TargetCorrespondent_idx4,
                                                                            TargetWorld, TriangulatePoints_d_idx2)){
// #ifndef INTELTBB 
//                                     if(verbal_flag_)  cout<<"After correspondence search, found "<<TargetCorrespondent_idx2.size()<<" points on image idx2"<<endl;
// #endif
                                    if(lsq_triangulation_flag_ && this->doLeastSquareTriangulation(cam, idx1_, idx2_, idx3_, idx4_,
                                                                                                    Points_idx1_u[j], TargetCorrespondent_idx2,
                                                                                                    TargetCorrespondent_idx3, TargetCorrespondent_idx4,
                                                                                                    Points_xy_[idx1_][j], TargetWorld, TargetWorld_lsq)){

                                        this->packResultsForCurrentParticle(Points_xy_[idx1_].at(j), TargetCorrespondent_idx2, TargetCorrespondent_idx3, TargetCorrespondent_idx4, TargetWorld_lsq);

                                    }
                                    else{
                                        this->packResultsForCurrentParticle(Points_xy_[idx1_].at(j), TargetCorrespondent_idx2, TargetCorrespondent_idx3, TargetCorrespondent_idx4, TargetWorld);
                                    }
                                }
                                // else
// #ifndef INTELTBB
//                                     if(verbal_flag_) std::cerr<<"Could not find the correspondence particle on the idx3 and the idx4 image"<<endl;
// #endif
                            }
                            // else
// #ifndef INTELTBB                            
//                                 if(verbal_flag_) std::cerr<<"The world point from the triangulation between the first and the second camera was not found"<<endl;
// #endif
                        }
                        // else
// #ifndef INTELTBB
//                             if(verbal_flag_) std::cerr<<"EpipolarPoints vector is empty, increase the thickness of the epipolar line or the threshold value"<<endl;
// #endif    
                    }
#ifdef INTELTBB
                });
#endif

    cout<<"######## Found "<<CorrespondentWorldPoints_.size()<<" particles' correspondence on 4 images ########"<<endl;
    if(verbal_flag_){
        cout<<"CorrespondentImagePoints_.size() for camera "<<idx1_<<" is "<<CorrespondentImagePoints_[0].size()<<endl
            <<"CorrespondentImagePoints_.size() for camera "<<idx2_<<" is "<<CorrespondentImagePoints_[1].size()<<endl
            <<"CorrespondentImagePoints_.size() for camera "<<idx3_<<" is "<<CorrespondentImagePoints_[2].size()<<endl
            <<"CorrespondentImagePoints_.size() for camera "<<idx4_<<" is "<<CorrespondentImagePoints_[3].size()<<endl
            <<"CorrespondentWorldPoints_.size()              is "<<CorrespondentWorldPoints_.size()<<endl;
    }

    if(!pos_only_flag){
        cout<<"######## Calibrate particle intensity ########"<<endl;
        for(int j=0; j<CorrespondentWorldPoints_.size(); j++){
            CorrespondentWorldPoints_[j].intensity = PTL::calibrateSingleParticleIntensity(cam, 
                                                            pcl2cv(CorrespondentWorldPoints_[j]),
                                                            CorrespondentImagePoints_[0][j],
                                                            CorrespondentImagePoints_[1][j],
                                                            CorrespondentImagePoints_[2][j],
                                                            CorrespondentImagePoints_[3][j],
                                                            Irec_float_, ImageWidth_, ImageHeight_,
                                                            std::vector<int>{idx1_, idx2_, idx3_, idx4_});
        }
    }
    
    return !(CorrespondentWorldPoints_.size()==0);   
}

double calibrateSingleParticleIntensity(const std::vector<Camera_ptr>& cam, const cv::Point3d& Xc, 
                                        const cv::Point2d& xc1, const cv::Point2d& xc2,
                                        const cv::Point2d& xc3, const cv::Point2d& xc4,
                                        const std::vector<cv::Mat_<double>>& Irec_float,
                                        double ImageWidth, double ImageHeight,
                                        const std::vector<int> cam_seq){

    std::vector<cv::Point2d> xc_vec{xc1, xc2, xc3, xc4};
    double      E_rhs=0., E_lhs=0.;

    for(auto i_cam=cam_seq.begin(); i_cam<cam_seq.end(); ++i_cam){

        int i_seq = std::distance(std::begin(cam_seq), i_cam);

        auto xc = xc_vec[i_seq];

        if(xc.x>0 && xc.x<ImageWidth && xc.y>0 && xc.y<ImageHeight){
            cv::Rect pos;
            cv::Mat  Imgi_smpl;
            double windowlength = utils::OTFCore(cam[*i_cam]->getPSFelement(Xc), xc, 1., Imgi_smpl, pos);

            cv::Rect pos_img, pos_BigCanvas;

            utils::getPositionAndSizeOfOverlapedAreaOfOneLocalPatchAndIres(pos, windowlength, static_cast<int>(ImageWidth), static_cast<int>(ImageHeight),
                                                                           pos_img, pos_BigCanvas);

            cv::Mat  Imgi_smpl_float;
            Imgi_smpl(pos_img).convertTo(Imgi_smpl_float, CV_64FC1);

            cv::Mat  Imgi_real = Irec_float[*i_cam](pos_BigCanvas).clone();
            
            cv::Mat  Imgi_real_nonngtv = cv::max(Imgi_real, 0);

            E_rhs += Imgi_smpl_float.dot(Imgi_real_nonngtv);
            E_lhs += Imgi_smpl_float.dot(Imgi_smpl_float);

            if(Imgi_real.size()!=Imgi_smpl_float.size() || E_lhs==0){
                cout<<"Xc "<<Xc<<endl;
                cout<<"xc "<<xc<<endl;
                cout<<"pos_img "<<pos_img<<endl
                    <<"Imgi_smpl_float"<<endl<<Imgi_smpl_float<<endl
                    <<"pos_BigCanvas "<<pos_BigCanvas<<endl
                    <<"Imgi_real"<<endl<<Imgi_real<<endl;
                std::abort();
            }
        }
    }

    return E_rhs/E_lhs;
} 

void recalibrateParticlesIntensities(const std::vector<Particle_ptr>& particles_bkg, int it_seq,
                                    const std::vector<Camera_ptr>& cam, 
                                    const std::vector<cv::Mat_<double>>& Irec_float,
                                    double ImageWidth, double ImageHeight,
                                    const std::vector<int>& cam_seq){

    cout<<"Recalibrate particle intensity at time level "<<it_seq<<" using new OTF"<<endl;
    for(const auto& pb: particles_bkg){
        pb->checkIfParticleAppearsOnSnapshot(it_seq);
        if(pb->is_ontrack()){
            auto Xc = pb->getCurrentXcoord(it_seq);

            auto E = PTL::calibrateSingleParticleIntensity(cam, Xc, 
                                                        cam[cam_seq[0]]->mappingFunction(Xc),
                                                        cam[cam_seq[1]]->mappingFunction(Xc),
                                                        cam[cam_seq[2]]->mappingFunction(Xc),
                                                        cam[cam_seq[3]]->mappingFunction(Xc),
                                                        Irec_float, ImageWidth, ImageHeight, cam_seq);
                        
            pb->setEvElement(E, it_seq);
        }
    }
}

}//namespace PTL