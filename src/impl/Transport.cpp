#include "../Transport.hpp"

namespace PTL{

Transport::Transport(){}

Transport::~Transport(){}

/// used in initexe

Transport::Transport(VelocityInterpolation_Type velocity_to_scatter_interp_type, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, const cv::Point3i& nXc,
					 TimeScheme_Type time_scheme, double dt, double dtObs)
					: velocity_to_scatter_interp_type_(velocity_to_scatter_interp_type), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), nXc_(nXc), 
						time_scheme_(time_scheme), dt_(dt), dtObs_(dtObs){}

void Transport::predictParticlePositionField(const std::vector<Particle_ptr>& particles_ana_in, 
											int it_seq_cnt, bool repredict_flag) const{

	for(const auto& p_in : particles_ana_in){
	    if(p_in->is_ontrack() && !p_in->Xcoord().empty()){
			auto pc = p_in->getPreviousXcoord(it_seq_cnt);

			for(int i=0; i<static_cast<int>(std::round(dtObs_/dt_)); i++){
				auto pc_n = this->integrateDtUsingEulerianVelocityFields(pc);
				if( std::abs(pc_n.x-pc.x) > (Xc_top_.x-Xc_bottom_.x)/2 ||
					std::abs(pc_n.y-pc.y) > (Xc_top_.y-Xc_bottom_.y)/2 ||
					std::abs(pc_n.z-pc.z) > (Xc_top_.z-Xc_bottom_.z)/2 )
					break;
				else
					pc = pc_n;
			}
            utils::maintainPositionInRange(pc, Xc_top_, Xc_bottom_);

			if(repredict_flag) p_in->setXcoordElement(pc, it_seq_cnt);
			else               p_in->push_backXcoordElement(pc, it_seq_cnt);
	 		p_in->setit_seq_fin(it_seq_cnt);
		}
	}
}

cv::Point3d Transport::interpolateVelocityFieldOnParticleCoordinates(const cv::Point3d& pc) const{

	cv::Point3d velo;
	switch (velocity_to_scatter_interp_type_){
		case Transport::VelocityInterpolation_Type::TriLinear:
			velo = trilinearInterpolate(pc);
			break;
		case Transport::VelocityInterpolation_Type::AGW:
			velo = AGWInterpolate(pc);
			break;
		default:
	    	cout<<"other interpolation scheme not supported!!!"<<endl; 
	    	std::abort();
			break;
	}
	return velo;
}

cv::Point3d Transport::integrateDtUsingEulerianVelocityFields(const cv::Point3d& pc) const{

	cv::Point3d pc_n;
    switch (time_scheme_){           
        case Transport::TimeScheme_Type::Euler:           
			pc_n = this->doEulerScheme(pc);
        	break;
        case Transport::TimeScheme_Type::Heun:            
			pc_n = this->doHeunScheme(pc);
        	break;
    }
	return pc_n;
}

cv::Point3d Transport::doEulerScheme(const cv::Point3d& pc) const{

	auto velo = this->interpolateVelocityFieldOnParticleCoordinates(pc);
	if(isnan(velo.x) || isnan(velo.y) || isnan(velo.z)) cout<<"velo "<<velo<<endl;

	auto pc_n = pc + dt_*velo;
	this->imposeBoundaryCondition(pc_n);

	return pc_n;
}

cv::Point3d Transport::doHeunScheme(const cv::Point3d& pc) const{

	auto velo = this->interpolateVelocityFieldOnParticleCoordinates(pc);
	auto pc_n = pc + dt_*velo;
	this->imposeBoundaryCondition(pc_n);

	auto velo_inter = this->interpolateVelocityFieldOnParticleCoordinates(pc_n);
	pc_n = pc + dt_*0.5*(velo+velo_inter);
	this->imposeBoundaryCondition(pc_n);

	return pc_n;
}

void Transport::imposeBoundaryCondition(cv::Point3d& pc_n) const{

	if(pc_n.x > Xc_top_.x){
		pc_n.x -= Xc_top_.x-Xc_bottom_.x;
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}
	else if(pc_n.x < Xc_bottom_.x){
		pc_n.x += Xc_top_.x-Xc_bottom_.x;
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}

	if(pc_n.y > Xc_top_.y){
		pc_n.y -= Xc_top_.y-Xc_bottom_.y;
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}
	else if(pc_n.y < Xc_bottom_.y){
		pc_n.y += Xc_top_.y-Xc_bottom_.y;
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}

	if(pc_n.z > Xc_top_.z){
		pc_n.z -= Xc_top_.z-Xc_bottom_.z;
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
	}
	else if(pc_n.z < Xc_bottom_.z){
		pc_n.z += Xc_top_.z-Xc_bottom_.z;	
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
	}

	assert(pc_n.x >= Xc_bottom_.x && pc_n.x <= Xc_top_.x);
	assert(pc_n.y >= Xc_bottom_.y && pc_n.y <= Xc_top_.y);
	assert(pc_n.z >= Xc_bottom_.z && pc_n.z <= Xc_top_.z);
}

cv::Point3d Transport::AGWInterpolate(const cv::Point3d& pc) const{

	double r_sq_to_scattered = 1.5*cv::norm(dXc_);

	double xl,xr,yl,yr,zl,zr;
	int xl_idx,xr_idx,yl_idx,yr_idx,zl_idx,zr_idx;

	this->findNeighbors("X", pc.x, xl_idx, xr_idx, xl, xr);
	this->findNeighbors("Y", pc.y, yl_idx, yr_idx, yl, yr);
	this->findNeighbors("Z", pc.z, zl_idx, zr_idx, zl, zr);

	std::vector<cv::Point3d> pts_idx{cv::Point3d(xl_idx, yl_idx, zl_idx),
									 cv::Point3d(xl_idx, yl_idx, zr_idx),
									 cv::Point3d(xl_idx, yr_idx, zl_idx),
									 cv::Point3d(xl_idx, yr_idx, zr_idx),
									 cv::Point3d(xr_idx, yl_idx, zl_idx),
									 cv::Point3d(xr_idx, yl_idx, zr_idx),
									 cv::Point3d(xr_idx, yr_idx, zl_idx),
									 cv::Point3d(xr_idx, yr_idx, zr_idx)};

	double sum_alpha=0;
    cv::Point3d sum_velo(0., 0., 0.);

	for(auto& pt_idx: pts_idx){
	    auto alpha=std::exp( -std::pow(cv::norm( pc - cv::Point3d(xgrid_[pt_idx.x], ygrid_[pt_idx.y], zgrid_[pt_idx.z]) ), 2)/r_sq_to_scattered );
        sum_velo+=alpha*cv::Point3d( U_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x), V_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x), W_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x) );
        sum_alpha+=alpha;
	}

	return sum_velo/sum_alpha;
}

void Transport::findNeighbors(const std::string& direction_flag, double pc_x, int& xl_idx, int& xr_idx, double& xl, double& xr) const{

	std::vector<double> xgrid;
	int 				nXc_x=0;
	double 				dXc_x=0;	

	if(direction_flag=="X"){
		xgrid = xgrid_;
		dXc_x = dXc_.x;
		nXc_x = nXc_.x;
	}
	else if(direction_flag=="Y"){
		xgrid = ygrid_;
		dXc_x = dXc_.y;
		nXc_x = nXc_.y;
	}
	else if(direction_flag=="Z"){
		xgrid = zgrid_;
		dXc_x = dXc_.z;
		nXc_x = nXc_.z;
	}

	if(pc_x<xgrid.front())		xl_idx=0;
	else if(pc_x>xgrid.back())	xl_idx=nXc_x-2;
	else{
		auto pxl=std::find_if(std::begin(xgrid), std::end(xgrid), [=](const double x) { return pc_x - x < dXc_x; } );
		if(pxl==std::end(xgrid)-1) pxl--;
		xl_idx=std::distance(std::begin(xgrid), pxl);
	}

	xr_idx=xl_idx+1;
	xl=xgrid[xl_idx];
	xr=xgrid[xr_idx];
}

//http://paulbourke.net/miscellaneous/interpolation/
cv::Point3d Transport::trilinearInterpolate(const cv::Point3d& pc) const{

	double xl,xr,yl,yr,zl,zr;
	int xl_idx,xr_idx,yl_idx,yr_idx,zl_idx,zr_idx;

	this->findNeighbors("X", pc.x, xl_idx, xr_idx, xl, xr);
	this->findNeighbors("Y", pc.y, yl_idx, yr_idx, yl, yr);
	this->findNeighbors("Z", pc.z, zl_idx, zr_idx, zl, zr);

	auto x=(pc.x-xl)/(xr-xl);
	auto y=(pc.y-yl)/(yr-yl);
	auto z=(pc.z-zl)/(zr-zl);

	auto U = U_ongrid3D_[zl_idx](yl_idx,xl_idx)*(1-x)*(1-y)*(1-z)+
			 U_ongrid3D_[zl_idx](yl_idx,xr_idx)*x*(1-y)*(1-z)+
			 U_ongrid3D_[zl_idx](yr_idx,xl_idx)*(1-x)*y*(1-z)+
			 U_ongrid3D_[zr_idx](yl_idx,xl_idx)*(1-x)*(1-y)*z+
			 U_ongrid3D_[zr_idx](yl_idx,xr_idx)*x*(1-y)*z+
			 U_ongrid3D_[zr_idx](yr_idx,xl_idx)*(1-x)*y*z+
			 U_ongrid3D_[zl_idx](yr_idx,xr_idx)*x*y*(1-z)+
			 U_ongrid3D_[zr_idx](yr_idx,xr_idx)*x*y*z;

	auto V = V_ongrid3D_[zl_idx](yl_idx,xl_idx)*(1-x)*(1-y)*(1-z)+
			 V_ongrid3D_[zl_idx](yl_idx,xr_idx)*x*(1-y)*(1-z)+
			 V_ongrid3D_[zl_idx](yr_idx,xl_idx)*(1-x)*y*(1-z)+
			 V_ongrid3D_[zr_idx](yl_idx,xl_idx)*(1-x)*(1-y)*z+
			 V_ongrid3D_[zr_idx](yl_idx,xr_idx)*x*(1-y)*z+
			 V_ongrid3D_[zr_idx](yr_idx,xl_idx)*(1-x)*y*z+
			 V_ongrid3D_[zl_idx](yr_idx,xr_idx)*x*y*(1-z)+
			 V_ongrid3D_[zr_idx](yr_idx,xr_idx)*x*y*z;
	
	auto W = W_ongrid3D_[zl_idx](yl_idx,xl_idx)*(1-x)*(1-y)*(1-z)+
			 W_ongrid3D_[zl_idx](yl_idx,xr_idx)*x*(1-y)*(1-z)+
			 W_ongrid3D_[zl_idx](yr_idx,xl_idx)*(1-x)*y*(1-z)+
			 W_ongrid3D_[zr_idx](yl_idx,xl_idx)*(1-x)*(1-y)*z+
			 W_ongrid3D_[zr_idx](yl_idx,xr_idx)*x*(1-y)*z+
			 W_ongrid3D_[zr_idx](yr_idx,xl_idx)*(1-x)*y*z+
			 W_ongrid3D_[zl_idx](yr_idx,xr_idx)*x*y*(1-z)+
			 W_ongrid3D_[zr_idx](yr_idx,xr_idx)*x*y*z;		 

	return cv::Point3d(U,V,W);
}

void Transport::makeGrids(void){

	dXc_.x=(Xc_top_.x-Xc_bottom_.x)/static_cast<double>(nXc_.x-1);
	dXc_.y=(Xc_top_.y-Xc_bottom_.y)/static_cast<double>(nXc_.y-1);
	dXc_.z=(Xc_top_.z-Xc_bottom_.z)/static_cast<double>(nXc_.z-1);

 	xgrid_.resize(nXc_.x);
    ygrid_.resize(nXc_.y);
    zgrid_.resize(nXc_.z);
    std::iota(xgrid_.begin(), xgrid_.end(), 0);
    std::iota(ygrid_.begin(), ygrid_.end(), 0);
    std::iota(zgrid_.begin(), zgrid_.end(), 0);
 	std::for_each(xgrid_.begin(), xgrid_.end(), [=](double &x){ x=Xc_bottom_.x+dXc_.x*x; });
 	std::for_each(ygrid_.begin(), ygrid_.end(), [=](double &y){ y=Xc_bottom_.y+dXc_.y*y; });
 	std::for_each(zgrid_.begin(), zgrid_.end(), [=](double &z){ z=Xc_bottom_.z+dXc_.z*z; });
}

void Transport::setVelocityOnGrids(const std::vector<cv::Point3d>& velocity_fields){

	if(!U_ongrid3D_.empty()) U_ongrid3D_.clear();
	if(!V_ongrid3D_.empty()) V_ongrid3D_.clear();
	if(!W_ongrid3D_.empty()) W_ongrid3D_.clear();

	MatrixXd U_ongrid2D, V_ongrid2D, W_ongrid2D;

	U_ongrid2D.resize(nXc_.y, nXc_.x);
	V_ongrid2D.resize(nXc_.y, nXc_.x);
	W_ongrid2D.resize(nXc_.y, nXc_.x);

	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				U_ongrid2D(j,i)=velocity_fields[k*nXc_.x*nXc_.y+j*nXc_.x+i].x;
				V_ongrid2D(j,i)=velocity_fields[k*nXc_.x*nXc_.y+j*nXc_.x+i].y;
				W_ongrid2D(j,i)=velocity_fields[k*nXc_.x*nXc_.y+j*nXc_.x+i].z;
			}
		}
		U_ongrid3D_.push_back(U_ongrid2D);
		V_ongrid3D_.push_back(V_ongrid2D);
		W_ongrid3D_.push_back(W_ongrid2D);
	}
}

void Transport::readVelocityFieldsFromTXTFile(const std::string& DataFile, bool starting_flag){

	std::ifstream veloFile(DataFile);

	std::vector<std::string> x_v, y_v, z_v, u_v, v_v, w_v;
	std::string x, y, z, u, v, w, line;

	if(veloFile.is_open()){
		while(std::getline(veloFile, line)){
			std::string::size_type nc=line.find("#");
			if(nc != std::string::npos)
				line.erase(nc);

			std::istringstream is(line);
			if(is >> x >> y >> z >> u >> v >> w){
				x_v.push_back(x);
				y_v.push_back(y);
				z_v.push_back(z);
				u_v.push_back(u);
				v_v.push_back(v);
				w_v.push_back(w);
			}
		}
	}
	else{
		std::cerr<<"Error opening velocity data file "<<DataFile<<endl;
		std::abort();
	}

	if(!velocity_fields_.empty()) velocity_fields_.clear();

	cout<<"Grid size in each direction: "<<nXc_<<endl;
	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				if(starting_flag) grid_coords_.push_back(cv::Point3d( std::stod(x_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(y_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(z_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]) ));
				velocity_fields_.push_back(cv::Point3d( std::stod(u_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(v_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(w_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]) ));
			}
		}
	}

	if(starting_flag){
		velo_max_=cv::Point3d(std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);
		velo_min_=cv::Point3d(std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);

		cout<<"Velo max "<<velo_max_<<endl
			<<"Velo min "<<velo_min_<<endl;
	}
}

void Transport::readVelocityFieldsFromNPZFile(const std::string& DataFile, bool starting_flag){
	
	cout<<"DataFile "<<DataFile<<endl;
    auto load_npz = cnpy::npz_load(DataFile);

    auto X_v = load_npz["X"].as_vec<double>();
    auto Y_v = load_npz["Y"].as_vec<double>();
    auto Z_v = load_npz["Z"].as_vec<double>();
    auto U_v = load_npz["U"].as_vec<double>();
    auto V_v = load_npz["V"].as_vec<double>();
    auto W_v = load_npz["W"].as_vec<double>();

	if(!velocity_fields_.empty()) velocity_fields_.clear();

	cout<<"Grid size in each direction: "<<nXc_<<endl;
	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				auto idx = k*nXc_.x*nXc_.y+j*nXc_.x+i;
				if(starting_flag) grid_coords_.push_back(cv::Point3d( X_v[idx], Y_v[idx], Z_v[idx] ));
				velocity_fields_.push_back(cv::Point3d( U_v[idx], V_v[idx], W_v[idx] ));
			}
		}
	}

	if(starting_flag){
		velo_max_=cv::Point3d(std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);
		velo_min_=cv::Point3d(std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);

		cout<<"Velo max "<<velo_max_<<endl
			<<"Velo min "<<velo_min_<<endl;
	}
}

void Transport::saveVelocityFieldsToNPZFile(const std::string& res_file) const{

    std::vector<double> X_vec, Y_vec, Z_vec, U_vec, V_vec, W_vec;

    ASSERT(U_ongrid3D_.size()==zgrid_.size());
    ASSERT(V_ongrid3D_.size()==zgrid_.size());
    ASSERT(W_ongrid3D_.size()==zgrid_.size());

    for(int k=0; k<zgrid_.size(); k++){

	    ASSERT(U_ongrid3D_[k].rows()==ygrid_.size());
	    ASSERT(V_ongrid3D_[k].rows()==ygrid_.size());
    	ASSERT(W_ongrid3D_[k].rows()==ygrid_.size());
	    ASSERT(U_ongrid3D_[k].cols()==xgrid_.size());
	    ASSERT(V_ongrid3D_[k].cols()==xgrid_.size());
    	ASSERT(W_ongrid3D_[k].cols()==xgrid_.size());

        for(int j=0; j<ygrid_.size(); j++){
            for(int i=0; i<xgrid_.size(); i++){
            	X_vec.push_back( xgrid_[i] );
            	Y_vec.push_back( ygrid_[j] );
            	Z_vec.push_back( zgrid_[k] );
            	U_vec.push_back( U_ongrid3D_[k](j,i) );
            	V_vec.push_back( V_ongrid3D_[k](j,i) );
            	W_vec.push_back( W_ongrid3D_[k](j,i) );
            }
        }
	}

    auto NX = static_cast<long unsigned int>(X_vec.size());
    cnpy::npz_save(res_file, "X", &X_vec[0], {NX,1}, "w");
    cnpy::npz_save(res_file, "Y", &Y_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "Z", &Z_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "U", &U_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "V", &V_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "W", &W_vec[0], {NX,1}, "a");
}

void Transport::saveVelocityFieldsToVTKFile(const std::string& res_file) const{

    auto nz=zgrid_.size();
    auto ny=ygrid_.size();
    auto nx=xgrid_.size();
    auto dXc=dXc_;

    auto grid = vtkSmartPointer<vtkDoubleArray>::New();
    grid->SetNumberOfComponents(3);
    grid->SetNumberOfTuples(nx*ny*nz);
    grid->SetName("Grid");

    auto velocity = vtkSmartPointer<vtkDoubleArray>::New();
    velocity->SetNumberOfComponents(3);
    velocity->SetNumberOfTuples(nx*ny*nz);
    velocity->SetName("Velocity");

    for(int k=0; k<nz; k++){
        for(int j=0; j<ny; j++){
            for(int i=0; i<nx; i++){
            	grid->SetTuple3(k*nx*ny+j*nx+i,
                                xgrid_[i],
                                ygrid_[j],
                                zgrid_[k]);

                velocity->SetTuple3(k*nx*ny+j*nx+i,
                                    this->U_ongrid3D_[k](j,i),
                                    this->V_ongrid3D_[k](j,i),
                                    this->W_ongrid3D_[k](j,i));
            }
        }
    }

    auto dataSet = vtkSmartPointer<vtkImageData>::New();
    dataSet->SetExtent(0, nx-1, 0, ny-1, 0, nz-1);
    dataSet->SetOrigin(xgrid_[0], ygrid_[0], zgrid_[0]);
    dataSet->SetSpacing(dXc.x, dXc.y, dXc.z);
    dataSet->GetPointData()->AddArray(velocity);
    dataSet->GetPointData()->AddArray(grid);

    auto writer = vtkSmartPointer<vtkXMLImageDataWriter>::New();
    writer->SetFileName(res_file.c_str());
    writer->SetInputData(dataSet);
    writer->Write();
}

}//namespace PTL