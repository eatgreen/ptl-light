#ifndef DYNMODEL
#define DYNMODEL

#include "Transport.hpp"

namespace PTL{

///DynModel class
class DynModel
{
    public:
        enum class DynModel_Type       {LinearPolynomial, WienerFilter};

	private:
        DynModel_Type               dynmodel_type_;

	public:
    	DynModel();

    	~DynModel();

    	DynModel(DynModel_Type);

        void predictNextPaticlesPosition(const std::vector<Particle_ptr>&, int);
        void predictNextPaticlesPosition(const std::vector<Particle_ptr>&, const std::vector<cv::Point3d>&, int, bool repredict_flag);
        void predictNextPaticlesPosition(const std::vector<Particle_ptr>&, int, bool repredict_flag);

    private:
        void LinearPolynomialPredictorForSingleParticle(Particle*, int);
        void WienerFilterPredictorForSingleParticle(Particle*, int);
};

}//namespace PTL
#endif