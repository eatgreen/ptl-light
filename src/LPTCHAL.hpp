#ifndef LPTCHAL_H
#define LPTCHAL_H

#include "Dataset.hpp"

namespace PTL{

class LPTCHAL: public Dataset
{
	public:
		enum class Chal_case_Type {TR, TP}; 

	private:
		Chal_case_Type case_type_=Chal_case_Type::TR;

	public:
		LPTCHAL();

		~LPTCHAL();

		LPTCHAL(int, Chal_case_Type);

		void setIMGData(const std::string&, int, int, const std::vector<Camera_ptr>&, const std::string&) final;

		void readMarkPositionTable(const std::string&, int,
									std::vector<std::vector<std::vector<cv::Point3d>>>&,
									std::vector<std::vector<std::vector<cv::Point2d>>>&, int, int) final;
};

}//namespace PTL
#endif