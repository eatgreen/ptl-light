# PTL

## Build & Install

### Set environmental variables

* PTL_PATH          for current folder
* PTVDATA_PATH	    for lptchallenge data

### Dependence

* EIGEN    		libeigen3-dev
* TINYXML2 		libtinyxml2-6a libtinyxml2-dev
* FFTW3    		libfftw3-dev
* BOOST    		libboost-dev libboost-all-dev
* TBB      		libtbb2 libtbb-dev
* VTK>7    		libvtk7-dev libvtk7.1
* PCL>1.10 		libpcl-dev
* OPENCV   		build from source (prefered)
* CERES    		build from source
* CNPY          build from source (check include/cnpy-master/)

Under MAC OS X, please use homebrew to install.
Under Windows, please use Ubuntu in WSL2 to install.

## How to run

1. Prepare data files
2. Add your own para_config_xxx file, xxx is the case name, e.g. xxx=lptchal4500 for LPT challenge datasets.
3. Go to build folder, change cmake_list accordingly
4. run `python cat.py xxx` to make sources and load para_config
5. run `python frog.py --calib` to launch calibration module
6. run `python frog.py --pre` to launch preproc module
7. run `python frog.py --ipr` to launch ipr module, reconstruct particle field for background
8. run `python frog.py --init` to launch initTrack module, initialize track
9. run `python frog.py --klpt` to launch opt module, using either STB or ENS method
10. run `python frog.py --post` to launch post module, doing post processing

### Install(Optional)

Install is optional unless the user wished to integrate PTL to her/his project. Go to folder `build_cmake`, run `make install`.

## Notes on choosing parameter

In para_config, here we discuss #ENS parameter
* n_ens:             		   (default 8)    larger value increases the accuracy, but also increase computational time.
* regul_const:       		   (default 1)    smaller value make converging to particle image observations
* MaxOuterIter:      		   (default 5)	  outer loop iteration after which we update the residual image for next iteration.
* sigmaXcRatioX(YZ): 		   (default 1)    larger value will produce less lost tracks.
* warm_start_flag:   		   (default true) provides a warm starting point.
* selectWarmStartMaxIter:      (default 10)	  increase if tracks are lost, but may effect accuracy
* selectWarmStart_pixel:	   (default 1)    increase if tracks are lost, but may effect accuracy
* selectWarmStart_relaxfactor: (default 0.5)
