#script for make exe and
import argparse
import os
import platform
import shutil 
from subprocess import call

parser = argparse.ArgumentParser()
parser.add_argument("exptype", help="give the name of the experiment, currently support lptchal4500")
parser.add_argument("-j", "--processors", default='8', help="number of make -j")
parser.add_argument("-d", "--debug", action="store_true", help="debug build of cmake")
parser.add_argument("-p", "--parallelscheme", default="TBB", help="parallel scheme of cmake, support NO, TBB")
parser.add_argument("-t", "--target", default="all", help="targets of cmake, support all")

args=parser.parse_args()

test_dir=os.environ['PTL_PATH']+'/build/test/'
if not os.path.exists(test_dir):
    os.makedirs(test_dir)

if args.exptype=="lptchal4500":
    print('This is a {} case, make sources and load para_config'.format(args.exptype))
else:
    print("Choose the right case.")
    os.abort()
    
build_dir=os.environ['PTL_PATH']+'/build/build_cmake/'
if not os.path.exists(build_dir):
    os.makedirs(build_dir)

os.chdir(build_dir)

if args.parallelscheme=="NO":
    cmake_suffix1='-DSINGLECORE=true'
    print("Single Core build")
else:
    cmake_suffix1='-DSINGLECORE=false'
    if args.parallelscheme=="TBB":
        cmake_suffix3='-DPARALLEL_SCHEME=TBB'

if args.debug:
    cmake_suffix2='-DCMAKE_BUILD_TYPE=Debug'
else:
    cmake_suffix2='-DCMAKE_BUILD_TYPE=Release'

if platform.system()=='Darwin':
    cmake_prefix="-DCMAKE_PREFIX_PATH=/anaconda3/lib/python3.7/site-packages/torch/"
elif platform.system()=='Linux':
    cmake_prefix="-DCMAKE_PREFIX_PATH="+os.environ['HOME']+"/anaconda3/lib/python3.8/site-packages/torch/"

if not os.path.isfile('Makefile'):
    if args.parallelscheme=="NO":
        call(["cmake", cmake_prefix, "..", cmake_suffix1, cmake_suffix2])
    else:
        call(["cmake", cmake_prefix, "..", cmake_suffix1, cmake_suffix2, cmake_suffix3])

call(["make", "-j", args.processors, args.target])

parasrcpath=os.environ['PTL_PATH']+'/para_config/para_config_'+args.exptype

paradstpath=os.path.join(test_dir,'para_config')

shutil.copy(parasrcpath, paradstpath)