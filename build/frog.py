#script for pipeline exe
import argparse
import os
import shutil 
import subprocess

def parse_var(s):
    """
    Parse a key, value pair, separated by '='
    That's the reverse of ShellArgs.

    On the command line (argparse) a declaration will typically look like:
        foo=hello
    or
        foo="hello world"
    """
    items = s.split('=')
    key = items[0].strip() # we remove blanks around keys, as is logical
    if len(items) > 1:
        # rejoin the rest:
        value = '='.join(items[1:])
    return (key, value)


def parse_vars(items):
    """
    Parse a series of key-value pairs and return a dictionary
    """
    d = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            d[key] = value
    return d

parser = argparse.ArgumentParser()
parser.add_argument("--calib", action="store_true", help="rerun calibration")
parser.add_argument("--pre", action="store_true", help="rerun preproc method")
parser.add_argument("--ipr", action="store_true", help="rerun ipr method")
parser.add_argument("--init", action="store_true", help="rerun init method")
parser.add_argument("--klpt", action="store_true", help="rerun klpt method")
parser.add_argument("--post", action="store_true", help="rerun postprocess method")

parser.add_argument("--set",
                        metavar="KEY=VALUE",
                        nargs='+',
                        help="Set a number of key-value pairs "
                             "(do not put spaces before or after the = sign). "
                             "If a value contains spaces, you should define "
                             "it with double quotes: "
                             'foo="this is a sentence". Note that '
                             "values are always treated as strings.")
args=parser.parse_args()

# parse the key-value pairs
values = parse_vars(args.set)

params={}
with open("test/para_config") as file:
    for line in file:
        line=line.partition("#")[0]
        line=line.rstrip()
        line=line.partition("//")[0]
        line=line.rstrip()

        if not line.strip(): continue

        key, value = line.split()
        params[key]=value

for key in list(values.keys()):
    params[key] = values[key]

if args.calib:
    print("run calibration module")
    subprocess.call("bin/calibexe", shell=True)

if args.pre:
    print("run preprocess module")
    subprocess.call('bin/preprocexe', shell=True)

if args.ipr:
    print("run ipr module")
    subprocess.call('bin/iprexe', shell=True)

if args.init:
    print("run init Track module")
    subprocess.call('bin/initexe', shell=True)

if args.klpt:
    print("run klpt module")
    subprocess.call('bin/klptexe', shell=True)

if args.post:
    print("run postprocess module")
    subprocess.call('bin/postprocexe', shell=True)
