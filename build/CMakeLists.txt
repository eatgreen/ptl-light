#----------------------------------------------------------------------
# CMake project file for KLPT/LAPIV
# Author: Yin YANG
# Licensing: this code is distributed under the CeCILL-B license
#----------------------------------------------------------------------
#
# Set your Fortran/C/C++ compiler (GNU or MPI):
set (COMPILER GNU)
#
# Set your build type (DEBUG or RELEASE or RelWithDebInfo):
set (CMAKE_CXX_STANDARD 17)

# set(BUILD_SHARED_LIBS ON)

message (STATUS ${CMAKE_BUILD_TYPE})

if (NOT DEFINED CMAKE_HOST_SYSTEM_NAME)
   set (CMAKE_HOST_SYSTEM_NAME Linux)
endif (NOT DEFINED CMAKE_HOST_SYSTEM_NAME)

message (STATUS ${CMAKE_HOST_SYSTEM_NAME})

find_program(LSB_RELEASE_EXEC lsb_release)
execute_process(COMMAND ${LSB_RELEASE_EXEC} -is
    OUTPUT_VARIABLE LSB_RELEASE_ID_SHORT
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

message (STATUS ${LSB_RELEASE_ID_SHORT})

if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")
   message ("MAC OS X")
   set(CMAKE_OSX_SYSROOT /Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk)
   #
   set (EXTERN_INCLUDE_EIGEN /usr/local/include/eigen3/)
   #
   set (EXTERN_INCLUDE_OPENCV  /usr/local/include/opencv4/)
   set (EXTERN_INCLUDE_OPENCV2 /usr/local/include/)
   #
   set (EXTERN_INCLUDE_FFTW /usr/local/include/)
   #   #
   set (EXTERN_INCLUDE_BOOST /usr/local/include/boost/)
   #
   set (EXTERN_INCLUDE_TBB /usr/local/include/tbb/)
   #
   set (EXTERN_INCLUDE_CNPY /usr/local/include/)
   #
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")

if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
   message ("UNIX")
   if (${LSB_RELEASE_ID_SHORT} MATCHES "Ubuntu")
      message ("Ubuntu")
      #
      set (EXTERN_INCLUDE_EIGEN /usr/include/eigen3/)
      #
      # set (EXTERN_INCLUDE_OPENCV  /usr/local/include/opencv4/)
      # set (EXTERN_INCLUDE_OPENCV2 /usr/local/include/opencv4/opencv2/)
      #
      # set (EXTERN_INCLUDE_FFTW /usr/include/)
      #
      # set (EXTERN_INCLUDE_BOOST /usr/include/)
      #
      # set (EXTERN_INCLUDE_TBB /usr/include/)
      #
      # set (EXTERN_INCLUDE_CNPY /usr/local/include/)
      #
   endif (${LSB_RELEASE_ID_SHORT} MATCHES "Ubuntu")
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
#
set (EXTERN_INCLUDE_ALGLIB ../include/alglib/src)
#
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../bin)
#
# Check CMAKE version
cmake_minimum_required (VERSION 3.18)
#
#----------------------------------------------------------------------
# Setup project
#----------------------------------------------------------------------
#
project ( PTL C CXX )

file (GLOB PTL_SRC "../src/impl/*.cpp")
file (GLOB ALGLIB_SRC "../include/alglib/src/*.cpp")

find_package(OpenCV REQUIRED)
find_package(VTK COMPONENTS vtkCommonCore vtkCommonDataModel vtkIOLegacy vtkIOXML QUIET)
include(${VTK_USE_FILE})
find_package(PCL 1.10 REQUIRED)
find_package(Ceres QUIET)
find_package(ZLIB REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${TORCH_CXX_FLAGS}")

set (KLPT_TARGET        klptexe)
set (CALIB_TARGET       calibexe)
set (IPR_TARGET         iprexe)
set (INIT_TARGET        initexe)
set (PREPROC_TARGET     preprocexe)
set (POSTPROC_TARGET    postprocexe)

add_library(PTLLIB      SHARED ${PTL_SRC})
add_library(ALGLIB      SHARED ${ALGLIB_SRC})

set_target_properties(ALGLIB PTLLIB PROPERTIES
         LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../lib)

add_executable (${KLPT_TARGET}     "../app/KLPT_App.cpp" )
add_executable (${CALIB_TARGET}    "../app/Calibration_App.cpp" )
add_executable (${IPR_TARGET}      "../app/IPR_App.cpp" )
add_executable (${INIT_TARGET}     "../app/initTrack_App.cpp" )
add_executable (${PREPROC_TARGET}  "../app/PreProc_App.cpp" )
add_executable (${POSTPROC_TARGET} "../app/PostProc_App.cpp" )

##----------------------------------------------------------------------
# Setup compiler
#----------------------------------------------------------------------
#
if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")
   set (CMAKE_C_COMPILER clang)
   set (CMAKE_CXX_COMPILER clang++ CACHE STRING "C++ compiler" FORCE)
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")

if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
   set (CMAKE_C_COMPILER gcc)
   set (CMAKE_CXX_COMPILER g++ CACHE STRING "C++ compiler" FORCE)
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")

set (CMAKE_CXX_COMPILER_AR ar)
set (CMAKE_CXX_COMPILER_RANLIB ranlib)
#
#----------------------------------------------------------------------
# Define flags
#----------------------------------------------------------------------
#
# set compiler flags
if (${COMPILER} MATCHES "GNU")
   if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")
      set (COMPILER_OPTS PUBLIC -c -Wall -Wno-unknown-pragmas -Wno-pragmas -Wno-unused-variable -Wno-unused-parameter -Wno-sign-compare -Wno-nullability-completeness -Xclang -fopenmp -std=c++14)
   endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")

   if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
      set (COMPILER_OPTS PUBLIC -c -Wall -Wno-sign-compare -Wno-unknown-pragmas -Wno-pragmas -Wno-unused-variable -Wno-unused-parameter -Wno-int-in-bool-context -fopenmp -std=c++1z)
   endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
endif (${COMPILER} MATCHES "GNU")

# set cpp flags
if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")
   if (${SINGLECORE})
      message ("Single Core Build")
      set (CPPFLAGS PUBLIC _MAC PUBLIC NO_TORCH)
   else (${SINGLECORE})
      message ("TBB Build")
      set (CPPFLAGS PUBLIC _MAC PUBLIC NO_TORCH PUBLIC INTELTBB)
   endif (${SINGLECORE})
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")

if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
   if (${SINGLECORE})
      message ("Single Core Build")
      set (CPPFLAGS PUBLIC _LINUX PUBLIC NO_TORCH)
   else (${SINGLECORE})
      if (${PARALLEL_SCHEME} MATCHES "TBB")
         message ("TBB Build")
         set (CPPFLAGS PUBLIC _LINUX PUBLIC NO_TORCH PUBLIC INTELTBB) 
      endif (${PARALLEL_SCHEME} MATCHES "TBB")
   endif (${SINGLECORE})
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")

# Set linker flags
if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")
   set(LIBS -L/usr/local/lib/ -lfftw3 -lboost_system -lboost_filesystem -ltbb -lcnpy)
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")

if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")
   if (${LSB_RELEASE_ID_SHORT} MATCHES "Ubuntu")
      set(LIBS -L/usr/lib/x86_64-linux-gnu/ -L/usr/lib/x86_64-linux-gnu/ -lfftw3 -lboost_system -lboost_filesystem -ltbb -lcrypt -lpthread -ldl -lutil -lm -lcnpy)
   else (${LSB_RELEASE_ID_SHORT} MATCHES "Ubuntu")
      set(LIBS -L/usr/lib/x86_64-linux-gnu/ -lfftw3 -lboost_system -lboost_filesystem -L/media/data/klpt-lapiv/include/tinyxml2-master/build/ -L/opt/intel/compilers_and_libraries_2018.5.274/linux/tbb/lib/intel64_lin/gcc4.7/ -ltbb)
   endif (${LSB_RELEASE_ID_SHORT} MATCHES "Ubuntu")
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Linux")

include_directories (${EXTERN_INCLUDE_EIGEN})
include_directories (${EXTERN_INCLUDE_ALGLIB})
if (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")
   include_directories (${EXTERN_INCLUDE_OPENCV})
   include_directories (${EXTERN_INCLUDE_OPENCV2})
   include_directories (${EXTERN_INCLUDE_BOOST})
   include_directories (${EXTERN_INCLUDE_FFTW})
   include_directories (${EXTERN_INCLUDE_TBB})
endif (${CMAKE_HOST_SYSTEM_NAME} MATCHES "Darwin")
include_directories (${OpenCV_INCLUDE_DIRS})
include_directories (${ZLIB_INCLUDE_DIRS})
include_directories (${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

target_compile_options(PTLLIB             ${COMPILER_OPTS})
target_compile_options(ALGLIB             ${COMPILER_OPTS})
target_compile_options(${KLPT_TARGET}     ${COMPILER_OPTS})
target_compile_options(${CALIB_TARGET}    ${COMPILER_OPTS})
target_compile_options(${IPR_TARGET}      ${COMPILER_OPTS})
target_compile_options(${INIT_TARGET}     ${COMPILER_OPTS})
target_compile_options(${PREPROC_TARGET}  ${COMPILER_OPTS})
target_compile_options(${POSTPROC_TARGET} ${COMPILER_OPTS})

target_compile_definitions(PTLLIB             ${CPPFLAGS} ${PCL_DEFINITIONS})
target_compile_definitions(ALGLIB             ${CPPFLAGS} ${PCL_DEFINITIONS})
target_compile_definitions(${KLPT_TARGET}     ${CPPFLAGS} ${PCL_DEFINITIONS})
target_compile_definitions(${CALIB_TARGET}    ${CPPFLAGS} ${PCL_DEFINITIONS})
target_compile_definitions(${IPR_TARGET}      ${CPPFLAGS} ${PCL_DEFINITIONS})
target_compile_definitions(${INIT_TARGET}     ${CPPFLAGS} ${PCL_DEFINITIONS})
target_compile_definitions(${PREPROC_TARGET}  ${CPPFLAGS} ${PCL_DEFINITIONS})
target_compile_definitions(${POSTPROC_TARGET} ${CPPFLAGS} ${PCL_DEFINITIONS})

target_link_libraries (${KLPT_TARGET}     PTLLIB ${ZLIB_LIBRARIES} ALGLIB ${OpenCV_LIBS} ${PCL_LIBRARIES} ${VTK_LIBRARIES} ${LIBS} Ceres::ceres)
target_link_libraries (${CALIB_TARGET}    PTLLIB ${ZLIB_LIBRARIES} ALGLIB ${OpenCV_LIBS} ${PCL_LIBRARIES} ${VTK_LIBRARIES} ${LIBS} Ceres::ceres)
target_link_libraries (${IPR_TARGET}      PTLLIB ${ZLIB_LIBRARIES} ALGLIB ${OpenCV_LIBS} ${PCL_LIBRARIES} ${VTK_LIBRARIES} ${LIBS} Ceres::ceres)
target_link_libraries (${INIT_TARGET}     PTLLIB ${ZLIB_LIBRARIES} ALGLIB ${OpenCV_LIBS} ${PCL_LIBRARIES} ${VTK_LIBRARIES} ${LIBS} Ceres::ceres)
target_link_libraries (${PREPROC_TARGET}  PTLLIB ${ZLIB_LIBRARIES} ALGLIB ${OpenCV_LIBS} ${PCL_LIBRARIES} ${VTK_LIBRARIES} ${LIBS} Ceres::ceres)
target_link_libraries (${POSTPROC_TARGET} PTLLIB ${ZLIB_LIBRARIES} ALGLIB ${OpenCV_LIBS} ${PCL_LIBRARIES} ${VTK_LIBRARIES} ${LIBS} Ceres::ceres)

file (GLOB PTL_HEADER "../src/*.hpp")
file (GLOB ALGLIB_HEADER "../include/alglib/src/*.h")

install(FILES ${PTL_HEADER}    DESTINATION include)
install(FILES ${ALGLIB_HEADER} DESTINATION include)
install(TARGETS PTLLIB LIBRARY DESTINATION lib PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
install(TARGETS ALGLIB LIBRARY DESTINATION lib PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
