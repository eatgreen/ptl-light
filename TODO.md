#TODO lists

##AAA: 

##AA: 

* consider adding dynamics
* bug found for ens method when the local patch size is not square, this is the case when the radius in projectToImageSpace is not rounded.
* move computing intensive part to GPU using cuda

##A: 

* stereoRectify can be added later because it must be added with a coordinate searching procedure
* add ensemble smoothing method
* gui 

##DONE:

* DONE add LAPIV-time-resolved
* DONE add track initialization method using predictor Eulerian velocity field
* DONE add OTF calibration model
* DONE LAPIV using IPR results
* DONE add particle position determination module
* DONE add particle triangulation module
* DONE add divergence free velocity field generation routine
* DONE intensity correction
* DONE revisit velocity interpolation scheme, overlapping not working
* DONE add sliding overlapping window for velocity estimation
* DONE add matrix-valued OTF parameter
* DONE remove n_part_tot parameter
* DONE ENS_V method finally works
* DONE triangulation for background as well as for new particles
* DONE modify projectToImageSpace, we do not need to calculate the presampled Gaussian each time, we can save it for the first time then just do interpolation
* DONE pose periodic boundary condition in transport module in order to treat vanishing particles
* DONE add opencv calibrateCamera and stereoCalibrate methods
* DONE add calibration view and more parameters to render better particle image.
* DONE enable cycling procedure for STB and ENS
* DONE migrate image synthetic production procedure from matlab to C++
* DONE decouple camera calibration and synthetic observation into C++ version.
* DONE add pinhole model for mapping function
* DONE add a warm start procedure before going into ENS minimization

##CANCELED:

