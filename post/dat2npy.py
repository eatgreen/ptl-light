import matplotlib.pyplot as plt
import numpy as np
import os

nx = 291#291,146,97,73,37
ny = 91#91,46,31,23,12
nz = 48#48,24,16,12,6

# velo_file = "/mnt/d/Google-Drive/Tomo_PIV_LASIE/2016-06-24-SOL-CONV_Re1250/CONV-2D_Re1250_Sol_F500/ImgPreproc_03/ShakeTheBox_tr_01/ConvertToGrid_64x64x64_75%ov_ord=0/ExportTecplotDat/B00001.dat"
# velo_file = "/mnt/d/Google-Drive/lapiv-30000-0.1/012021/ConvertToGrid_64x64x64_75%ov_ord=0/ExportTecplotDat/B00039.dat"
# velo_file = "/mnt/d/Google-Drive/lapiv-30000-0.1/012021/ConvertToGrid_32x32x32_75%ov_ord=0/ExportTecplotDat/B00039.dat"
velo_file = "/mnt/d/Google-Drive/lapiv-30000-0.1/012021/FineScaleReconst_grid=4/ExportTecplotDat/B00001.dat"
f=np.loadtxt(velo_file,skiprows=4,max_rows=nx*ny*nz)

X=f[:,0] 
Y=f[:,1]
Z=f[:,2]
U=f[:,3]/1000. 
V=f[:,4]/1000. 
W=f[:,5]/1000.

Xr=[]
Yr=[]
Zr=[]
Ur=[]
Vr=[]
Wr=[]

for k in range(nz):
	for j in reversed(range(ny)):
		for i in range(nx):
			# print("{} {} {}".format(i,j,k))
			idx = k*nx*ny + j*nx + i 
			Xr.append(X[idx])
			Yr.append(Y[idx])
			Zr.append(Z[idx])
			Ur.append(U[idx])
			Vr.append(V[idx])
			Wr.append(W[idx])

XX=np.asarray(Xr)
YY=np.asarray(Yr)
ZZ=np.asarray(Zr)
UU=np.asarray(Ur)
VV=np.asarray(Vr)
WW=np.asarray(Wr)

# out_file = "/mnt/d/stb-ens-ptv-c/data/lavision_jet/part900000_maxt0050_fct00005/velo"
# out_file = "/mnt/d/Google-Drive/lapiv-30000-0.1/012021/ConvertToGrid_64x64x64_75%ov_ord=0/velo"
# out_file = "/mnt/d/Google-Drive/lapiv-30000-0.1/012021/ConvertToGrid_32x32x32_75%ov_ord=0/velo"
out_file = "/mnt/d/Google-Drive/lapiv-30000-0.1/012021/FineScaleReconst_grid=4/velo"
np.savez(out_file+".npz", X=XX, Y=YY, Z=ZZ, U=UU, V=VV, W=WW)

data = np.load(out_file+".npz")
for i in range(50):
	print('{} {} {}'.format(data['X'][i], data['Y'][i], data['Z'][i]))

# np.savetxt(out_file+".txt", U)