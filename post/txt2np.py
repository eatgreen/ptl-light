import numpy as np

res_dir='/media/yin/Data/ptl/data/lptchal4500/part010000_maxt0050_fct00005_dtobs0.0006/'

for i in range(4):
	res_file=res_dir+'bkg_recon/bkg_it00'+str(i+1)
	res_name=res_file+'.txt'

	f=np.loadtxt(res_name)
	id=f[:,0]
	id=id.astype(np.int32)  
	x=f[:,3]
	y=f[:,4]
	z=f[:,5]
	e=f[:,6]

	ID = np.delete(id, np.where(x == -1))
	X = np.delete(x, np.where(x == -1))
	Y = np.delete(y, np.where(y == -1))
	Z = np.delete(z, np.where(z == -1))
	E = np.delete(e, np.where(e == -1))

	out_file=res_file+'.npz'
	np.savez(out_file, ID=ID, X=X, Y=Y, Z=Z, E=E)

	f=np.load(out_file)
	X=f['X']
	ID=f['ID']

	# print(ID[-100:])
	# print(X[-100:])
